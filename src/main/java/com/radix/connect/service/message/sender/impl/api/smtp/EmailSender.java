package com.radix.connect.service.message.sender.impl.api.smtp;

import com.google.gson.JsonArray;
import com.radix.connect.core.annotation.MsgSender;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.SenderAPI;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.message.sender.Sender;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.SenderConfig;
import com.radix.connect.core.model.response.MsgProcessResponse;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.RateLimiter.Permission;
import com.radix.connect.core.util.Validator;
import com.radix.connect.service.config.AccountService;
import com.radix.connect.service.message.SenderLogService;
import com.radix.connect.service.message.sender.impl.AbstractSender;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author kiran.chokhande
 */
@MsgSender(sender_api = SenderAPI.SMTP_API, modality = Modality.EMAIL)
public final class EmailSender extends AbstractSender implements Sender {

  private volatile String fromEmailAddress;
  private volatile SmtpMailSender smtpMailSender;

  @Autowired
  public EmailSender(AccountService accountService, SenderLogService senderLogService) {
    super(Modality.EMAIL, accountService, senderLogService);
  }

  @Override
  public final void configure(final SenderConfig senderConfig) {

    String configuration = senderConfig == null ? null : senderConfig.getConfig_json();
    Validator.requireNonEmpty(configuration, "SMTP email configuration must be required");

    JsonUtil smtpConfig = new JsonUtil(configuration);

    String smtpHost = smtpConfig.getValue("smtp_host");
    String smtpPort = smtpConfig.getValue("smtp_port");
    String smtpAuth = smtpConfig.getValue("smtp_auth");
    String startTls = smtpConfig.getValue("start_tls");
    String fromEmail = smtpConfig.getValue("from_email");

    smtpAuth = Boolean.parseBoolean(smtpAuth) ? "true" : "false";
    startTls = Boolean.parseBoolean(startTls) ? "true" : "false";
    Validator.requireNonEmpty(smtpHost, "SMTP host must be required");
    Validator.requireNonEmpty(smtpPort, "SMTP port must be required");
    Validator.requireNonEmpty(fromEmail, "From email address must be required");

    Properties smtpProperties = new Properties();
    smtpProperties.put("mail.smtp.host", smtpHost);
    smtpProperties.put("mail.smtp.port", smtpPort);
    smtpProperties.put("mail.smtp.auth", smtpAuth);
    smtpProperties.put("mail.smtp.starttls.enable", startTls);

    Session session = Session.getInstance(smtpProperties, new Authenticator() {
      @Override
      protected PasswordAuthentication getPasswordAuthentication() {
        String username = smtpConfig.getValue("username");
        String password = smtpConfig.getValue("password");
        return new PasswordAuthentication(username, password);
      }
    });

    this.fromEmailAddress = fromEmail;
    this.smtpMailSender = new SmtpMailSender(session);
    super.messagingRateLimiter.setMaxRate(senderConfig.getMessaging_rate());
  }

  @Override
  public final MsgProcessResponse sendOutboundMessage(Message message, boolean isResponseMessage) {

    Validator.requireNonEmpty(message.getContact(), "Email address must be required");
    if (!Validator.isValidContact(message.getModality(), message.getContact())) {
      return MsgProcessResponse
          .failure(Status.FAILED, String.format(INVALID_EMAIL_ID_ERR, message.getContact()));
    }

    try {

      JsonUtil messageJson = new JsonUtil(message.getMessage());
      JsonUtil attachment = new JsonUtil(messageJson.getAsJsonO("attachment"));
      String s3BucketName = attachment.getValue("s3_bucket_name");
      JsonArray attachmentS3FilePaths = attachment.getAsJsonA("s3_file_paths");

      String senderName = messageJson.getValue("sender_name");
      String toEmailAddress = Validator.sanitizeContact(modality, message.getContact());
      MimeMessageBuilder messageBuilder = new MimeMessageBuilder(smtpMailSender.session,
          senderName + " <" + this.fromEmailAddress + ">", toEmailAddress);

      messageBuilder.setSubject(messageJson.getValue("subject"));
      messageBuilder.setCC(messageJson.getValue("cc"));
      messageBuilder.setReplyTo(messageJson.getValue("reply_to"));
      messageBuilder.setBody(messageJson.getValue("message"),
          messageBuilder.buildAttachmentFiles(s3BucketName, attachmentS3FilePaths));

      try (Permission permission = super.messagingRateLimiter.acquire()) {
        this.smtpMailSender.sendEmailMsg(messageBuilder.build());
      }
      return MsgProcessResponse.success(Status.COMPLETED);
    } catch (Exception exception) {
      RxLogger.logException("SMTP API Request Error", exception);
      return MsgProcessResponse.failure(Status.EXCEPTION, exception);
    }
  }

  /**
   * @author kiran.chokhande
   */
  private final class SmtpMailSender {

    private final Session session;
    private volatile Transport transport;

    private SmtpMailSender(final Session session) {
      this.session = session;
      System.setProperty("line.separator", "\r\n");
    }

    private void sendEmailMsg(MimeMessage mimeMessage) throws Exception {

      if (this.transport == null || !this.transport.isConnected()) {
        this.transport = getSmtpTransports();
      }
      transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
    }

    private synchronized Transport getSmtpTransports() throws Exception {

      if (transport != null && transport.isConnected()) return transport;

      try {

        if (transport != null) {
          transport.close();
        }
      } catch (Exception exc) {
        RxLogger.logException(exc);
      }

      Transport transport = this.session.getTransport();
      transport.connect();
      return this.transport = transport;
    }
  }
}
