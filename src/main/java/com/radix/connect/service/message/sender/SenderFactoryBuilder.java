package com.radix.connect.service.message.sender;

import com.radix.connect.core.Builder;
import com.radix.connect.core.annotation.MsgSender;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.SenderAPI;
import com.radix.connect.core.message.sender.Sender;
import com.radix.connect.core.message.sender.SenderManager.SenderFactory;
import com.radix.connect.core.model.SenderConfig;
import com.radix.connect.core.model.SenderMapping;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;
import com.radix.connect.repository.ConfigRepository;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class SenderFactoryBuilder implements Builder<Modality, SenderFactory> {

  private final ApplicationContext context;
  private final ConfigRepository configRepository;
  private final Map<Modality, Map<SenderAPI, Class<Sender>>> classMap = new HashMap<>();

  private static final String APPLICATION_CONTEXT_REQUIRED = "ApplicationContext must be required";
  private static final String CONFIG_REPOSITORY_REQ = "ConfigRepository must be required";
  private static final String MULTIPLE_SENDERS_IMPL = "Sender implemented multiple times for sender api: %s and modality: %s";
  private static final String NO_SENDER_IMPLEMENTED_ER = "Sender not implemented or not annotated by @MsgSender with sender api: %s and modality: %s";
  private static final String NO_SENDER_CONFIG_ERROR = "Sender not configured for account id: %s, modality: %s";

  public SenderFactoryBuilder(ApplicationContext context, ConfigRepository configRepository) {

    this.context = Validator.requireNonNull(context, APPLICATION_CONTEXT_REQUIRED);
    this.configRepository = Validator.requireNonNull(configRepository, CONFIG_REPOSITORY_REQ);

    Arrays.stream(context.getBeanNamesForAnnotation(MsgSender.class))
        .forEach(beanName -> registerSenderImplClsIfSupports(context.getType(beanName)));
  }

  @Override
  public final SenderFactory build(final Modality modality) {
    return new SenderFactoryImpl(modality, classMap.get(modality));
  }

  @SuppressWarnings("unchecked")
  private void registerSenderImplClsIfSupports(final Class<?> senderImplementationCls) {

    if (Sender.class.isAssignableFrom(senderImplementationCls)) {

      MsgSender msgSender = senderImplementationCls.getAnnotation(MsgSender.class);
      SenderAPI api = msgSender.sender_api();
      Modality modality = msgSender.modality();

      Map<SenderAPI, Class<Sender>> senderClasses;
      senderClasses = classMap.computeIfAbsent(modality, clMap -> new HashMap<>());

      if (!senderClasses.containsKey(api)) {
        senderClasses.put(api, (Class<Sender>) senderImplementationCls);
      } else {
        throw new ConnectException(String.format(MULTIPLE_SENDERS_IMPL, api, modality));
      }
    }
  }

  /**
   * @author kiran.chokhande
   */
  private final class SenderFactoryImpl implements SenderFactory {

    private final Modality modality;
    private final Map<SenderAPI, Class<Sender>> senderClassMap;
    private final Map<String, Sender> sendersMap = new HashMap<>();

    private SenderFactoryImpl(final Modality mode, Map<SenderAPI, Class<Sender>> maps) {
      this.modality = Validator.requireNonNull(mode, "Modality must required");
      this.senderClassMap = Collections.unmodifiableMap(maps != null ? maps : new HashMap<>());
    }

    @Override
    public final void reload(final String senderId) {
      this.sendersMap.remove(senderId);
    }

    @Override
    public final Sender getSender(final String accountId) {

      Validator.requireNonEmpty(accountId, "Account Id must be required");

      SenderMapping mapping = configRepository.selectSenderMapping(accountId, modality);
      if (mapping == null) {
        throw new ConnectException(String.format(NO_SENDER_CONFIG_ERROR, accountId, modality));
      }

      Sender sender = sendersMap.get(mapping.getSender_id());
      if (sender != null) {
        return sender;
      }

      synchronized (sendersMap) {
        return sendersMap.computeIfAbsent(mapping.getSender_id(),
            newSenderImpl -> createSenderFor(accountId, mapping.getSender_id()));
      }
    }

    private Sender createSenderFor(final String accountId, final String senderId) {

      SenderConfig config = configRepository.selectSenderConfig(senderId);
      if (config != null && config.getSender_api() != null) {

        Class<Sender> senderImplClass = senderClassMap.get(config.getSender_api());
        if (senderImplClass != null) {
          Sender senderI = context.getAutowireCapableBeanFactory().createBean(senderImplClass);
          senderI.configure(config);
          return senderI;
        }
      } else {
        throw new ConnectException(String.format(NO_SENDER_CONFIG_ERROR, accountId, modality));
      }

      SenderAPI senderAPI = config.getSender_api();
      throw new ConnectException(String.format(NO_SENDER_IMPLEMENTED_ER, senderAPI, modality));
    }
  }
}
