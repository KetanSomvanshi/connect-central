package com.radix.connect.service.message.sender.impl.api.twilio;

import com.radix.connect.core.annotation.MsgSender;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.SenderAPI;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.message.sender.Sender;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.SenderConfig;
import com.radix.connect.core.model.response.MsgProcessResponse;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.RateLimiter.Permission;
import com.radix.connect.core.util.RepetitiveSequence;
import com.radix.connect.core.util.Validator;
import com.radix.connect.service.config.AccountService;
import com.radix.connect.service.message.SenderLogService;
import com.radix.connect.service.message.sender.impl.AbstractSender;
import com.twilio.exception.ApiException;
import com.twilio.http.HttpMethod;
import com.twilio.http.TwilioRestClient;
import com.twilio.rest.api.v2010.account.Call;
import com.twilio.rest.api.v2010.account.CallCreator;
import com.twilio.type.PhoneNumber;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author kiran.chokhande
 */
@MsgSender(sender_api = SenderAPI.TWILIO_API, modality = Modality.VOICE)
public final class VoiceSender extends AbstractSender implements Sender {

  private volatile TwilioRestClient twilioRestClient;
  private volatile RepetitiveSequence<PhoneNumber> fromPhoneNumbers;

  private static final String DASH_VOICE_CALL_ANSWERING_URL = "%s/rs/connect/message/%s/answered";

  @Autowired
  public VoiceSender(AccountService accountService, SenderLogService senderLogService) {
    super(Modality.VOICE, accountService, senderLogService);
  }

  @Override
  public final void configure(final SenderConfig senderConfig) {

    String configuration = senderConfig == null ? null : senderConfig.getConfig_json();
    Validator.requireNonEmpty(configuration, "Twilio configuration must be required");

    JsonUtil config = new JsonUtil(configuration);

    String phoneNumber = config.getValue("phone_number");
    String username = config.getValue("account_sid");
    String password = config.getValue("auth_token");

    List<PhoneNumber> fromPhoneNumbers = new ArrayList<>();
    if (phoneNumber != null && !phoneNumber.trim().isEmpty()) {
      Arrays.stream(phoneNumber.split(",")).filter(Objects::nonNull).forEach(phn -> {
        if (Validator.isValidContact(modality, phn)) {
          fromPhoneNumbers.add(new PhoneNumber(phn));
        }
      });
    }

    Validator.requireNonEmpty(username, "Account sid must be required");
    Validator.requireNonEmpty(password, "Authentication token must be required");
    Validator.requireNonEmpty(fromPhoneNumbers, "From phone number must be required");

    super.messagingRateLimiter.setMaxRate(senderConfig.getMessaging_rate());
    this.fromPhoneNumbers = new RepetitiveSequence<>(fromPhoneNumbers);
    this.twilioRestClient = new TwilioRestClient.Builder(username, password).build();
  }

  @Override
  public final MsgProcessResponse fetchStatus(Message message) {

    if (Validator.isEmpty(message.getMsg_sid()) || message.getProcessed_ts() == null) {
      return null;
    }

    Status msgStatus = null;
    String errorDesc = null;
    switch (Call.fetcher(message.getMsg_sid()).fetch(twilioRestClient).getStatus()) {
      case COMPLETED:
        msgStatus = Status.COMPLETED;
        break;

      case FAILED:
        msgStatus = Status.FAILED;
        errorDesc = "Failed to make a voice call";
        break;

      case BUSY:
        msgStatus = Status.BUSY;
        errorDesc = "The user phone number was busy";
        break;

      case CANCELED:
        msgStatus = Status.CANCELED;
        errorDesc = "User has canceled the voice call";
        break;

      case NO_ANSWER:
        msgStatus = Status.NO_ANSWER;
        errorDesc = "User did not answer the voice call";
        break;
    }

    return message.getProcessed_ts() == null || msgStatus == null ? null
        : (msgStatus.isSuccess() ? MsgProcessResponse.success(msgStatus)
            : MsgProcessResponse.failure(msgStatus, errorDesc)).withMsgSid(message.getMsg_sid());
  }

  @Override
  public final MsgProcessResponse sendOutboundMessage(Message message, boolean isResponseMessage) {

    Validator.requireNonEmpty(message.getContact(), "Phone number must be required");
    if (!Validator.isValidContact(message.getModality(), message.getContact())) {
      return MsgProcessResponse
          .failure(Status.FAILED, String.format(INVALID_PHONE_NO_ERR, message.getContact()));
    }

    try {

      PhoneNumber fromPhone = this.fromPhoneNumbers.next();
      PhoneNumber toPhone = new PhoneNumber(Validator.sanitizeContact(modality, message.getContact()));

      String msg_sid, accountId = message.getAccount_id(), messageId = message.getMessage_id();
      URI DASH_VOICE_CALL_ANSWERING_URI = URI.create(String.format(DASH_VOICE_CALL_ANSWERING_URL,
          fetchAccountDetailFor(accountId).getApp_base_url(), message.getMessage_id()));

      try (Permission permission = super.messagingRateLimiter.acquire()) {
        CallCreator creator = new CallCreator(toPhone, fromPhone, DASH_VOICE_CALL_ANSWERING_URI);
        msg_sid = creator.setMethod(HttpMethod.GET).create(twilioRestClient).getSid();
      }
      if (fromPhone != null && !Validator.isEmpty(msg_sid)) {
        updateSenderLog(accountId, messageId, toPhone.getEndpoint(), fromPhone.getEndpoint());
      }

      return !Validator.isEmpty(msg_sid) ? MsgProcessResponse.success(Status.IN_PROCESS, msg_sid)
          : MsgProcessResponse.failure(Status.FAILED, "Failed to send text message.", msg_sid);
    } catch (Exception exception) {
      RxLogger.logException("Twilio API Request Error", exception);

      String error = exception.getMessage(), contact = message.getContact();
      if (exception instanceof ApiException && error != null) {
        if (error.contains("not a valid phone number")) {
          return MsgProcessResponse.failure(Status.FAILED, String.format(INVALID_PHONE_NO_ERR, contact));
        } else if (error.contains("not currently reachable") || error.contains("not allowed to call")) {
          return MsgProcessResponse.failure(Status.FAILED, String.format(PHONE_NO_NOT_REACHABLE_ERR, contact));
        }
      }
      return MsgProcessResponse.failure(Status.EXCEPTION, exception);
    }
  }
}
