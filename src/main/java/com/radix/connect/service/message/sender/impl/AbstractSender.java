package com.radix.connect.service.message.sender.impl;

import com.google.gson.JsonArray;
import com.radix.connect.core.constant.Direction;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.message.sender.Sender;
import com.radix.connect.core.model.Account;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.SenderLog;
import com.radix.connect.core.model.response.MsgProcessResponse;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.RateLimiter;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.FieldRequiredException;
import com.radix.connect.service.config.AccountService;
import com.radix.connect.service.file.AmazonS3Service;
import com.radix.connect.service.message.SenderLogService;
import com.radix.connect.service.message.processor.DashApiExecutor;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

/**
 * @author kiran.chokhande
 */
public abstract class AbstractSender implements Sender {

  private final AccountService accountService;
  private final SenderLogService senderLogService;

  protected final Modality modality;
  protected final RateLimiter messagingRateLimiter = new RateLimiter(1);

  protected final static String INVALID_EMAIL_ID_ERR = "%s is not valid email address";
  protected final static String INVALID_FAX_NO_ERR = "%s is not valid fax number";
  protected final static String INVALID_PHONE_NO_ERR = "%s is not valid phone number";
  protected final static String PHONE_NO_NOT_REACHABLE_ERR = "Phone number %s is currently not reachable";
  protected final static String AMAZON_SECURITY_TOKEN_ERR = "Amazon SES security token is either expired or invalid";

  public AbstractSender(Modality modality, AccountService accountService, SenderLogService logService) {
    this.modality = Validator.requireNonNull(modality, "Modality must be required");
    this.accountService = Validator.requireNonNull(accountService, "AccountService must required");
    this.senderLogService = Validator.requireNonNull(logService, "SenderLogService must required");
  }

  protected final Account fetchAccountDetailFor(final String accountId) {
    return accountService.getAccountById(accountId);
  }

  protected final SenderLog getPreviousSenderLog(final String toContact) {

    try {
      return senderLogService.getRecentSenderLogByPhone(modality, toContact);
    } catch (Exception exc) {
      RxLogger.logException("Fetch Previous Sender Log By Contact: " + toContact, exc);
      return null;
    }
  }

  protected final void updateSenderLog(String accountId, String messageId, String toContact, String fromContact) {
    SenderLog senderLog = new SenderLog(modality, toContact, fromContact, accountId, messageId);
    senderLogService.updateRecentSenderLog(senderLog);
  }

  @Override
  public MsgProcessResponse fetchStatus(Message message) {

    MsgProcessResponse res = null;
    if (message.getMsg_status() != null && message.getProcessed_ts() != null) {
      if (message.getMsg_status().isSuccess()) {
        res = MsgProcessResponse.success(message.getMsg_status(), message.getMsg_sid());
      } else {
        res = MsgProcessResponse.failure(message.getMsg_status(), message.getError_desc());
      }
    }
    return res;
  }

  @Override
  public final MsgProcessResponse sendMessage(Message message, boolean isResponseMessage) {

    try {
      if (message.getDirection() == Direction.INBOUND) {
        return sendInboundMessage(message);
      } else {
        JsonUtil messageObj = new JsonUtil(message.getMessage());
        if (!Validator.isEmpty(messageObj.getValue("message"))) {
          return sendOutboundMessage(message, isResponseMessage);
        } else {
          throw new FieldRequiredException("Message body must be required.");
        }
      }
    } catch (FieldRequiredException field_req_exception) {
      return MsgProcessResponse.failure(Status.FAILED, field_req_exception.getMessage());
    }
  }

  protected MsgProcessResponse sendInboundMessage(Message message) {

    Account account = fetchAccountDetailFor(message.getAccount_id());
    return DashApiExecutor.sendInboundMessageReqToDash(account, message);
  }

  protected abstract MsgProcessResponse sendOutboundMessage(Message message, boolean isResponseMessage);

  /**
   * @author kiran.chokhande
   */
  protected static final class MimeMessageBuilder {

    private Multipart mimeMultipart;
    private final MimeMessage mimeMessage;

    public MimeMessageBuilder(Session session, String fromEmail, String toEmails) throws Exception {
      Objects.requireNonNull(session, "Session must be required");
      Validator.requireNonEmpty(toEmails, "To email address must be required");
      Validator.requireNonEmpty(fromEmail, "From email address must be required");

      this.mimeMessage = new MimeMessage(session);
      this.mimeMessage.setFrom(new InternetAddress(fromEmail));
      this.mimeMessage.addRecipient(RecipientType.TO, new InternetAddress(toEmails));
    }

    public final void setSubject(final String subject) throws Exception {
      if (!Validator.isEmpty(subject)) {
        mimeMessage.setSubject(subject);
      }
    }

    public final void setReplyTo(final String replyTo) throws Exception {
      if (!Validator.isEmpty(replyTo)) {
        mimeMessage.setReplyTo(InternetAddress.parse(replyTo));
      }
    }

    public final void setCC(final String cc) throws Exception {
      if (!Validator.isEmpty(cc)) {
        mimeMessage.addRecipients(RecipientType.CC, InternetAddress.parse(cc));
      }
    }

    public final void setBody(final String body, final List<File> attachmentFile) throws Exception {
      Validator.requireNonEmpty(body, "Email body must be required");

      this.mimeMultipart = new MimeMultipart();

      MimeBodyPart bodyPart = new MimeBodyPart();
      bodyPart.setContent(body, "text/html; charset=UTF-8");
      this.mimeMultipart.addBodyPart(bodyPart);

      if (!Validator.isEmpty(attachmentFile)) {
        attachmentFile.stream().filter(f -> f != null && f.exists() && f.isFile()).forEach(file -> {
          try {
            MimeBodyPart attachment = new MimeBodyPart();
            attachment.setDisposition(Part.ATTACHMENT);
            attachment.attachFile(file);
            attachment.setFileName(file.getName());

            this.mimeMultipart.addBodyPart(attachment);
          } catch (Exception error) {
            RxLogger.logException("Failed to set email attachments file: " + file.getName(), error);
          }
        });
      }
    }

    public final List<File> buildAttachmentFiles(final String s3Bucket, final JsonArray filePaths) {

      List<File> attachmentFileList = new ArrayList<>();
      if (!Validator.isEmpty(s3Bucket) && !Validator.isEmpty(filePaths)) {
        filePaths.forEach(path -> {
          if (path != null && path.isJsonPrimitive()) {
            try {
              attachmentFileList.add(AmazonS3Service.downloadFile(s3Bucket, path.getAsString()));
            } catch (Exception exc) {
              RxLogger.logException(String.format(
                  "Failed to load file from S3 bucket: %s, file path: %s", s3Bucket, path), exc);
            }
          }
        });
      }
      return attachmentFileList;
    }

    public final MimeMessage build() throws Exception {

      if (this.mimeMultipart != null) {
        this.mimeMessage.setContent(this.mimeMultipart);
      }
      return this.mimeMessage;
    }
  }
}
