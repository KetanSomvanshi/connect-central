package com.radix.connect.service.message.sender.impl.api.twilio;

import com.radix.connect.core.annotation.MsgSender;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.SenderAPI;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.message.sender.Sender;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.SenderConfig;
import com.radix.connect.core.model.response.MsgProcessResponse;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.RateLimiter.Permission;
import com.radix.connect.core.util.Validator;
import com.radix.connect.service.config.AccountService;
import com.radix.connect.service.message.SenderLogService;
import com.radix.connect.service.message.sender.impl.AbstractSender;
import com.twilio.exception.ApiException;
import com.twilio.http.TwilioRestClient;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author kiran.chokhande
 */
@MsgSender(sender_api = SenderAPI.TWILIO_API, modality = Modality.TEXT)
public final class TextSender extends AbstractSender implements Sender {

  private volatile PhoneNumber fromPhoneNumber;
  private volatile TwilioRestClient twilioRestClient;

  @Autowired
  public TextSender(AccountService accountService, SenderLogService senderLogService) {
    super(Modality.TEXT, accountService, senderLogService);
  }

  @Override
  public final void configure(final SenderConfig senderConfig) {

    String configuration = senderConfig == null ? null : senderConfig.getConfig_json();
    Validator.requireNonEmpty(configuration, "Twilio configuration must be required");

    JsonUtil config = new JsonUtil(configuration);

    String phoneNumber = config.getValue("phone_number");
    String username = config.getValue("account_sid");
    String password = config.getValue("auth_token");
    Validator.requireNonEmpty(username, "Account sid must be required");
    Validator.requireNonEmpty(password, "Authentication token must be required");
    Validator.requireNonEmpty(phoneNumber, "From phone number must be required");

    super.messagingRateLimiter.setMaxRate(senderConfig.getMessaging_rate());
    this.fromPhoneNumber = new PhoneNumber(phoneNumber);
    this.twilioRestClient = new TwilioRestClient.Builder(username, password).build();
  }

  @Override
  public final MsgProcessResponse fetchStatus(Message message) {

    if (Validator.isEmpty(message.getMsg_sid()) || message.getProcessed_ts() == null) {
      return null;
    }

    Status msgStatus = null;
    String errorDesc = null;
    switch (com.twilio.rest.api.v2010.account.Message
        .fetcher(message.getMsg_sid()).fetch(twilioRestClient).getStatus()) {
      case DELIVERED:
        msgStatus = Status.DELIVERED;
        break;

      case FAILED:
        msgStatus = Status.FAILED;
        errorDesc = "Failed to send text message to user";
        break;

      case UNDELIVERED:
        msgStatus = Status.UNDELIVERED;
        errorDesc = "Text message not delivered to user";
        break;
    }

    return message.getProcessed_ts() == null || msgStatus == null ? null
        : (msgStatus.isSuccess() ? MsgProcessResponse.success(msgStatus)
            : MsgProcessResponse.failure(msgStatus, errorDesc)).withMsgSid(message.getMsg_sid());
  }

  @Override
  public final MsgProcessResponse sendOutboundMessage(Message message, boolean isResponseMessage) {

    Validator.requireNonEmpty(message.getContact(), "Phone number must be required");
    if (!Validator.isValidContact(message.getModality(), message.getContact())) {
      return MsgProcessResponse
          .failure(Status.FAILED, String.format(INVALID_PHONE_NO_ERR, message.getContact()));
    }

    try {

      PhoneNumber fromPhone = this.fromPhoneNumber;
      PhoneNumber toPhone = new PhoneNumber(Validator.sanitizeContact(modality, message.getContact()));

      String body = new JsonUtil(message.getMessage()).getValue("message");
      String msg_sid, accountId = message.getAccount_id(), messageId = message.getMessage_id();
      try (Permission permission = super.messagingRateLimiter.acquire()) {
        MessageCreator creator = new MessageCreator(toPhone, fromPhone, body);
        msg_sid = creator.setValidityPeriod(240).create(twilioRestClient).getSid();
      }
      if (fromPhone != null && !Validator.isEmpty(msg_sid)) {
        updateSenderLog(accountId, messageId, toPhone.getEndpoint(), fromPhone.getEndpoint());
      }

      return !Validator.isEmpty(msg_sid) ? MsgProcessResponse.success(Status.SENT, msg_sid)
          : MsgProcessResponse.failure(Status.FAILED, "Failed to send text message.", msg_sid);
    } catch (Exception exception) {
      RxLogger.logException("Twilio API Request Error", exception);

      String error = exception.getMessage(), contact = message.getContact();
      if (exception instanceof ApiException && error != null) {
        if (error.contains("not a valid phone number")) {
          return MsgProcessResponse.failure(Status.FAILED, String.format(INVALID_PHONE_NO_ERR, contact));
        } else if (error.contains("not currently reachable")) {
          return MsgProcessResponse.failure(Status.FAILED, String.format(PHONE_NO_NOT_REACHABLE_ERR, contact));
        }
      }
      return MsgProcessResponse.failure(Status.EXCEPTION, exception);
    }
  }
}
