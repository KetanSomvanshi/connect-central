package com.radix.connect.service.message.sender.impl.api.amazon;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.AmazonSimpleEmailServiceException;
import com.amazonaws.services.simpleemail.model.MessageTag;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.amazonaws.services.simpleemail.model.SendRawEmailResult;
import com.google.gson.JsonArray;
import com.radix.connect.core.annotation.MsgSender;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.SenderAPI;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.message.sender.Sender;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.SenderConfig;
import com.radix.connect.core.model.response.MsgProcessResponse;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.RateLimiter.Permission;
import com.radix.connect.core.util.Validator;
import com.radix.connect.service.config.AccountService;
import com.radix.connect.service.message.SenderLogService;
import com.radix.connect.service.message.sender.impl.AbstractSender;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Properties;
import javax.mail.Session;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author kiran.chokhande
 */
@MsgSender(sender_api = SenderAPI.AMAZON_API, modality = Modality.EMAIL)
public final class EmailSender extends AbstractSender implements Sender {

  private volatile String fromEmailAddress;
  private volatile AmazonSimpleEmailService amazonSimpleEmailService;

  private final Session defaultSession = Session.getDefaultInstance(new Properties());

  @Autowired
  public EmailSender(AccountService accountService, SenderLogService senderLogService) {
    super(Modality.EMAIL, accountService, senderLogService);
  }

  @Override
  public final void configure(final SenderConfig senderConfig) {

    String configuration = senderConfig == null ? null : senderConfig.getConfig_json();
    Validator.requireNonEmpty(configuration, "Amazon SES configuration must be required");

    JsonUtil smtpConfig = new JsonUtil(configuration);

    String accessKey = smtpConfig.getValue("access_key");
    String secretKey = smtpConfig.getValue("secret_key");
    String fromEmail = smtpConfig.getValue("from_email");
    Validator.requireNonEmpty(accessKey, "Amazon access key must be required");
    Validator.requireNonEmpty(secretKey, "Amazon secret key must be required");
    Validator.requireNonEmpty(fromEmail, "From email address must be required");

    super.messagingRateLimiter.setMaxRate(senderConfig.getMessaging_rate());
    AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

    fromEmailAddress = fromEmail.trim();
    amazonSimpleEmailService = AmazonSimpleEmailServiceClientBuilder.standard()
        .withRegion(Regions.US_EAST_1)
        .withCredentials(new AWSStaticCredentialsProvider(credentials)).build();
  }

  @Override
  public final MsgProcessResponse sendOutboundMessage(Message message, boolean isResponseMessage) {

    Validator.requireNonEmpty(message.getContact(), "Email address must be required");
    if (!Validator.isValidContact(message.getModality(), message.getContact())) {
      return MsgProcessResponse
          .failure(Status.FAILED, String.format(INVALID_EMAIL_ID_ERR, message.getContact()));
    }

    try {

      JsonUtil messageJson = new JsonUtil(message.getMessage());
      JsonUtil attachment = new JsonUtil(messageJson.getAsJsonO("attachment"));
      String s3BucketName = attachment.getValue("s3_bucket_name");
      JsonArray attachmentS3FilePaths = attachment.getAsJsonA("s3_file_paths");

      String senderName = messageJson.getValue("sender_name");
      String toEmailAddress = Validator.sanitizeContact(modality, message.getContact());
      MimeMessageBuilder messageBuilder = new MimeMessageBuilder(defaultSession,
          senderName + " <" + fromEmailAddress + ">", toEmailAddress);

      messageBuilder.setSubject(messageJson.getValue("subject"));
      messageBuilder.setCC(messageJson.getValue("cc"));
      messageBuilder.setReplyTo(messageJson.getValue("reply_to"));
      messageBuilder.setBody(messageJson.getValue("message"),
          messageBuilder.buildAttachmentFiles(s3BucketName, attachmentS3FilePaths));

      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      messageBuilder.build().writeTo(outputStream);
      RawMessage rawMsg = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));

      SendRawEmailRequest sendRawEmailRequest = new SendRawEmailRequest(rawMsg);
      sendRawEmailRequest.withConfigurationSetName("CONNECT_CENTRAL_EMAIL_TRACKER").withTags(
          new MessageTag().withName("account_id").withValue(message.getAccount_id()),
          new MessageTag().withName("message_id").withValue(message.getMessage_id())
      );

      SendRawEmailResult sendEmailResult;
      try (Permission permission = super.messagingRateLimiter.acquire()) {
        sendEmailResult = amazonSimpleEmailService.sendRawEmail(sendRawEmailRequest);
      }

      if (sendEmailResult != null && !Validator.isEmpty(sendEmailResult.getMessageId())) {
        return MsgProcessResponse.success(Status.SENT, sendEmailResult.getMessageId());
      } else {
        return MsgProcessResponse.failure(Status.FAILED, "Failed to send email");
      }
    } catch (Exception exception) {
      RxLogger.logException("Amazon SES API Request Error", exception);

      String error = exception.getMessage(), contact = message.getContact();
      if (exception instanceof AmazonSimpleEmailServiceException && error != null) {
        if (error.contains("security token included in the request is invalid")) {
          return MsgProcessResponse.failure(Status.FAILED, AMAZON_SECURITY_TOKEN_ERR);
        } else if (error.contains("Invalid domain name")) {
          return MsgProcessResponse.failure(Status.FAILED, String.format(INVALID_EMAIL_ID_ERR, contact));
        }
      }
      return MsgProcessResponse.failure(Status.EXCEPTION, exception);
    }
  }
}
