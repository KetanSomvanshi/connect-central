package com.radix.connect.service.message.sender.impl.api.interfax;

import com.radix.connect.core.annotation.MsgSender;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.SenderAPI;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.message.sender.Sender;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.SenderConfig;
import com.radix.connect.core.model.response.MsgProcessResponse;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.RateLimiter.Permission;
import com.radix.connect.core.util.Validator;
import com.radix.connect.service.config.AccountService;
import com.radix.connect.service.message.SenderLogService;
import com.radix.connect.service.message.sender.impl.AbstractSender;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import net.interfax.rest.client.InterFAX;
import net.interfax.rest.client.domain.APIResponse;
import net.interfax.rest.client.domain.OutboundFaxStructure;
import net.interfax.rest.client.impl.DefaultInterFAXClient;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author kiran.chokhande
 */
@MsgSender(sender_api = SenderAPI.INTERFAX_API, modality = Modality.FAX)
public final class FaxSender extends AbstractSender implements Sender {

  private volatile InterFAX interFAXClient;

  private static final String UNKNOWN_ERROR = "Unknown api error";

  @Autowired
  public FaxSender(AccountService accountService, SenderLogService senderLogService) {
    super(Modality.FAX, accountService, senderLogService);
  }

  @Override
  public final void configure(final SenderConfig senderConfig) {

    String configuration = senderConfig == null ? null : senderConfig.getConfig_json();
    Validator.requireNonEmpty(configuration, "Fax configuration must be required");

    JsonUtil config = new JsonUtil(configuration);
    String username = config.getValue("username");
    String password = config.getValue("password");
    Validator.requireNonEmpty(username, "Username must be required");
    Validator.requireNonEmpty(password, "Password must be required");

    super.messagingRateLimiter.setMaxRate(senderConfig.getMessaging_rate());
    this.interFAXClient = new DefaultInterFAXClient(username, password);
  }

  @Override
  public final MsgProcessResponse fetchStatus(Message message) {

    if (Validator.isEmpty(message.getMsg_sid()) || message.getProcessed_ts() == null) {
      return null;
    }

    Status msgStatus = null;
    String errorDesc = null;
    try {

      OutboundFaxStructure faxStructure;
      faxStructure = this.interFAXClient.getOutboundFaxRecord(message.getMsg_sid());
      int statusCode = faxStructure == null ? -1 : faxStructure.getStatus();
      switch (statusCode < 0 ? -1 : statusCode) {
        case 0:
          msgStatus = Status.DELIVERED;
          break;
        case 6021:
        case 6029:
          msgStatus = Status.REJECTED;
          errorDesc = "Fax rejected.";
          break;
        case 403:
          msgStatus = Status.CANCELED;
          errorDesc = "Fax manually canceled";
          break;
        case 263:
        case 3931:
        case 3933:
        case 6017:
        case 3937:
        case 8025:
          msgStatus = Status.BUSY;
          errorDesc = "Destination Fax machine busy";
          break;
        case 8021:
        case 3935:
        case 6018:
          msgStatus = Status.NO_ANSWER;
          errorDesc = "No answer (might be out of paper)";
          break;
        case 3211:
        case 3220:
        case 3225:
        case 3231:
        case 3233:
        case 3264:
        case 3267:
        case 3269:
        case 6088:
        case 6095:
        case 6097:
        case 6099:
        case 6100:
          msgStatus = Status.UNDELIVERED;
          errorDesc = "Fax machine incompatibility";
          break;
        case 3224:
          msgStatus = Status.UNDELIVERED;
          errorDesc = "The remote fax machine failed to respond";
          break;
        case 8010:
          msgStatus = Status.UNDELIVERED;
          errorDesc = "The remote fax machine hung up before receiving fax";
          break;
        default:
          msgStatus = statusCode < 0 ? null : Status.FAILED;
          errorDesc = statusCode < 0 ? null : "Failed to send Fax (internal system error)";
          break;
      }
    } catch (Exception exception) {
      RxLogger.logException("InterFax API Request Error", exception);
    }

    return message.getProcessed_ts() == null || msgStatus == null ? null
        : (msgStatus.isSuccess() ? MsgProcessResponse.success(msgStatus)
            : MsgProcessResponse.failure(msgStatus, errorDesc)).withMsgSid(message.getMsg_sid());
  }

  @Override
  public final MsgProcessResponse sendOutboundMessage(Message message, boolean isResponseMessage) {

    Validator.requireNonEmpty(message.getContact(), "Fax number must be required");
    if (!Validator.isValidContact(message.getModality(), message.getContact())) {
      return MsgProcessResponse
          .failure(Status.FAILED, String.format(INVALID_FAX_NO_ERR, message.getContact()));
    }

    try {

      APIResponse response;
      try (final Permission permission = super.messagingRateLimiter.acquire()) {
        String faxNo = Validator.sanitizeContact(modality, message.getContact());
        String body = new JsonUtil(message.getMessage()).getValue("message");
        response = this.interFAXClient.sendFax(faxNo, new InputStream[]{
            new ByteArrayInputStream((body == null ? "" : body).getBytes())
        }, new String[]{"text/html"});
      }

      int statusCode = response == null ? 0 : response.getStatusCode();
      if (statusCode == 200 || statusCode == 201) {
        List<Object> list = response.getHeaders().get("Location");
        String location = Validator.isEmpty(list) ? "" : (String) list.get(0);
        String msgSid = location.substring(location.lastIndexOf('/') + 1);

        return MsgProcessResponse.success(Status.SENT, msgSid);
      } else {
        String errMsg = response == null ? null : response.getResponseBody();
        return MsgProcessResponse.failure(Status.FAILED, Validator.coalesce(errMsg, UNKNOWN_ERROR));
      }
    } catch (Exception exception) {
      RxLogger.logException("InterFax API Request Error", exception);
      return MsgProcessResponse.failure(Status.EXCEPTION, exception);
    }
  }
}
