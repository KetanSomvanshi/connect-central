package com.radix.connect.service.message;

import com.radix.connect.core.Cache;
import com.radix.connect.core.constant.Constant.CacheGroupName;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.model.SenderLog;
import com.radix.connect.core.util.SmartSearch;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;
import com.radix.connect.repository.SenderLogRepository;
import com.radix.connect.service.cache.CacheManager;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class SenderLogService {

  private final SenderLogRepository senderLogRepository;
  private final Cache<SenderLog> textSenderLogCache, voiceSenderLogCache;

  private final static String FAILED_TO_UPDATE_SENDER_LOG = "Failed to log recent sender for Account Id:%s and Phone No:%s";

  @Autowired
  public SenderLogService(SenderLogRepository senderRepository, CacheManager cacheManager) {
    this.senderLogRepository = senderRepository;
    this.textSenderLogCache = cacheManager.createCache(CacheGroupName.RECENT_TEXT_SENDER_LOG);
    this.voiceSenderLogCache = cacheManager.createCache(CacheGroupName.RECENT_VOICE_SENDER_LOG);
    this.loadRecentSenderLogsInCache();
  }

  private Cache<SenderLog> getCacheFor(final Modality modality) {
    Cache<SenderLog> cache = modality == Modality.TEXT ? textSenderLogCache
        : modality == Modality.VOICE ? voiceSenderLogCache : null;
    return Validator.requireNonNull(cache, "Found unsupported modality: " + modality);
  }

  private void loadRecentSenderLogsInCache() {

    textSenderLogCache.clearCache();
    voiceSenderLogCache.clearCache();
    List<SenderLog> senderLogs = senderLogRepository.selectRecentSenderLogs();
    if (senderLogs != null && !senderLogs.isEmpty()) {
      senderLogs.stream().filter(Objects::nonNull).forEach(log -> {
        switch (log.getModality()) {
          case TEXT:
            textSenderLogCache
                .put(Validator.sanitizeContact(log.getModality(), log.getTo_phone()), log);
            break;
          case VOICE:
            voiceSenderLogCache
                .put(Validator.sanitizeContact(log.getModality(), log.getTo_phone()), log);
            break;
        }
      });
    }
  }

  public final String getLastSenderAccountId(Modality modality, String toPhoneNo) {

    SenderLog senderLog = getRecentSenderLogByPhone(modality, toPhoneNo);
    return senderLog == null ? null : senderLog.getAccount_id();
  }

  public final SenderLog getRecentSenderLogByPhone(Modality modality, String toPhoneNo) {

    Cache<SenderLog> senderLogCache = getCacheFor(modality);

    String phoneNoToSearch = toPhoneNo.replaceAll("\\D", "");
    SenderLog senderLog = senderLogCache.get(phoneNoToSearch);
    if (senderLog == null) {
      senderLog = senderLogRepository.selectRecentSenderLogByPhone(modality, toPhoneNo);
      if (senderLog != null) {
        senderLogCache.put(phoneNoToSearch, senderLog);
      }
    }
    return senderLog;
  }

  public final List<SenderLog> getRecentSenderLogsByPhrase(Modality modality, String phrase) {

    List<SenderLog> senderLogs = getCacheFor(modality).list();
    return senderLogs == null || senderLogs.isEmpty() ? null :
        SmartSearch.extract(phrase, senderLogs, l -> l == null ? null
            : new String[]{l.getTo_phone(), l.getFrom_phone(), l.getAccount_id()}, 30, 10);
  }

  public final void updateRecentSenderLog(final SenderLog senderLog) {

    String toPhone = senderLog.getTo_phone().replaceAll("\\D", "");
    if (senderLogRepository.updateRecentSenderLog(senderLog)) {
      getCacheFor(senderLog.getModality()).put(toPhone, senderLog);
    } else {
      throw new ConnectException(
          String.format(FAILED_TO_UPDATE_SENDER_LOG, senderLog.getAccount_id(), toPhone));
    }
  }
}
