package com.radix.connect.service.message.processor;

import com.google.gson.JsonObject;
import com.radix.connect.core.Configurable;
import com.radix.connect.core.constant.Constant.WorkSpaceName;
import com.radix.connect.core.constant.Direction;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.message.queue.QueueManager;
import com.radix.connect.core.message.sender.SenderManager;
import com.radix.connect.core.model.Account;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.QueueMsg;
import com.radix.connect.core.model.response.MsgProcessResponse;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import com.radix.connect.core.util.WorkManager;
import com.radix.connect.core.util.WorkManager.WorkSpace;
import com.radix.connect.core.util.WorkManager.WorkSpace.InfiniteWork;
import com.radix.connect.core.util.WorkManager.WorkSpace.Work;
import com.radix.connect.error.ConnectException;
import com.radix.connect.repository.MessageRepository;
import com.radix.connect.service.config.AccountService;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class MessageProcessor implements Configurable<Integer> {

  private final QueueManager queueManager;
  private final SenderManager senderManager;
  private final AccountService accountService;
  private final MessageRepository messageRepository;

  private final WorkSpace msgConsumersWorkSpace;
  private final WorkSpace msgProcessorWorkSpace;

  private static final int START_WORKING_HOURS = 6, CLOSE_WORKING_HOURS = 20;
  private static final Status[] IN_PROCESS_STATUSES = {Status.SENT, Status.IN_PROCESS};

  private static final String PROCESSING_QUEUE_MSG = "Processing Queue Message => Account id: %s, Message Id: %s";
  private static final String PROCESSING_MSG = "Processing Message => Direction: %s,  Account id: %s, Message Id: %s";
  private static final String MSG_LOG_NOT_FOUND_ERROR = "No record found in message log for Account Id: %s and Message Id: %s";
  private static final String TIME_BLOCK_ER = "Can not send message between 9PM to 6AM except urgent messages";
  private static final String MSG_PROCESS_FAILED = "Message processing failed for Account ID:%s, Message ID:%s, error: %s";
  private static final String MSG_PROCESS_SUCCESS = "Message processed successfully. Account Id: %s, Message Id: %s";
  private static final String UNKNOWN_ERROR_MSG = "Unknown error! Please check application logs for more details";
  private static final String BEFORE_TS_EXPIRED_ERROR = "Can not send message at this time, max waiting time has expired";

  @Autowired
  public MessageProcessor(final QueueManager queueManager, final SenderManager senderManager,
      final AccountService accountService, final MessageRepository messageRepository) {

    this.queueManager = queueManager;
    this.senderManager = senderManager;
    this.accountService = accountService;
    this.messageRepository = messageRepository;
    this.msgConsumersWorkSpace = WorkManager.workSpace(WorkSpaceName.MSG_CONSUMERS);
    this.msgProcessorWorkSpace = WorkManager.workSpace(WorkSpaceName.MSG_PROCESSOR);

    List<Message> msgList = messageRepository.selectMessage(IN_PROCESS_STATUSES);
    if (msgList != null && !msgList.isEmpty()) {
      msgList.stream().filter(Objects::nonNull)
          .forEach(this::submitInProcessingMessageToFetchAndUpdateMsgStatus);
    }
  }

  @Override
  public final void configure(final Integer totalMaxNumberOfMessageQueueConsumers) {

    /* Stop all previous submitted workers */
    msgConsumersWorkSpace.stopAllWorkers();

    int totalNumberOfConsumers = 0;
    if (totalMaxNumberOfMessageQueueConsumers != null) {
      totalNumberOfConsumers = totalMaxNumberOfMessageQueueConsumers;
    }

    int consumerNumber = 0;
    while (++consumerNumber <= totalNumberOfConsumers && consumerNumber <= 100) {

      final String id = "QUEUE_MSG_CONSUMER_$" + consumerNumber;
      msgConsumersWorkSpace.submitWork(queueManager.consumer(id, messageList -> {
        if (messageList != null && !messageList.isEmpty()) {
          messageList.stream().filter(Objects::nonNull).forEach(queueMessage ->
              msgProcessorWorkSpace.submitWork(new Work<QueueMsg>(queueMessage) {
                @Override
                protected final void doWork(final QueueMsg queueMsg) {
                  processQueueMessage(queueMsg);
                  queueManager.delete(queueMsg, (queueMsgToDelete, exc) ->
                      queueManager.delete(queueMsgToDelete, null));
                }
              }));
        }
      }));
    }
  }

  public final void submit(Message message, TimeZone tz, boolean isUrgent, boolean isResponse) {

    if (isUrgent || isResponse || message.getDirection() == Direction.INBOUND) {
      processingMessage(message, isResponse);
    } else {
      String timezone = tz == null ? "EST" : tz.getID();
      JsonObject messageBody = new JsonUtil(message.getMessage()).getAsJsonO();
      String accountId = message.getAccount_id(), messageId = message.getMessage_id();

      queueManager.produce(new QueueMsg(accountId, messageId, timezone, messageBody), (m, e) ->
          queueManager.produce(m, (queue_msg, exception) -> processQueueMessage(queue_msg)));
    }
  }

  private void processQueueMessage(final QueueMsg queueMsg) {

    String accountId = queueMsg.getAccount_id(), messageId = queueMsg.getMessage_id();
    RxLogger.logMessage(String.format(PROCESSING_QUEUE_MSG, accountId, messageId));

    Message message = messageRepository.selectMessage(accountId, messageId);
    if (message == null) {
      RxLogger.logError(String.format(MSG_LOG_NOT_FOUND_ERROR, accountId, messageId));
      return;
    }

    TimeZone timeZone = TimeZone.getTimeZone(queueMsg.getTimezone());
    int hours = Calendar.getInstance(timeZone).get(Calendar.HOUR_OF_DAY);
    if (hours >= START_WORKING_HOURS && hours <= CLOSE_WORKING_HOURS) {
      JsonObject messageBody = queueMsg.getMessage();
      message.setMessage(messageBody == null ? null : messageBody.toString());
      processingMessage(message, false);
    } else {
      RxLogger.logError(String.format(MSG_PROCESS_FAILED, accountId, messageId, TIME_BLOCK_ER));
      updateMessage(message, MsgProcessResponse.failure(Status.TIME_BLOCK, TIME_BLOCK_ER));
    }
  }

  private void processingMessage(final Message message, final boolean isResponseMsg) {

    String accountId = message.getAccount_id(), messageId = message.getMessage_id();

    boolean isOutboundMsg = message.getDirection() == Direction.OUTBOUND;
    if (isOutboundMsg && Calendar.getInstance().after(message.getBefore_ts())) {
      String e = BEFORE_TS_EXPIRED_ERROR;
      RxLogger.logError(String.format(MSG_PROCESS_FAILED, accountId, messageId, e));
      updateMessage(message, MsgProcessResponse.failure(Status.TIME_OUT, e));
      return;
    }

    try {

      Direction dir = message.getDirection();
      RxLogger.logMessage(String.format(PROCESSING_MSG, dir, accountId, messageId));

      MsgProcessResponse response = senderManager.sendMessage(message, isResponseMsg);
      response = response != null ? response
          : MsgProcessResponse.failure(Status.FAILED, UNKNOWN_ERROR_MSG);

      updateMessage(message, response);
      if (response.isSuccess()) {
        RxLogger.logMessage(String.format(MSG_PROCESS_SUCCESS, accountId, messageId));
        if (message.getDirection() == Direction.OUTBOUND) {
          submitInProcessingMessageToFetchAndUpdateMsgStatus(message);
        }
      } else {
        String e = message.getError_desc();
        RxLogger.logError(String.format(MSG_PROCESS_FAILED, accountId, messageId, e));
      }
    } catch (Exception exception) {
      String errorDesc = extractStackTraceAsString(exception);
      String context = String.format(MSG_PROCESS_FAILED, accountId, messageId, "Exc");
      RxLogger.logException(context, exception);
      updateMessage(message, MsgProcessResponse.failure(Status.EXCEPTION, errorDesc));
    }
  }

  private String extractStackTraceAsString(final Throwable throwable) {

    final StringBuilder error_builder = new StringBuilder();
    if (throwable != null) {
      error_builder.append(throwable.getClass().getName());
      error_builder.append(":").append(throwable.getMessage());
      for (StackTraceElement trace : throwable.getStackTrace()) {
        error_builder.append("\n at ").append(trace);
      }
    }
    return throwable == null ? null : error_builder.toString().trim();
  }

  private void submitInProcessingMessageToFetchAndUpdateMsgStatus(final Message msg) {

    msgProcessorWorkSpace.submitWork(new InfiniteWork<>(msg.getMessage_id(), msg) {

      final AtomicInteger cnt = new AtomicInteger();
      final String accountId = msg.getAccount_id(), messageId = msg.getMessage_id();

      @Override
      protected final void doWork(final Message message) {

        Message messageDB = messageRepository.selectMessage(accountId, messageId);

        boolean terminateWorkerCondition = messageDB.getModality() == Modality.EMAIL
            || cnt.getAndIncrement() > 120 || !messageDB.getMsg_status().isInProcess();
        if (!super.terminateOnCondition(terminateWorkerCondition)) {

          MsgProcessResponse response;
          try {
            response = senderManager.fetchStatus(messageDB);
          } catch (Exception exception) {
            String details = extractStackTraceAsString(exception);
            response = MsgProcessResponse.failure(Status.EXCEPTION, details);
          }

          Status status = response == null ? null : response.getMsg_status();
          if (status != null && status != messageDB.getMsg_status()) {
            updateMessage(messageDB, response);
          }
          sleepOnCondition(messageDB.getMsg_status().isInProcess(), 60);
        }
      }
    });
  }

  public final void updateMessage(Message message, MsgProcessResponse msgProcessResponse) {

    if (!Validator.isEmpty(msgProcessResponse.getMsg_sid())) {
      message.setMsg_sid(msgProcessResponse.getMsg_sid());
    }
    if (message.getProcessed_ts() == null) {
      message.setProcessed_ts(new Timestamp(System.currentTimeMillis()));
    }
    if (msgProcessResponse.getMsg_status() != null) {
      message.setMsg_status(msgProcessResponse.getMsg_status());
    }

    Status status = message.getMsg_status();
    boolean isOutboundMsg = message.getDirection() == Direction.OUTBOUND;

    message.setError_desc(msgProcessResponse.getError_desc());
    if (messageRepository.updateMessage(message)) {
      if (isOutboundMsg && status != null && !status.isInProcess()) {
        Account account = accountService.getAccountById(message.getAccount_id());
        DashApiExecutor.sendMsgStatusUpdateReqToDash(account, message);
      }
    } else {
      throw new ConnectException("Failed to update message status into message log.");
    }
  }
}
