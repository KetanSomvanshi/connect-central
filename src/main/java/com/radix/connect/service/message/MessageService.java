package com.radix.connect.service.message;

import com.google.gson.JsonObject;
import com.radix.connect.core.constant.Direction;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.SenderLog;
import com.radix.connect.core.model.response.MsgProcessResponse;
import com.radix.connect.core.model.response.MsgStatus;
import com.radix.connect.core.util.IdGenerator;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;
import com.radix.connect.error.RecordNotFoundException;
import com.radix.connect.repository.MessageRepository;
import com.radix.connect.service.message.processor.MessageProcessor;
import java.sql.Timestamp;
import java.util.TimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class MessageService {

  private final MessageProcessor messageProcessor;
  private final MessageRepository messageRepository;
  private final SenderLogService senderLogService;

  private static final String NO_SENDER_LOG_ERR = "No recent message sender log found for contact: %s, communication mode: %s";

  @Autowired
  public MessageService(MessageRepository repository, MessageProcessor processor, SenderLogService logService) {
    this.messageProcessor = processor;
    this.senderLogService = logService;
    this.messageRepository = repository;
  }

  public final Message getMessageById(final String accountId, final String messageId) {

    Validator.requireNonEmpty(accountId, "Account ID must be required");
    Validator.requireNonEmpty(messageId, "Message ID must be required");

    Message message = messageRepository.selectMessage(accountId, messageId);
    if (message == null) {
      throw new RecordNotFoundException("No record found for message id: " + messageId);
    }

    if (message.getMsg_status() == Status.EXCEPTION) {
      message.setError_desc("Application error! Please check application logs for more detail");
    }
    return message;
  }

  public final MsgStatus getMessageStatus(final String accountId, final String messageId) {

    Message message = getMessageById(accountId, messageId);
    Timestamp processedTsz = message.getProcessed_ts();
    return new MsgStatus(messageId, message.getMsg_status(), message.getError_desc(), processedTsz);
  }

  public final void updateMsgStatus(Modality modality, String msgSid, Status status, String error) {

    if (modality != null && !Validator.isEmpty(msgSid) && status != null && !status.isInProcess()) {

      Message message = messageRepository.selectMessageByMsgSid(modality, msgSid);
      if (message != null && status.isSuccess()) {
        messageProcessor.updateMessage(message, MsgProcessResponse.success(status));
      } else if (message != null) {
        messageProcessor.updateMessage(message, MsgProcessResponse.failure(status, error));
      }
    }
  }

  public void processInboundMessage(String msg_sid, Modality modality, JsonObject body) {

    modality = Validator.requireNonNull(modality, "Communication mode must be required");

    if (modality != Modality.TEXT) {
      throw new ConnectException("Can not process inbound message for modality:" + modality);
    }

    String contact = new JsonUtil(body).getValue("contact");
    Validator.requireNonEmpty(contact, "User contact must be required");
    SenderLog senderLog = senderLogService.getRecentSenderLogByPhone(modality, contact);
    if (senderLog == null) {
      throw new RecordNotFoundException(String.format(NO_SENDER_LOG_ERR, contact, modality));
    }

    Direction direction = Direction.INBOUND;
    String messageId = IdGenerator.createMessageId(modality);
    String accountId = senderLog.getAccount_id(), prevMessageId = senderLog.getMessage_id();

    Message message = new Message(accountId, modality, messageId, direction, body.toString());
    message.setMsg_sid(msg_sid);
    message.setMsg_status(Status.RECEIVED);
    message.setPrev_message_id(prevMessageId);

    if (messageRepository.insertMessage(message, 1440)) {
      messageProcessor.submit(message, null, true, false);
    } else {
      throw new ConnectException("Application error! Please check application logs for more detail");
    }
  }

  public final MsgStatus processOutboundMessage(String accountId, Modality modality, JsonObject body,
      String timezone, int maxTimeouts, boolean sendImmediately, boolean isReplyMsg) {

    String unicode, msg = new JsonUtil(body).getValue("message");
    if (modality == Modality.TEXT && !Validator.isEmpty(unicode = Validator.findUnicodeChar(msg))) {
      throw new ConnectException("Message contains unsupported unicode characters: " + unicode);
    }

    Direction direction = Direction.OUTBOUND;
    String messageId = IdGenerator.createMessageId(modality);
    TimeZone timeZone = TimeZone.getTimeZone(timezone == null ? "EST" : timezone);

    Message message = new Message(accountId, modality, messageId, direction, body.toString());
    message.setMsg_status(sendImmediately || isReplyMsg ? Status.IN_PROCESS : Status.QUEUED);

    if (messageRepository.insertMessage(message, maxTimeouts)) {
      messageProcessor.submit(message, timeZone, sendImmediately, isReplyMsg);
      return getMessageStatus(accountId, messageId);
    } else {
      throw new ConnectException("Application error! Please check application logs for more detail");
    }
  }
}
