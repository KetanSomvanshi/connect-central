package com.radix.connect.service.message.processor;

import com.google.gson.JsonObject;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.logger.RxLogger.RequestLog;
import com.radix.connect.core.model.Account;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.response.MsgProcessResponse;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

/**
 * @author kiran.chokhande
 */
public final class DashApiExecutor {

  private static final HttpClientBuilder DASH_HTTP_CLIENT_BUILDER = HttpClientBuilder.create();

  private static final String DASH_INBOUND_WEB_HOOK_URL = "%s/rs/connect/message/%s/receive";
  private static final String DASH_STATUS_CALLBACK_URL = "%s/rs/connect/message/%s/status/update";

  private static final String APPLICATION_ERROR = "Application error! Please check application logs for more detail";

  private DashApiExecutor() {
  }

  public static MsgProcessResponse sendMsgStatusUpdateReqToDash(Account account, Message message) {

    String accountId = message.getAccount_id(), messageId = message.getMessage_id();
    String targetDashConnectInboundMsgUrlToProcessInboundMsg = String
        .format(DASH_STATUS_CALLBACK_URL, account.getApp_base_url(), messageId);

    JsonObject reqPayload = new JsonObject();
    reqPayload.addProperty("account_id", accountId);
    reqPayload.addProperty("modality", String.valueOf(message.getModality()));
    reqPayload.addProperty("status", String.valueOf(message.getMsg_status()));
    reqPayload.addProperty("error_desc",
        message.getMsg_status() != Status.EXCEPTION ? message.getError_desc() : APPLICATION_ERROR);

    HttpPost request = new HttpPost(targetDashConnectInboundMsgUrlToProcessInboundMsg);
    request.setHeader("Authorization", account.getAuth_token());
    request.addHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType());
    request.setEntity(new StringEntity(reqPayload.toString(), ContentType.APPLICATION_JSON));

    String context = "Send Message Status Callback Request To Dash";
    try (final RequestLog requestLog = RxLogger.logRequest(accountId, context, request)) {

      execute(request, requestLog);
      return MsgProcessResponse.success(message.getMsg_status(), message.getMsg_sid());
    } catch (ConnectException exc) {
      String errorMsg = Validator.coalesce(exc.getMessage(), "Unknown error");
      return MsgProcessResponse.failure(Status.FAILED, errorMsg, message.getMsg_sid());
    } catch (Exception error) {
      RxLogger.logException("Dash Message Status Callback API Call", error);
      return MsgProcessResponse.failure(Status.EXCEPTION, error, message.getMsg_sid());
    }
  }

  public static MsgProcessResponse sendInboundMessageReqToDash(Account account, Message message) {

    String accountId = message.getAccount_id(), messageId = message.getPrev_message_id();
    String targetDashConnectInboundMsgUrlToProcessInboundMsg = String
        .format(DASH_INBOUND_WEB_HOOK_URL, account.getApp_base_url(), messageId);

    JsonUtil messageBody = new JsonUtil(message.getMessage());
    JsonObject reqPayload = new JsonObject();
    reqPayload.addProperty("account_id", accountId);
    reqPayload.addProperty("modality", String.valueOf(message.getModality()));
    reqPayload.addProperty("contact", messageBody.getValue("contact"));
    reqPayload.addProperty("message", messageBody.getValue("message"));

    HttpPost request = new HttpPost(targetDashConnectInboundMsgUrlToProcessInboundMsg);
    request.setHeader("Authorization", account.getAuth_token());
    request.addHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType());
    request.setEntity(new StringEntity(reqPayload.toString(), ContentType.APPLICATION_JSON));

    String context = "Send Inbound Message Request To Dash";
    try (final RequestLog requestLog = RxLogger.logRequest(accountId, context, request)) {

      execute(request, requestLog);
      return MsgProcessResponse.success(message.getMsg_status(), message.getMsg_sid());
    } catch (ConnectException exc) {
      String errorMsg = Validator.coalesce(exc.getMessage(), "Unknown error");
      return MsgProcessResponse.failure(Status.FAILED, errorMsg, message.getMsg_sid());
    } catch (Exception error) {
      RxLogger.logException("Dash Inbound Message API Call", error);
      return MsgProcessResponse.failure(Status.EXCEPTION, error, message.getMsg_sid());
    }
  }

  private static void execute(HttpPost httpPostRequest, RequestLog requestLog) throws Exception {

    try (CloseableHttpClient closeableHttpClients = DASH_HTTP_CLIENT_BUILDER.build()) {

      HttpResponse httpResponse = closeableHttpClients.execute(httpPostRequest);
      int httpResponseStatusCode = httpResponse.getStatusLine().getStatusCode();

      String apiResponse;
      try {

        HttpEntity entity = httpResponse.getEntity();
        apiResponse = entity == null ? null : EntityUtils.toString(entity, "UTF-8");
        requestLog.setResponse(httpResponseStatusCode, apiResponse);
      } catch (Exception exception) {
        apiResponse = exception.getMessage();
        requestLog.setResponse(httpResponseStatusCode, exception.getMessage());
      }

      if (!(httpResponseStatusCode >= 200 && httpResponseStatusCode < 400)) {
        throw new ConnectException(Validator.coalesce(apiResponse, "Unknown api error"));
      }
    }
  }
}
