package com.radix.connect.service.cache;

import com.radix.connect.core.Cache;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import javax.inject.Singleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
@Singleton
public final class CacheManager {

  private final RedisTemplate<Object, Object> redisServerTemplate;
  private final static String CACHE_GROUP_NAME_FORMAT = "_%s_CACHE_GROUP$";

  public CacheManager(@Autowired final RedisTemplate<Object, Object> redisServerTemplate) {
    this.redisServerTemplate = Validator
        .requireNonNull(redisServerTemplate, "RedisTemplate must be required");
    this.redisServerTemplate.execute((RedisCallback<Object>) connection -> {
      connection.flushAll();
      return null;
    });
  }

  public final <V extends Serializable> CacheImpl<V> createCache(final String cacheGroup) {
    return new CacheImpl<>(cacheGroup);
  }

  /**
   * @author kiran.chokhande
   */
  private final class CacheImpl<V extends Serializable> implements Cache<V> {

    private final String group;
    private final HashOperations<Object, String, V> redisStorage;

    private CacheImpl(final String cacheGroupName) {
      Validator.requireNonEmpty(cacheGroupName, "Cache group name must be required");
      this.redisStorage = redisServerTemplate.opsForHash();
      this.group = String.format(CACHE_GROUP_NAME_FORMAT, cacheGroupName.trim());
    }

    @Override
    public final void put(String key, V value) {
      Validator.requireNonEmpty(key, "Cache key must be required");
      if (value != null) {
        redisStorage.put(group, key, value);
      }
    }

    @Override
    public final boolean isEmpty() {
      return keys().isEmpty();
    }

    @Override
    public final boolean contains(String key) {
      return !Validator.isEmpty(key) && redisStorage.hasKey(group, key);
    }

    @Override
    public final Set<String> keys() {
      return redisStorage.keys(group);
    }

    @Override
    public final List<V> list() {
      return redisStorage.values(group);
    }

    @Override
    public final Map<String, V> map() {
      return redisStorage.entries(group);
    }

    @Override
    public final V get(String key) {
      return Validator.isEmpty(key) ? null : redisStorage.get(group, key);
    }

    @Override
    public V computeIfAbsent(String key, Function<String, V> valueFunction) {

      Validator.requireNonEmpty(key, "Cache key must be required");

      V v = get(key);
      if (v == null && valueFunction != null && (v = valueFunction.apply(key)) != null) {
        put(key, v);
      }
      return v;
    }

    @Override
    public final void remove(String... keys) {
      remove(keys == null ? null : Arrays.asList(keys));
    }

    @Override
    public final void remove(Collection<String> keys) {
      if (!Validator.isEmpty(keys)) {
        redisStorage.delete(group, keys.toArray());
      }
    }

    @Override
    public final void clearCache() {
      redisStorage.getOperations().delete(group);
    }
  }
}