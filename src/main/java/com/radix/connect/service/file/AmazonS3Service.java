package com.radix.connect.service.file;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import java.io.File;
import org.apache.commons.io.FileUtils;

/**
 * @author kiran.chokhande
 */
public final class AmazonS3Service {

  private static volatile AmazonS3 amazonS3;

  public static void configureAmazonS3(final String amazonS3Configurations) {

    final JsonUtil s3Config = new JsonUtil(amazonS3Configurations);

    String accessKey = s3Config.getValue("access_key");
    String secretKey = s3Config.getValue("secret_key");
    accessKey = Validator.requireNonEmpty(accessKey, "Amazon access key must required");
    secretKey = Validator.requireNonEmpty(secretKey, "Amazon secret key must required");

    BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKey, secretKey);
    AmazonS3Service.amazonS3 = AmazonS3ClientBuilder.standard()
        .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials)).build();
  }

  public static File downloadFile(final String s3BucketName, final String s3FileKey) {

    File localFile = new File(FileUtils.getTempDirectoryPath() + s3FileKey);
    amazonS3.getObject(new GetObjectRequest(s3BucketName, s3FileKey), localFile);
    return localFile;
  }
}
