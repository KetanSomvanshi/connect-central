package com.radix.connect.service.aop;

import com.radix.connect.core.annotation.LogRequest;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.logger.RxLogger.RequestLog;
import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author kiran.chokhande
 */
@Aspect
@Service
public final class AspectService {

  @Around("@annotation(com.radix.connect.core.annotation.LogRequest)")
  public final Object autoRequestLogger(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

    MethodSignature method = (MethodSignature) proceedingJoinPoint.getSignature();
    LogRequest logRequestAnnotation = method.getMethod().getAnnotation(LogRequest.class);

    HttpServletRequest request = null;
    try {
      RequestAttributes requestAttribute = RequestContextHolder.currentRequestAttributes();
      request = ((ServletRequestAttributes) requestAttribute).getRequest();
    } catch (Exception ignored) {
    }

    try (final RequestLog requestLog = RxLogger
        .logRequest(logRequestAnnotation == null ? null : logRequestAnnotation.value(), request)) {
      return proceedingJoinPoint.proceed();
    }
  }
}
