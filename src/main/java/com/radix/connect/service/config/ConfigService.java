package com.radix.connect.service.config;

import com.radix.connect.core.Cache;
import com.radix.connect.core.constant.Constant.CacheGroupName;
import com.radix.connect.core.constant.Constant.SystemConfigKey;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.QueueType;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.message.queue.QueueManager;
import com.radix.connect.core.message.sender.SenderManager;
import com.radix.connect.core.model.QueueConfig;
import com.radix.connect.core.model.SenderConfig;
import com.radix.connect.core.model.SenderMapping;
import com.radix.connect.core.model.SystemConfig;
import com.radix.connect.core.util.Crypto;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;
import com.radix.connect.error.RecordNotFoundException;
import com.radix.connect.repository.ConfigRepository;
import com.radix.connect.service.cache.CacheManager;
import com.radix.connect.service.file.AmazonS3Service;
import com.radix.connect.service.message.processor.MessageProcessor;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class ConfigService {

  private final QueueManager queueManager;
  private final SenderManager senderManager;
  private final MessageProcessor messageProcessor;
  private final ConfigRepository configRepository;

  private final Cache<SystemConfig> systemConfigCache;

  @Autowired
  public ConfigService(final ConfigRepository configRepository, final CacheManager cacheManager,
      QueueManager queueManager, SenderManager senderManager, MessageProcessor messageProcessor) {

    this.configRepository = configRepository;
    this.queueManager = queueManager;
    this.senderManager = senderManager;
    this.messageProcessor = messageProcessor;
    this.systemConfigCache = cacheManager.createCache(CacheGroupName.SYSTEM_CONFIG);

    RxLogger.configureLoggerToProcessLog(loadLoggerConfiguration());
    AmazonS3Service.configureAmazonS3(loadAmazonS3Configuration());
    this.queueManager.configure(getActiveMessageQueueConfig());
    this.messageProcessor.configure(getNumberOfMessageConsumers());
  }

  private void reconfigureApplicationOnSystemConfUpdate(final String updatedSysConfigKey) {

    switch (updatedSysConfigKey == null ? "<<NULL_KEY>>" : updatedSysConfigKey) {
      case SystemConfigKey.APP_LOGGER_CONFIG:
        RxLogger.configureLoggerToProcessLog(loadLoggerConfiguration());
        break;
      case SystemConfigKey.AMAZON_S3_CONFIG:
        AmazonS3Service.configureAmazonS3(loadAmazonS3Configuration());
        break;
      case SystemConfigKey.MSG_QUEUE_CONSUMER_COUNT:
        messageProcessor.configure(getNumberOfMessageConsumers());
        break;
      case SystemConfigKey.ACTIVE_MSG_QUEUE_TYPE:
        queueManager.configure(getActiveMessageQueueConfig());
        break;
      case SystemConfigKey.AMAZON_MQ_CONFIG:
      case SystemConfigKey.RABBIT_MQ_CONFIG:
        if (updatedSysConfigKey.equals(getActiveMessageQueueType() + "_CONFIG")) {
          this.queueManager.configure(getActiveMessageQueueConfig());
        }
        break;
    }
  }

  private String loadLoggerConfiguration() {
    return getSystemConfigValue(SystemConfigKey.APP_LOGGER_CONFIG);
  }

  private String loadAmazonS3Configuration() {
    return getSystemConfigValue(SystemConfigKey.AMAZON_S3_CONFIG);
  }

  private QueueConfig getActiveMessageQueueConfig() {

    QueueType queueType = getActiveMessageQueueType();
    JsonUtil config = new JsonUtil(getSystemConfigValue(queueType + "_CONFIG"));
    int maxVisibility = config.getAsInt("visibility_timeout_minutes");

    QueueConfig queueConfig = new QueueConfig(queueType);
    queueConfig.setConfig_json(config.getAsString());
    queueConfig.setVisibility_timeout_minutes(maxVisibility < 1 ? 30 : maxVisibility);
    return queueConfig;
  }

  private QueueType getActiveMessageQueueType() {
    return QueueType.forValue(getSystemConfigValue(SystemConfigKey.ACTIVE_MSG_QUEUE_TYPE));
  }

  private int getNumberOfMessageConsumers() {
    String number = getSystemConfigValue(SystemConfigKey.MSG_QUEUE_CONSUMER_COUNT);
    return number == null || number.trim().isEmpty() ? 0 : Integer.parseInt(number.trim());
  }

  public final String getSystemConfigValue(final String key) {
    SystemConfig systemConfig = getSystemConfigByKey(key);
    return systemConfig == null ? null : systemConfig.getValue();
  }

  public final List<SystemConfig> getSystemConfigs(final String... keys) {

    List<SystemConfig> configs = Validator.isEmpty(keys) ? null : systemConfigCache.list();
    if (Validator.isEmpty(configs) || Validator.isEmpty(keys)) {
      systemConfigCache.clearCache();
      configs = configRepository.selectSystemConfigs();
      if (configs != null && !configs.isEmpty()) {
        configs.stream().filter(Objects::nonNull).forEach(systemConfig
            -> systemConfigCache.put(systemConfig.getKey(), systemConfig));
      }
    }
    List<String> keysToReturns = keys == null ? null : Arrays.asList(keys);

    return Validator.isEmpty(configs) || Validator.isEmpty(keys) ? configs
        : configs.stream().filter(sc -> sc != null && keysToReturns.contains(sc.getKey()))
            .collect(Collectors.toList());
  }

  public final SystemConfig getSystemConfigByKey(final String key) {

    return Validator.isEmpty(key) ? null :
        systemConfigCache.computeIfAbsent(key, v -> configRepository.selectSystemConfig(key));
  }

  public final SystemConfig updateSystemConfig(String key, String value, String updUserId) {

    Validator.requireNonEmpty(key, "System config key required to update system config");

    final SystemConfig systemConfig = getSystemConfigByKey(key);
    if (systemConfig == null) {
      throw new RecordNotFoundException("No record found for system config key: " + key);
    }

    value = Validator.isEmpty(value) ? null
        : !systemConfig.isEncrypted() ? value : Crypto.encrypt(value);
    if (configRepository.updateSystemConfig(key, value, updUserId)) {
      try {
        systemConfigCache.remove(key);
        return getSystemConfigByKey(key);
      } finally {
        reconfigureApplicationOnSystemConfUpdate(key);
      }
    } else {
      throw new ConnectException("Failed to update system config, Key: " + key);
    }
  }

  public final List<SenderConfig> getSenderConfigs() {
    return configRepository.selectSenderConfigs();
  }

  public final List<SenderConfig> getSenderConfigs(final Modality modality) {
    return configRepository.selectSenderConfigs(modality);
  }

  public final SenderConfig getSenderConfigById(final String senderId) {
    return senderId == null ? null : configRepository.selectSenderConfig(senderId);
  }

  public final SenderConfig updateSenderConfig(final String senderId, final String senderDesc,
      final int messagingRate, final String configJson, final String updUserId) {

    Validator.requireNonEmpty(senderId, "Sender Id must be required");
    Validator.requireNonEmpty(senderDesc, "Sender description must be required");

    SenderConfig senderConfig = getSenderConfigById(senderId);
    if (senderConfig == null) {
      throw new RecordNotFoundException("No record found for Sender Id: " + senderId);
    }

    senderConfig.setSender_desc(senderDesc);
    senderConfig.setLast_updated_by(updUserId);
    senderConfig.setMessaging_rate(messagingRate <= 0 ? 1 : messagingRate);
    senderConfig.setConfig_json(Validator.isEmpty(configJson) ? null : configJson);

    if (configRepository.updateSenderConfig(senderConfig)) {
      senderConfig = getSenderConfigById(senderId);
      this.senderManager.reload(senderConfig);
      return senderConfig;
    } else {
      throw new ConnectException("Failed to update sender config, Sender Id : " + senderId);
    }
  }

  public final List<SenderMapping> getSenderMappings() {
    return configRepository.selectSenderMappings();
  }

  public final SenderMapping getSenderMappingById(String accountId, Modality modality) {
    return configRepository.selectSenderMapping(accountId, modality);
  }

  public final SenderMapping updateSenderMapping(String accountId, Modality modality,
      String senderId, String updUserId) {

    Validator.requireNonNull(modality, "Modality must be required");
    Validator.requireNonEmpty(senderId, "Sender Id must be required");
    Validator.requireNonEmpty(accountId, "Account Id must be required");

    SenderMapping senderMapping = new SenderMapping(accountId, modality, senderId);
    senderMapping.setLast_updated_by(updUserId);

    if (configRepository.updateSenderMapping(senderMapping)) {
      return getSenderMappingById(accountId, modality);
    } else {
      throw new ConnectException("Failed to update sender mapping, Account Id: " + accountId);
    }
  }
}
