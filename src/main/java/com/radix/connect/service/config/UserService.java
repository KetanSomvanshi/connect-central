package com.radix.connect.service.config;

import com.radix.connect.core.Provider;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.model.AppUser;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;
import com.radix.connect.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class UserService implements Provider<String, AppUser> {

  private final UserRepository userRepository;

  public UserService(@Autowired final UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public final AppUser provide(final String username) {

    try {

      return Validator.isEmpty(username) ? null : getUserById(username);
    } catch (Exception exc) {
      RxLogger.logException("UserService: Load user: " + username, exc);
    }
    return null;
  }

  public final List<AppUser> getUsers() {

    return userRepository.selectUser();
  }

  public final AppUser getUserById(final String userId) {

    return Validator.isEmpty(userId) ? null : userRepository.selectUser(userId);
  }

  public final void createUser(final AppUser user) {

    if (!userRepository.insertUser(user)) {
      throw new ConnectException("Failed to create user");
    }
  }

  public final void updateUser(final AppUser user) {

    if (!userRepository.updateUser(user)) {
      throw new ConnectException("Failed to update user");
    }
  }
}
