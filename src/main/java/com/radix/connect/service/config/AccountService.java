package com.radix.connect.service.config;

import com.radix.connect.core.Cache;
import com.radix.connect.core.Provider;
import com.radix.connect.core.constant.Constant.CacheGroupName;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.model.Account;
import com.radix.connect.core.model.AuthToken;
import com.radix.connect.core.util.IdGenerator;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;
import com.radix.connect.error.RecordNotFoundException;
import com.radix.connect.repository.AccountRepository;
import com.radix.connect.service.cache.CacheManager;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class AccountService implements Provider<String, Account> {

  private final Cache<Account> accountCache;
  private final AccountRepository accountRepository;
  private final static Encoder BASE64_ENCODER = Base64.getEncoder();

  @Autowired
  public AccountService(final AccountRepository accountRepository, CacheManager cacheManager) {
    this.accountRepository = accountRepository;
    this.accountCache = cacheManager.createCache(CacheGroupName.ACCOUNT);
    this.getAccounts();
  }

  @Override
  public final Account provide(final String accountId) {

    try {

      return Validator.isEmpty(accountId) ? null : getAccountById(accountId);
    } catch (Exception e) {
      RxLogger.logException("AccountService: Load account: " + accountId, e);
    }
    return null;
  }

  public final List<Account> getAccounts() {

    accountCache.clearCache();
    List<Account> accounts = accountRepository.selectAccounts();
    if (accounts != null && !accounts.isEmpty()) {
      accounts.stream().filter(Objects::nonNull)
          .forEach(account -> accountCache.put(account.getAccount_id(), account));
    }
    return accounts;
  }

  public final Account getAccountById(final String accountId) {

    Validator.requireNonEmpty(accountId, "Account ID must be required");

    Account account = accountCache
        .computeIfAbsent(accountId, acc -> accountRepository.selectAccount(accountId));
    if (account != null) {
      return account;
    }
    throw new RecordNotFoundException("No records found for account id: " + accountId);
  }

  public final Account createAccount(String accountName, String appBaseUrl, String updUserId) {

    Validator.requireNonEmpty(accountName, "Account name required to create account");
    Validator.requireNonEmpty(appBaseUrl, "App base URL required to create account");

    String accountId = IdGenerator.createAccountId();
    String authToken = IdGenerator.createAuthToken();

    Account account = new Account(accountId, authToken);
    account.setAccount_name(accountName);
    account.setApp_base_url(appBaseUrl);
    account.setLast_updated_by(updUserId);

    if (accountRepository.insertAccount(account)) {
      accountCache.remove(accountId);
      return getAccountById(account.getAccount_id());
    } else {
      throw new ConnectException("Failed to create account, Account ID: " + accountId);
    }
  }

  public final Account updateAccount(String accountId, String accountName, String appBaseUrl,
      boolean isActive, String updUserId) {

    Validator.requireNonEmpty(accountId, "Account ID required to update account");
    Validator.requireNonEmpty(accountName, "Account name required to update account");
    Validator.requireNonEmpty(appBaseUrl, "App base URL required to update account");

    Account account = getAccountById(accountId);
    account.setActive(isActive);
    account.setAccount_name(accountName);
    account.setApp_base_url(appBaseUrl);
    account.setLast_updated_by(updUserId);

    if (accountRepository.updateAccount(account)) {
      accountCache.remove(accountId);
      return getAccountById(account.getAccount_id());
    } else {
      throw new ConnectException("Failed to update account, Account ID: " + accountId);
    }
  }

  public final AuthToken getAuthToken(final String accountId) {

    Account account = getAccountById(accountId);
    byte[] authorizationTokenBytes = account.getAuth_token().getBytes();
    return new AuthToken(accountId, BASE64_ENCODER.encodeToString(authorizationTokenBytes));
  }

  public final AuthToken refreshAuthToken(final String accountId, String updUserId) {

    Validator.requireNonEmpty(accountId, "Account ID required to update auth token");

    String authToken = IdGenerator.createAuthToken();
    if (accountRepository.updateAuthToken(accountId, authToken, updUserId)) {
      accountCache.remove(accountId);
      return getAuthToken(accountId);
    } else {
      throw new ConnectException("Failed to update authorization token, Account ID: " + accountId);
    }
  }
}
