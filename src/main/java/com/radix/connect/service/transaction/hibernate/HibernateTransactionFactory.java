package com.radix.connect.service.transaction.hibernate;

import com.radix.connect.core.annotation.TxnFactory;
import com.radix.connect.core.constant.TxnType;
import com.radix.connect.core.transaction.Transaction;
import com.radix.connect.core.transaction.TransactionManager.TransactionFactory;
import com.radix.connect.error.TransactionException;
import javax.sql.DataSource;

/**
 * @author kiran.chokhande
 */
@TxnFactory(txn_type = TxnType.HIBERNATE_TXN)
public final class HibernateTransactionFactory implements TransactionFactory {

  @Override
  public final void configure(final DataSource dataSource) {
  }

  @Override
  public final Transaction getTransaction() throws Exception {
    throw new TransactionException("Not implemented");
  }
}
