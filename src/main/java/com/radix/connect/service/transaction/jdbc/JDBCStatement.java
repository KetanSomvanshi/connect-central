package com.radix.connect.service.transaction.jdbc;

import com.radix.connect.core.transaction.QueryMapper.Query;
import com.radix.connect.core.transaction.Statement;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author kiran.chokhande
 */
public final class JDBCStatement implements Statement, AutoCloseable {

  private final Query query;
  private final PreparedStatement statement;
  private final AtomicInteger batchSize = new AtomicInteger();
  private final Map<Integer, Map<Integer, Object>> indexParamValues = new HashMap<>();

  public JDBCStatement(final Query query, final PreparedStatement statement) {
    this.query = query;
    this.statement = statement;
  }

  private void putIndexParameterValue(int parameterIndex, Object value) {
    Map<Integer, Object> mapping;
    mapping = indexParamValues.computeIfAbsent(batchSize.get(), m -> new HashMap<>());
    mapping.put(parameterIndex, value);
  }

  @Override
  public final String toString() {
    return query.buildQuery(indexParamValues);
  }

  @Override
  public final void setNumber(String paramName, Number value) throws Exception {
    for (Integer index : query.getParamIndex(paramName)) {
      setNumber(index, value);
    }
  }

  @Override
  public final void setNumber(int parameterIndex, Number value) throws Exception {
    setObject(parameterIndex, value);
  }

  @Override
  public final void setString(String paramName, String value) throws Exception {
    for (Integer index : query.getParamIndex(paramName)) {
      setString(index, value);
    }
  }

  @Override
  public final void setString(int parameterIndex, String value) throws Exception {
    statement.setString(parameterIndex, value);
    putIndexParameterValue(parameterIndex, value);
  }

  @Override
  public final void setString(String paramName, Object value) throws Exception {
    for (Integer index : query.getParamIndex(paramName)) {
      setString(index, value);
    }
  }

  @Override
  public final void setString(int parameterIndex, Object value) throws Exception {
    setString(parameterIndex, value == null ? null : value.toString());
  }

  @Override
  public final void setObject(String paramName, Object value) throws Exception {
    for (Integer index : query.getParamIndex(paramName)) {
      setObject(index, value);
    }
  }

  @Override
  public final void setObject(int parameterIndex, Object value) throws Exception {
    if (value != null) {
      statement.setObject(parameterIndex, value);
    } else {
      statement.setString(parameterIndex, null);
    }
    putIndexParameterValue(parameterIndex, value);
  }

  @Override
  public final void setValue(String paramName, boolean value) throws Exception {
    for (Integer index : query.getParamIndex(paramName)) {
      setValue(index, value);
    }
  }

  @Override
  public final void setValue(int parameterIndex, boolean value) throws Exception {
    statement.setBoolean(parameterIndex, value);
    putIndexParameterValue(parameterIndex, value);
  }

  @Override
  public final void setValue(String paramName, Date value) throws Exception {
    for (Integer index : query.getParamIndex(paramName)) {
      setValue(index, value);
    }
  }

  @Override
  public final void setValue(int parameterIndex, Date value) throws Exception {

    putIndexParameterValue(parameterIndex, value);
    if (value == null) {
      statement.setString(parameterIndex, null);
    } else if (value instanceof Timestamp) {
      statement.setTimestamp(parameterIndex, (Timestamp) value);
    } else if (value instanceof Time) {
      statement.setTime(parameterIndex, (Time) value);
    } else {
      statement.setDate(parameterIndex, new java.sql.Date(value.getTime()));
    }
  }

  @Override
  public final void setArray(String paramName, Object[] value) throws Exception {
    Array array = value == null ? null : statement.getConnection().createArrayOf("text", value);
    for (Integer parameterIndex : query.getParamIndex(paramName)) {
      if (value != null) {
        statement.setArray(parameterIndex, array);
      } else {
        statement.setString(parameterIndex, null);
      }
      putIndexParameterValue(parameterIndex, array == null ? null : array.toString());
    }
  }

  @Override
  public final void setArray(int parameterIndex, Object[] value) throws Exception {
    Array array = value == null ? null : statement.getConnection().createArrayOf("text", value);
    if (value != null) {
      statement.setArray(parameterIndex, array);
    } else {
      statement.setString(parameterIndex, null);
    }
    putIndexParameterValue(parameterIndex, array == null ? null : array.toString());
  }

  @Override
  public final void addBatch() throws Exception {
    statement.addBatch();
    batchSize.incrementAndGet();
  }

  @Override
  public final void close() throws Exception {
    statement.close();
  }

  public final int executeUpdate() throws Exception {
    return statement.executeUpdate();
  }

  public final int[] executeBatch() throws Exception {
    return statement.executeBatch();
  }

  public final ResultSet executeQuery() throws Exception {
    return statement.executeQuery();
  }
}
