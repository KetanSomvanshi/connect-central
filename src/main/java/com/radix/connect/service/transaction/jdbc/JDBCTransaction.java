package com.radix.connect.service.transaction.jdbc;

import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.logger.RxLogger.QueryLog;
import com.radix.connect.core.transaction.ParameterSetter;
import com.radix.connect.core.transaction.QueryMapper;
import com.radix.connect.core.transaction.QueryMapper.Query;
import com.radix.connect.core.transaction.ResultExtractor;
import com.radix.connect.core.transaction.Transaction;
import com.radix.connect.error.TransactionException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Objects;

/**
 * @author kiran.chokhande
 */
public final class JDBCTransaction implements Transaction, AutoCloseable {

  private final Connection connection;

  public JDBCTransaction(final Connection connection) {
    this.connection = Objects.requireNonNull(connection, "Error: Database Connection required");
  }

  @Override
  public final void setAutoCommit(final boolean autoCommitMode) {

    try {
      this.connection.setAutoCommit(autoCommitMode);
    } catch (Exception exc) {
      RxLogger.logException("Failed to set auto commit: " + autoCommitMode, exc);
      throw new TransactionException("Failed to set auto commit: " + autoCommitMode, exc);
    }
  }

  private JDBCStatement statement(String query_id) throws Exception {
    Query query;
    if ((query = QueryMapper.get(query_id)) == null) {
      throw new TransactionException("Error: SQL Query not configured for given ID: " + query_id);
    }
    return statement(query);
  }

  private JDBCStatement statement(Query query) throws Exception {
    if (query == null) {
      throw new TransactionException("Error: Must provide valid SQL Query to execute");
    }
    return new JDBCStatement(query, connection.prepareStatement(query.getQuery()));
  }

  @Override
  public final <E> E select(Query query, ResultExtractor<E> extractor) {

    try (JDBCStatement statement = statement(query)) {
      try (QueryLog ignored = RxLogger.logQuery(query.getQueryId(), statement.toString())) {
        try (ResultSet result = statement.executeQuery()) {
          return result == null ? null : extractor.extract(result);
        }
      }
    } catch (TransactionException exc) {
      throw exc;
    } catch (Exception exc) {
      RxLogger.logException("Failed to execute SELECT statement", exc);
      throw new TransactionException("Failed to execute SELECT statement", exc);
    }
  }

  @Override
  public final <E> E select(String query_id, ResultExtractor<E> extractor) {
    return select(query_id, ParameterSetter.NULLABLE, extractor);
  }

  @Override
  public final <E> E select(String query_id, ResultExtractor<E> extractor, Object... params) {
    return select(query_id, params, extractor);
  }

  @Override
  public final <E> E select(String query_id, Object[] params, ResultExtractor<E> extractor) {
    return select(query_id, extractor, params == null || params.length < 1 ? null : statement -> {
      for (int i = 1; i <= params.length; i++) {
        statement.setObject(i, params[i - 1]);
      }
    });
  }

  @Override
  public final <E> E select(String query_id, ResultExtractor<E> extractor, ParameterSetter setter) {
    return select(query_id, setter, extractor);
  }

  @Override
  public final <E> E select(String query_id, ParameterSetter setter, ResultExtractor<E> extractor) {

    try (JDBCStatement statement = statement(query_id)) {
      if (setter != null) {
        setter.setValues(statement);
      }
      try (QueryLog ignored = RxLogger.logQuery(query_id, statement.toString())) {
        try (ResultSet result = statement.executeQuery()) {
          return result == null ? null : extractor.extract(result);
        }
      }
    } catch (TransactionException exc) {
      throw exc;
    } catch (Exception exc) {
      RxLogger.logException("Failed to execute SELECT statement", exc);
      throw new TransactionException("Failed to execute SELECT statement", exc);
    }
  }

  @Override
  public final int update(Query query) {

    try (JDBCStatement statement = statement(query)) {
      try (QueryLog ignored = RxLogger.logQuery(query.getQueryId(), statement.toString())) {
        return statement.executeUpdate();
      }
    } catch (TransactionException exc) {
      throw exc;
    } catch (Exception exc) {
      RxLogger.logException("Failed to execute UPDATE statement", exc);
      throw new TransactionException("Failed to execute UPDATE statement", exc);
    }
  }

  @Override
  public final int update(String query_id) {
    return update(query_id, ParameterSetter.NULLABLE);
  }

  @Override
  public final int update(String query_id, Object... params) {
    return update(query_id, params == null || params.length <= 0 ? null : statement -> {
      for (int i = 1; i <= params.length; i++) {
        statement.setObject(i, params[i - 1]);
      }
    });
  }

  @Override
  public final int update(String query_id, ParameterSetter setter) {

    try (JDBCStatement statement = statement(query_id)) {
      if (setter != null) {
        setter.setValues(statement);
      }
      try (QueryLog ignored = RxLogger.logQuery(query_id, statement.toString())) {
        return statement.executeUpdate();
      }
    } catch (TransactionException exc) {
      throw exc;
    } catch (Exception exc) {
      RxLogger.logException("Failed to execute UPDATE statement", exc);
      throw new TransactionException("Failed to execute UPDATE statement", exc);
    }
  }

  @Override
  public final int[] batchUpdate(String query_id, ParameterSetter setter) {
    try (JDBCStatement statement = statement(query_id)) {
      if (setter != null) {
        setter.setValues(statement);
      }
      try (QueryLog ignored = RxLogger.logQuery(query_id, statement.toString())) {
        return statement.executeBatch();
      }
    } catch (TransactionException exc) {
      throw exc;
    } catch (Exception exc) {
      RxLogger.logException("Failed to execute BATCH statement", exc);
      throw new TransactionException("Failed to execute BATCH statement", exc);
    }
  }

  @Override
  public final void commit() {
    try {
      this.connection.commit();
    } catch (Exception exc) {
      RxLogger.logException("Commits Transaction", exc);
    }
  }

  @Override
  public final void rollback() {
    try {
      this.connection.rollback();
    } catch (Exception exc) {
      RxLogger.logException("Rollbacks Transaction", exc);
    }
  }

  @Override
  public final void close() {
    try {
      this.connection.setAutoCommit(true);
    } catch (Exception exc) {
      RxLogger.logException("Set AutoCommit back to TRUE", exc);
    } finally {
      try {
        this.connection.close();
      } catch (Exception exc) {
        RxLogger.logException("Close Database Connection", exc);
      }
    }
  }
}
