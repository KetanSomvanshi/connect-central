package com.radix.connect.service.transaction.jdbc;

import com.radix.connect.core.annotation.TxnFactory;
import com.radix.connect.core.constant.TxnType;
import com.radix.connect.core.transaction.Transaction;
import com.radix.connect.core.transaction.TransactionManager.TransactionFactory;
import com.radix.connect.core.util.Validator;
import javax.sql.DataSource;

/**
 * @author kiran.chokhande
 */
@TxnFactory(txn_type = TxnType.JDBC_TXN)
public final class JDBCTransactionFactory implements TransactionFactory {

  private volatile DataSource dataSource;

  @Override
  public final void configure(final DataSource dataSource) {
    this.dataSource = Validator.requireNonNull(dataSource, "DataSource must required");
  }

  @Override
  public final Transaction getTransaction() throws Exception {
    return new JDBCTransaction(dataSource.getConnection());
  }
}
