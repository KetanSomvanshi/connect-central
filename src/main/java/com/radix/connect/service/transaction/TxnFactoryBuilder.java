package com.radix.connect.service.transaction;


import com.radix.connect.core.Builder;
import com.radix.connect.core.annotation.TxnFactory;
import com.radix.connect.core.constant.TxnType;
import com.radix.connect.core.transaction.TransactionManager.TransactionFactory;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class TxnFactoryBuilder implements Builder<TxnType, TransactionFactory> {

  private final DataSource dataSource;
  private final ApplicationContext context;
  private final Map<TxnType, Class<TransactionFactory>> classMap = new HashMap<>();

  private static final String APPLICATION_CONTEXT_REQ_ERROR = "ApplicationContext must be required";
  private static final String DATA_SOURCE_REQUIRED_ER = "DataSource must be required";
  private static final String MULTIPLE_TXN_FACTORY_IMPL = "Transaction factory implemented multiple times for txn type: %s";
  private static final String NO_TXN_FACTORY_IMPLEMENT_ER = "Transaction factory not implemented or not annotated by @TxnFactory with txn type: %s";

  @Autowired
  public TxnFactoryBuilder(final DataSource dataSource, ApplicationContext context) {

    this.context = Validator.requireNonNull(context, APPLICATION_CONTEXT_REQ_ERROR);
    this.dataSource = Validator.requireNonNull(dataSource, DATA_SOURCE_REQUIRED_ER);

    Arrays.stream(context.getBeanNamesForAnnotation(TxnFactory.class))
        .forEach(name -> registerTxnFactoryImplClsIfSupport(context.getType(name)));
  }

  @Override
  public final TransactionFactory build(final TxnType txnType) {

    Class<TransactionFactory> txnFactoryImplClass = classMap.get(txnType);
    if (txnFactoryImplClass != null) {
      TransactionFactory txnFactory = context.getBean(txnFactoryImplClass);
      txnFactory.configure(dataSource);
      return txnFactory;
    }
    throw new ConnectException(String.format(NO_TXN_FACTORY_IMPLEMENT_ER, txnType));
  }

  @SuppressWarnings("unchecked")
  private void registerTxnFactoryImplClsIfSupport(final Class<?> txnFactoryImplClass) {

    if (TransactionFactory.class.isAssignableFrom(txnFactoryImplClass)) {

      TxnFactory txnFactory = txnFactoryImplClass.getAnnotation(TxnFactory.class);
      TxnType txn_type = txnFactory.txn_type();

      if (!classMap.containsKey(txn_type)) {
        classMap.put(txn_type, (Class<TransactionFactory>) txnFactoryImplClass);
      } else {
        throw new ConnectException(String.format(MULTIPLE_TXN_FACTORY_IMPL, txn_type));
      }
    }
  }
}
