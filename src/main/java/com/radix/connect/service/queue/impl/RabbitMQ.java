package com.radix.connect.service.queue.impl;

import com.radix.connect.core.Handler;
import com.radix.connect.core.annotation.MsgQueue;
import com.radix.connect.core.constant.QueueType;
import com.radix.connect.core.message.queue.Queue;
import com.radix.connect.core.model.QueueConfig;
import com.radix.connect.core.model.QueueMsg;
import java.util.List;

/**
 * @author kiran.chokhande
 */
@MsgQueue(queue_type = QueueType.RABBIT_MQ)
public final class RabbitMQ implements Queue {

  @Override
  public void configure(final QueueConfig queueConfig) {
  }

  @Override
  public final List<QueueMsg> select() {
    return null;
  }

  @Override
  public final void insert(final QueueMsg queueMsg, final Handler<QueueMsg> handler) {
  }

  @Override
  public final void delete(final QueueMsg queueMsg, final Handler<QueueMsg> handler) {
  }
}
