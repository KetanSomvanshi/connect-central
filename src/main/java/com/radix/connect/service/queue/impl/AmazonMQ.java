package com.radix.connect.service.queue.impl;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.handlers.AsyncHandler;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.radix.connect.core.Handler;
import com.radix.connect.core.annotation.MsgQueue;
import com.radix.connect.core.constant.QueueType;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.message.queue.Queue;
import com.radix.connect.core.model.QueueConfig;
import com.radix.connect.core.model.QueueMsg;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author kiran.chokhande
 */
@MsgQueue(queue_type = QueueType.AMAZON_MQ)
public final class AmazonMQ implements Queue {

  private String queueUrl;
  private AmazonSQSAsync amazonSQSAsync;
  private int maxVisibilityTimeoutInSeconds;

  @Override
  public final void configure(final QueueConfig queueConfig) {

    JsonUtil config = new JsonUtil(queueConfig.getConfig_json());

    String queueUrl = config.getValue("queue_url");
    String accessKey = config.getValue("access_key");
    String secretKey = config.getValue("secret_key");
    queueUrl = Validator.requireNonEmpty(queueUrl, "Amazon queue url must required");
    accessKey = Validator.requireNonEmpty(accessKey, "Amazon access key must required");
    secretKey = Validator.requireNonEmpty(secretKey, "Amazon secret key must required");

    int visibilityMinute = queueConfig.getVisibility_timeout_minutes();
    BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKey, secretKey);

    this.queueUrl = queueUrl;
    this.maxVisibilityTimeoutInSeconds = (visibilityMinute <= 0 ? 30 : visibilityMinute) * 60;
    this.amazonSQSAsync = AmazonSQSAsyncClientBuilder.standard().withRegion(Regions.US_EAST_1)
        .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials)).build();
  }

  @Override
  public final List<QueueMsg> select() {

    ReceiveMessageRequest mRequest;
    mRequest = new ReceiveMessageRequest(queueUrl).withWaitTimeSeconds(20);
    mRequest.withMaxNumberOfMessages(10).withVisibilityTimeout(maxVisibilityTimeoutInSeconds);

    final List<QueueMsg> queueMsgList = new ArrayList<>();
    try {

      ReceiveMessageResult result = amazonSQSAsync.receiveMessageAsync(mRequest).get();
      result.getMessages().stream().filter(Objects::nonNull).forEach(message -> {
        QueueMsg queueMsg = parse(message.getBody(), message.getReceiptHandle());
        if (queueMsg != null) {
          queueMsgList.add(queueMsg);
        }
      });
    } catch (Exception exception) {
      RxLogger.logException("Failed To Consume Messages From Queue", exception);
    }
    return queueMsgList;
  }

  private QueueMsg parse(final String messageBody, final String msgReceiptHandle) {

    QueueMsg queueMsg = JsonUtil.toType(messageBody, QueueMsg.class);
    if (queueMsg != null) {
      queueMsg.setReceipt_handle(msgReceiptHandle);
    } else {
      amazonSQSAsync.deleteMessageAsync(queueUrl, msgReceiptHandle);
      RxLogger.logMessage("Unrecognised queue message body", messageBody);
    }
    return queueMsg;
  }

  @Override
  public final void insert(final QueueMsg queueMsg, final Handler<QueueMsg> handler) {

    Validator.requireNonNull(queueMsg, "Missing input detail to queue message");

    SendMessageRequest msReq;
    msReq = new SendMessageRequest(queueUrl, JsonUtil.toString(queueMsg));
    msReq.withMessageGroupId(queueMsg.getMessage_id()).withDelaySeconds(0);

    amazonSQSAsync.sendMessageAsync(msReq, new AsyncHandler<>() {
      @Override
      public void onError(Exception ex) {
        handler.onFailure(queueMsg, ex);
      }
      @Override
      public void onSuccess(SendMessageRequest request, SendMessageResult result) {
        handler.onSuccess(queueMsg);
      }
    });
  }

  @Override
  public final void delete(final QueueMsg queueMsg, final Handler<QueueMsg> handler) {

    String receiptHandle = queueMsg.getReceipt_handle();
    Validator.requireNonEmpty(receiptHandle, "Receipt handle must be required");

    amazonSQSAsync.deleteMessageAsync(queueUrl, receiptHandle, new AsyncHandler<>() {
      @Override
      public void onError(Exception ex) {
        handler.onFailure(queueMsg, ex);
      }
      @Override
      public void onSuccess(DeleteMessageRequest request, DeleteMessageResult result) {
        handler.onSuccess(queueMsg);
      }
    });
  }
}
