package com.radix.connect.service.queue;

import com.radix.connect.core.Builder;
import com.radix.connect.core.annotation.MsgQueue;
import com.radix.connect.core.constant.QueueType;
import com.radix.connect.core.message.queue.Queue;
import com.radix.connect.core.model.QueueConfig;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class QueueBuilder implements Builder<QueueConfig, Queue> {

  private final ApplicationContext context;
  private final Map<QueueType, Class<Queue>> classMap = new HashMap<>();

  private static final String APP_CONTEXT_REQ_ERROR = "ApplicationContext must be required";
  private static final String MULTIPLE_QUEUE_IMPL_ERR = "Queue implemented multiple times for queue type: %s";
  private static final String NO_QUEUE_IMPLEMENTED_ERR = "Queue not implemented or not annotated by @MsgQueue with queue type: %s";

  public QueueBuilder(final ApplicationContext applicationContext) {

    this.context = Validator.requireNonNull(applicationContext, APP_CONTEXT_REQ_ERROR);

    Arrays.stream(context.getBeanNamesForAnnotation(MsgQueue.class))
        .forEach(beanName -> registerQueueImplClsIfSupport(context.getType(beanName)));
  }

  @SuppressWarnings("unchecked")
  private void registerQueueImplClsIfSupport(final Class<?> queueImplementationClass) {

    if (Queue.class.isAssignableFrom(queueImplementationClass)) {

      MsgQueue msgQueue = queueImplementationClass.getAnnotation(MsgQueue.class);
      QueueType queue_type = msgQueue.queue_type();

      if (!classMap.containsKey(queue_type)) {
        classMap.put(queue_type, (Class<Queue>) queueImplementationClass);
      } else {
        throw new ConnectException(String.format(MULTIPLE_QUEUE_IMPL_ERR, queue_type));
      }
    }
  }

  @Override
  public final Queue build(final QueueConfig queueConfig) {

    Validator.requireNonNull(queueConfig, "Queue configuration must required");

    Class<Queue> queueImplClass = classMap.get(queueConfig.getQueue_type());
    if (queueImplClass == null) {
      QueueType queueType = queueConfig.getQueue_type();
      throw new ConnectException(String.format(NO_QUEUE_IMPLEMENTED_ERR, queueType));
    }

    Queue queue = context.getAutowireCapableBeanFactory().createBean(queueImplClass);
    queue.configure(queueConfig);
    return queue;
  }
}
