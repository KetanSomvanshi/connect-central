package com.radix.connect.core;

/**
 * @author kiran.chokhande
 */
@FunctionalInterface
public interface Configurable<Config> {

  void configure(Config config);
}
