package com.radix.connect.core;

/**
 * @author kiran.chokhande
 */
@FunctionalInterface
public interface Reloadable<Entity> {

  void reload(Entity entity);
}
