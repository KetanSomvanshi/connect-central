package com.radix.connect.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author kiran.chokhande
 */
public final class SenderLog implements Serializable {

  @JsonProperty
  private final Modality modality;
  @JsonProperty
  private final String to_phone;
  @JsonProperty
  private final String from_phone;
  @JsonProperty
  private final String account_id;
  @JsonProperty
  private final String message_id;

  @JsonProperty
  private final Timestamp sent_timestamp;

  public SenderLog(Modality modality, String toPhone, String fromPhone, String accountId, String messageId) {
    this(modality, toPhone, fromPhone, accountId, messageId, null);
  }

  public SenderLog(Modality mode, String toPhone, String fromPhone, String accountId, String messageId, Timestamp sentTs) {
    this.modality = Validator.requireNonNull(mode, "Modality must be required");
    this.to_phone = Validator.requireNonEmpty(toPhone, "To phone number required");
    this.from_phone = Validator.requireNonEmpty(fromPhone, "From phone number required");
    this.account_id = Validator.requireNonEmpty(accountId, "Account Id required");
    this.message_id = Validator.requireNonEmpty(messageId, "Message Id required");
    this.sent_timestamp = Validator.coalesce(sentTs, new Timestamp(System.currentTimeMillis()));
  }

  public Modality getModality() {
    return modality;
  }

  public String getTo_phone() {
    return to_phone;
  }

  public String getFrom_phone() {
    return from_phone;
  }

  public String getAccount_id() {
    return account_id;
  }

  public String getMessage_id() {
    return message_id;
  }

  public Timestamp getSent_timestamp() {
    return sent_timestamp;
  }
}
