package com.radix.connect.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.radix.connect.core.constant.Direction;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author kiran.chokhande
 */
public final class Message implements Serializable {

  @JsonProperty
  private final Modality modality;
  @JsonProperty
  private final String account_id;
  @JsonProperty
  private final String message_id;
  @JsonProperty
  private final Direction direction;

  @JsonProperty
  private Status msg_status;
  @JsonProperty
  private String error_desc;

  @JsonProperty
  private Timestamp logged_ts;
  @JsonProperty
  private Timestamp before_ts;
  @JsonProperty
  private Timestamp processed_ts;

  @JsonProperty
  private String prev_message_id;

  private String contact, message, msg_sid;

  public Message(String account_id, Modality modality, String message_id, Direction direction) {
    this(account_id, modality, message_id, direction, null);
  }

  public Message(String account_id, Modality modality, String message_id, Direction direction, String message) {

    this.account_id = Validator.requireNonEmpty(account_id, "Account ID must be required");
    this.message_id = Validator.requireNonEmpty(message_id, "Message ID must be required");
    this.modality = Validator.requireNonNull(modality, "Communication mode must be required");
    this.direction = Validator.requireNonNull(direction, "Message direction must be required");

    this.message = message;
    this.contact = message == null ? null : new JsonUtil(message).getValue("contact");
  }

  public Modality getModality() {
    return modality;
  }

  public String getAccount_id() {
    return account_id;
  }

  public String getMessage_id() {
    return message_id;
  }

  public Direction getDirection() {
    return direction;
  }

  public Status getMsg_status() {
    return msg_status;
  }

  public void setMsg_status(Status msg_status) {
    this.msg_status = msg_status;
  }

  public String getError_desc() {
    return error_desc;
  }

  public void setError_desc(String error_desc) {
    this.error_desc = error_desc;
  }

  public Timestamp getLogged_ts() {
    return logged_ts;
  }

  public void setLogged_ts(Timestamp logged_ts) {
    this.logged_ts = logged_ts;
  }

  public Timestamp getBefore_ts() {
    return before_ts;
  }

  public void setBefore_ts(Timestamp before_ts) {
    this.before_ts = before_ts;
  }

  public Timestamp getProcessed_ts() {
    return processed_ts;
  }

  public void setProcessed_ts(Timestamp processed_ts) {
    this.processed_ts = processed_ts;
  }

  public String getPrev_message_id() {
    return prev_message_id;
  }

  public void setPrev_message_id(String prev_message_id) {
    this.prev_message_id = prev_message_id;
  }

  public String getContact() {
    return contact;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
    this.contact = message == null ? null : new JsonUtil(message).getValue("contact");
  }

  public String getMsg_sid() {
    return msg_sid;
  }

  public void setMsg_sid(String msg_sid) {
    this.msg_sid = msg_sid;
  }
}

