package com.radix.connect.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.SenderAPI;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author kiran.chokhande
 */
public final class SenderConfig implements Serializable {

  @JsonProperty
  private final String sender_id;
  @JsonProperty
  private final Modality modality;
  @JsonProperty
  private final SenderAPI sender_api;

  @JsonProperty
  private String sender_desc;

  @JsonProperty
  private int messaging_rate;
  @JsonProperty
  private String config_json;

  @JsonProperty
  private String last_updated_by;
  @JsonProperty
  private Timestamp last_updated_ts;

  public SenderConfig(String sender_id, Modality modality, SenderAPI sender_api) {
    this.sender_id = Validator.requireNonEmpty(sender_id, "Sender Id must be required");
    this.modality = Validator.requireNonNull(modality, "Modality must be required");
    this.sender_api = Validator.requireNonNull(sender_api, "Message sender api must be required");
  }

  public String getSender_id() {
    return sender_id;
  }

  public Modality getModality() {
    return modality;
  }

  public SenderAPI getSender_api() {
    return sender_api;
  }

  public String getSender_desc() {
    return sender_desc;
  }

  public void setSender_desc(String sender_desc) {
    this.sender_desc = sender_desc;
  }

  public int getMessaging_rate() {
    return messaging_rate;
  }

  public void setMessaging_rate(int messaging_rate) {
    this.messaging_rate = messaging_rate;
  }

  public String getConfig_json() {
    return config_json;
  }

  public void setConfig_json(String config_json) {
    this.config_json = config_json;
  }

  public String getLast_updated_by() {
    return last_updated_by;
  }

  public void setLast_updated_by(String last_updated_by) {
    this.last_updated_by = last_updated_by;
  }

  public Timestamp getLast_updated_ts() {
    return last_updated_ts;
  }

  public void setLast_updated_ts(Timestamp last_updated_ts) {
    this.last_updated_ts = last_updated_ts;
  }
}