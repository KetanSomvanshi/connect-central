package com.radix.connect.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.radix.connect.core.constant.Role;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * @author kiran.chokhande
 */
public final class AppUser implements Serializable {

  @JsonProperty
  private final String user_id;
  @JsonProperty(access = Access.WRITE_ONLY)
  private final String password;

  @JsonProperty
  private String name;
  @JsonProperty
  private String picture;

  @JsonProperty
  private Role role;
  @JsonProperty
  private boolean active;
  @JsonProperty
  private boolean locked;
  @JsonProperty(access = Access.WRITE_ONLY)
  private int num_failed_login_attempt;

  @JsonProperty
  private Timestamp last_logged_in;

  @JsonProperty
  private String last_updated_by;
  @JsonProperty
  private Timestamp last_updated_ts;

  public AppUser(final String user_id, final String password) {
    this.user_id = Validator.requireNonEmpty(user_id, "User ID can not NULL or Empty");
    this.password = Validator.requireNonEmpty(password, "Password can not NULL or Empty");
  }

  public String getUser_id() {
    return user_id;
  }

  public String getPassword() {
    return password;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public boolean isLocked() {
    return locked;
  }

  public void setLocked(boolean locked) {
    this.locked = locked;
  }

  public int getNum_failed_login_attempt() {
    return num_failed_login_attempt;
  }

  public void setNum_failed_login_attempt(int num_failed_login_attempt) {
    this.num_failed_login_attempt = num_failed_login_attempt;
  }

  public Timestamp getLast_logged_in() {
    return last_logged_in;
  }

  public void setLast_logged_in(Timestamp last_logged_in) {
    this.last_logged_in = last_logged_in;
  }

  public String getLast_updated_by() {
    return last_updated_by;
  }

  public void setLast_updated_by(String last_updated_by) {
    this.last_updated_by = last_updated_by;
  }

  public Timestamp getLast_updated_ts() {
    return last_updated_ts;
  }

  public void setLast_updated_ts(Timestamp last_updated_ts) {
    this.last_updated_ts = last_updated_ts;
  }

  public final boolean isDifferent(final AppUser other) {

    if (other == null || Validator.isEmpty(other.getUser_id())) return false;

    for (Field field : AppUser.class.getDeclaredFields()) {
      switch (field.getName()) {
        case "last_updated_by":
        case "last_updated_ts":
          continue;

        default:
          try {
            Object value1 = field.get(this);
            Object value2 = field.get(other);
            if (!Objects.equals(value1, value2)) {
              return true;
            }
          } catch (Exception exc) {
            throw new RuntimeException("Failed To Compare User Details", exc);
          }
      }
    }
    return false;
  }
}
