package com.radix.connect.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * @author kiran.chokhande
 */
public final class Account implements Serializable {

  @JsonProperty
  private final String account_id;
  @JsonProperty(access = Access.WRITE_ONLY)
  private final String auth_token;

  @JsonProperty
  private String account_name;
  @JsonProperty
  private String app_base_url;

  @JsonProperty
  private boolean active;

  @JsonProperty
  private String last_updated_by;
  @JsonProperty
  private Timestamp last_updated_ts;

  public Account(String account_id, String auth_token) {
    this(account_id, auth_token, false);
  }

  public Account(String account_id, String auth_token, boolean active) {
    this.active = active;
    this.account_id = Validator.requireNonEmpty(account_id, "Account ID must be required");
    this.auth_token = Validator.requireNonEmpty(auth_token, "Auth token must be required");
  }

  public String getAccount_id() {
    return account_id;
  }

  public String getAuth_token() {
    return auth_token;
  }

  public String getAccount_name() {
    return account_name;
  }

  public void setAccount_name(String account_name) {
    this.account_name = account_name;
  }

  public String getApp_base_url() {
    return app_base_url;
  }

  public void setApp_base_url(String app_base_url) {
    this.app_base_url = app_base_url;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public String getLast_updated_by() {
    return last_updated_by;
  }

  public void setLast_updated_by(String last_updated_by) {
    this.last_updated_by = last_updated_by;
  }

  public Timestamp getLast_updated_ts() {
    return last_updated_ts;
  }

  public void setLast_updated_ts(Timestamp last_updated_ts) {
    this.last_updated_ts = last_updated_ts;
  }

  public final boolean isDifferent(final Account other) {

    if (other == null || Validator.isEmpty(other.getAccount_id())) return false;

    for (Field field : Account.class.getDeclaredFields()) {
      switch (field.getName()) {
        case "last_updated_by":
        case "last_updated_ts":
          continue;

        default:
          try {
            Object value1 = field.get(this);
            Object value2 = field.get(other);
            if (!Objects.equals(value1, value2)) {
              return true;
            }
          } catch (Exception exc) {
            throw new RuntimeException("Failed To Compare Account Details", exc);
          }
      }
    }
    return false;
  }
}
