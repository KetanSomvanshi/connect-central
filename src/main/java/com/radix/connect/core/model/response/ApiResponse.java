package com.radix.connect.core.model.response;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;
import java.sql.Timestamp;
import org.springframework.http.HttpStatus;

/**
 * @author kiran.chokhande
 */
public final class ApiResponse implements Serializable {

  @JsonProperty
  private final Timestamp timestamp = new Timestamp(System.currentTimeMillis());

  @JsonProperty
  private final boolean success;

  @JsonProperty
  private final int status;
  @JsonProperty
  private final HttpStatus code;
  @JsonProperty
  private final String description;

  public ApiResponse(HttpStatus status, String description, boolean success) {
    this.success = success;
    this.code = Validator.requireNonNull(status, "Http status must be required");
    this.description = Validator.requireNonEmpty(description, "Description must be required");
    this.status = this.code.value();
  }

  public static ApiResponse success(HttpStatus status, String description) {
    return new ApiResponse(status, description, true);
  }

  public static ApiResponse failure(HttpStatus status, String description) {
    return new ApiResponse(status, description, false);
  }

  @Override
  public final String toString() {
    return JsonUtil.toString(this);
  }
}