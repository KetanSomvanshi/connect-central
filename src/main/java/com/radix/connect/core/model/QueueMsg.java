package com.radix.connect.core.model;

import com.google.gson.JsonObject;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;

/**
 * @author kiran.chokhande
 */
public final class QueueMsg implements Serializable {

  private final String account_id;
  private final String message_id;
  private final String timezone;
  private final JsonObject message;

  private String receipt_handle;

  public QueueMsg(String account_id, String message_id, String timezone, JsonObject message) {
    this.timezone = Validator.isEmpty(timezone) ? "EST" : timezone;
    this.message = Validator.requireNonEmpty(message, "Message contents must be required");
    this.account_id = Validator.requireNonEmpty(account_id, "Account id must be required");
    this.message_id = Validator.requireNonEmpty(message_id, "Message id must be required");
  }

  public String getAccount_id() {
    return account_id;
  }

  public String getMessage_id() {
    return message_id;
  }

  public String getTimezone() {
    return timezone;
  }

  public JsonObject getMessage() {
    return message;
  }

  public String getReceipt_handle() {
    return receipt_handle;
  }

  public void setReceipt_handle(String receipt_handle) {
    this.receipt_handle = receipt_handle;
  }
}