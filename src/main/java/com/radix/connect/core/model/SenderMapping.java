package com.radix.connect.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author kiran.chokhande
 */
public final class SenderMapping implements Serializable {

  @JsonProperty
  private final String account_id;
  @JsonProperty
  private final Modality modality;
  @JsonProperty
  private final String sender_id;

  @JsonProperty
  private String last_updated_by;
  @JsonProperty
  private Timestamp last_updated_ts;

  public SenderMapping(String account_id, Modality modality, String sender_id) {
    this.modality = Validator.requireNonNull(modality, "Modality must be required");
    this.sender_id = Validator.requireNonEmpty(sender_id, "Sender Id must be required");
    this.account_id = Validator.requireNonEmpty(account_id, "Account Id must be required");
  }

  public String getAccount_id() {
    return account_id;
  }

  public Modality getModality() {
    return modality;
  }

  public String getSender_id() {
    return sender_id;
  }

  public String getLast_updated_by() {
    return last_updated_by;
  }

  public void setLast_updated_by(String last_updated_by) {
    this.last_updated_by = last_updated_by;
  }

  public Timestamp getLast_updated_ts() {
    return last_updated_ts;
  }

  public void setLast_updated_ts(Timestamp last_updated_ts) {
    this.last_updated_ts = last_updated_ts;
  }
}