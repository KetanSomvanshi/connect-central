package com.radix.connect.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author kiran.chokhande
 */
public final class SystemConfig implements Serializable {

  @JsonProperty
  private final String key;
  @JsonProperty
  private final String desc;
  @JsonProperty
  private final String group;
  @JsonProperty
  private final String value_type;

  @JsonProperty
  private String value;

  @JsonProperty
  private final boolean encrypted;

  @JsonProperty
  private String last_updated_by;
  @JsonProperty
  private Timestamp last_updated_ts;

  public SystemConfig(String key, String desc, String group, String valueType, boolean encrypted) {
    this.encrypted = encrypted;
    this.value_type = Validator.coalesce(valueType, "TEXT");
    this.key = Validator.requireNonEmpty(key, "Key can not NULL or Empty");
    this.group = Validator.requireNonEmpty(group, "Config group can not NULL or Empty");
    this.desc = Validator.requireNonEmpty(desc, "Description can not NULL or Empty");
  }

  public String getKey() {
    return key;
  }

  public String getDesc() {
    return desc;
  }

  public String getGroup() {
    return group;
  }

  public String getValue_type() {
    return value_type;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public boolean isEncrypted() {
    return encrypted;
  }

  public String getLast_updated_by() {
    return last_updated_by;
  }

  public void setLast_updated_by(String last_updated_by) {
    this.last_updated_by = last_updated_by;
  }

  public Timestamp getLast_updated_ts() {
    return last_updated_ts;
  }

  public void setLast_updated_ts(Timestamp last_updated_ts) {
    this.last_updated_ts = last_updated_ts;
  }
}
