package com.radix.connect.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;

/**
 * @author kiran.chokhande
 */
public final class AuthToken implements Serializable {

  @JsonProperty
  private final String account_id;
  @JsonProperty
  private final String auth_token;

  public AuthToken(String account_id, String auth_token) {
    this.account_id = Validator.requireNonEmpty(account_id, "Account ID must be required");
    this.auth_token = Validator.requireNonEmpty(auth_token, "Auth token must be required");
  }

  public String getAccount_id() {
    return account_id;
  }

  public String getAuth_token() {
    return auth_token;
  }
}
