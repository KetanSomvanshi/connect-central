package com.radix.connect.core.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author kiran.chokhande
 */
public final class MsgStatus implements Serializable {

  @JsonProperty
  private final String message_id;
  @JsonProperty
  private final Status msg_status;

  @JsonProperty
  private final Timestamp processed_ts;

  @JsonProperty
  private final String error_desc;

  public MsgStatus(String messageId, Status msgStatus) {
    this(messageId, msgStatus, null, null);
  }

  public MsgStatus(String messageId, Status msgStatus, String errorDesc) {
    this(messageId, msgStatus, errorDesc, null);
  }

  public MsgStatus(String messageId, Status msgStatus, Timestamp processedTs) {
    this(messageId, msgStatus, null, processedTs);
  }

  public MsgStatus(String messageId, Status msgStatus, String errorDesc, Timestamp processedTs) {
    this.error_desc = errorDesc;
    this.processed_ts = processedTs;
    this.message_id = Validator.requireNonEmpty(messageId, "Message ID required");
    this.msg_status = Validator.requireNonNull(msgStatus, "Message status required");
  }
}

