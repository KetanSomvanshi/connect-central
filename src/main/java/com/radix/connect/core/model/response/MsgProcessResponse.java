package com.radix.connect.core.model.response;

import com.radix.connect.core.constant.Status;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;

/**
 * @author kiran.chokhande
 */
public class MsgProcessResponse implements Serializable {

  private final boolean success;
  private final Status msg_status;
  private final String error_desc;

  private String msg_sid;

  private MsgProcessResponse(boolean success, Status status, String error) {

    this.msg_status = Validator.requireNonNull(status, "Message status can not be NULL");

    this.error_desc = error;
    this.success = success && status.isSuccess();
  }

  public static MsgProcessResponse success(Status status) {
    return new MsgProcessResponse(true, status, null);
  }

  public static MsgProcessResponse success(Status status, String msg_sid) {
    return new MsgProcessResponse(true, status, null).withMsgSid(msg_sid);
  }

  public static MsgProcessResponse failure(Status status, String error) {
    return new MsgProcessResponse(false, status, error);
  }

  public static MsgProcessResponse failure(Status status, Throwable error) {
    return new MsgProcessResponse(false, status, extractStackTrace(error));
  }

  public static MsgProcessResponse failure(Status status, String error, String msg_sid) {
    return new MsgProcessResponse(false, status, error).withMsgSid(msg_sid);
  }

  public static MsgProcessResponse failure(Status status, Throwable error, String msg_sid) {
    return new MsgProcessResponse(false, status, extractStackTrace(error)).withMsgSid(msg_sid);
  }

  public boolean isSuccess() {
    return success;
  }

  public String getMsg_sid() {
    return msg_sid;
  }

  public String getError_desc() {
    return error_desc;
  }

  public Status getMsg_status() {
    return msg_status;
  }

  public MsgProcessResponse withMsgSid(String msg_sid) {
    this.msg_sid = msg_sid;
    return this;
  }

  private static String extractStackTrace(final Throwable throwable) {

    StringBuilder error_builder = new StringBuilder();
    if (throwable != null) {
      error_builder.append(throwable.getClass().getName());
      error_builder.append(":").append(throwable.getMessage());
      for (StackTraceElement trace : throwable.getStackTrace()) {
        error_builder.append("\n at ").append(trace);
      }
    }
    return throwable == null ? null : error_builder.toString().trim();
  }
}
