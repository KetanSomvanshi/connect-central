package com.radix.connect.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * @author kiran.chokhande
 */
public final class SenderSetting implements Serializable {

  @JsonProperty
  private final String account_id;
  @JsonProperty
  private final Modality modality;
  @JsonProperty
  private final String config_json;

  @JsonProperty
  private String last_updated_by;
  @JsonProperty
  private Timestamp last_updated_ts;

  public SenderSetting(String account_id, Modality modality, String config_json) {
    this.account_id = Validator.isEmpty(account_id) ? null : account_id;
    this.config_json = Validator.isEmpty(config_json) ? null : config_json;
    this.modality = Validator.requireNonNull(modality, "Modality must be required");
  }

  public String getAccount_id() {
    return account_id;
  }

  public Modality getModality() {
    return modality;
  }

  public String getConfig_json() {
    return config_json;
  }

  public String getLast_updated_by() {
    return last_updated_by;
  }

  public void setLast_updated_by(String last_updated_by) {
    this.last_updated_by = last_updated_by;
  }

  public Timestamp getLast_updated_ts() {
    return last_updated_ts;
  }

  public void setLast_updated_ts(Timestamp last_updated_ts) {
    this.last_updated_ts = last_updated_ts;
  }

  public final boolean isDifferent(final SenderSetting other) {

    if (other == null) return false;

    for (Field field : SenderSetting.class.getDeclaredFields()) {
      switch (field.getName()) {
        case "last_updated_by":
        case "last_updated_ts":
          continue;

        default:
          try {
            Object value1 = field.get(this);
            Object value2 = field.get(other);
            if (!Objects.equals(value1, value2)) {
              return true;
            }
          } catch (Exception exc) {
            throw new RuntimeException("Failed To Compare Sender Setting", exc);
          }
      }
    }
    return false;
  }
}
