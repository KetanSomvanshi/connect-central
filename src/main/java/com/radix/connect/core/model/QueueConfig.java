package com.radix.connect.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.radix.connect.core.constant.QueueType;
import com.radix.connect.core.util.Validator;
import java.io.Serializable;

/**
 * @author kiran.chokhande
 */
public final class QueueConfig implements Serializable {

  @JsonProperty
  private final QueueType queue_type;
  @JsonProperty
  private String config_json;
  @JsonProperty
  private int visibility_timeout_minutes;

  public QueueConfig(QueueType queue_type) {
    this.queue_type = Validator.requireNonNull(queue_type, "Queue type required");
  }

  public QueueType getQueue_type() {
    return queue_type;
  }

  public String getConfig_json() {
    return config_json;
  }

  public void setConfig_json(String config_json) {
    this.config_json = config_json;
  }

  public int getVisibility_timeout_minutes() {
    return visibility_timeout_minutes;
  }

  public void setVisibility_timeout_minutes(int visibility_timeout_minutes) {
    this.visibility_timeout_minutes = visibility_timeout_minutes;
  }
}