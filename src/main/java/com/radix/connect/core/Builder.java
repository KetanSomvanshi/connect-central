package com.radix.connect.core;

/**
 * @author kiran.chokhande
 */
@FunctionalInterface
public interface Builder<Input, Output> {

  Output build(Input value);
}
