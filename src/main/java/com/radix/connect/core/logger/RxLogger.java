package com.radix.connect.core.logger;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.radix.connect.core.constant.Constant;
import com.radix.connect.core.constant.Constant.WorkSpaceName;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import com.radix.connect.core.util.WorkManager;
import com.radix.connect.core.util.WorkManager.WorkSpace;
import com.radix.connect.core.util.WorkManager.WorkSpace.Work;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author kiran.chokhande
 */
public final class RxLogger {

  private static final Logger LOGGER = LoggerFactory.getLogger(RxLogger.class);

  private static boolean isLoggerEnable = false;
  private static boolean isSqlQueryLogEnable = false;
  private static boolean isLogPrintingEnable = false;
  private static String site_id = null, site_name = null, queue_url = null;

  private static AmazonSQSAsync amazonSQSAsync = null;
  private static final WorkSpace CONNECT_LOGGER_WORKSPACE;

  private RxLogger() {
  }

  static {
    CONNECT_LOGGER_WORKSPACE = WorkManager.workSpace(WorkSpaceName.LOG_PROCESSOR);
  }

  public static void configureLoggerToProcessLog(String connectCentralLoggerConfigurations) {

    /* Stop all previous submitted workers */
    RxLogger.CONNECT_LOGGER_WORKSPACE.stopAllWorkers();

    final JsonUtil logConfig = new JsonUtil(connectCentralLoggerConfigurations);

    RxLogger.site_id = logConfig.getValue("site_id");
    RxLogger.site_name = logConfig.getValue("site_name");
    RxLogger.queue_url = logConfig.getValue("queue_url");
    RxLogger.isLoggerEnable = logConfig.getAsBoolean("logger_enable");
    RxLogger.isLogPrintingEnable = logConfig.getAsBoolean("log_printing_enable");
    RxLogger.isSqlQueryLogEnable = logConfig.getAsBoolean("sql_query_log_enable");

    RxLogger.amazonSQSAsync = null;
    if (RxLogger.isLoggerEnable) {
      String accessKey = logConfig.getValue("access_key");
      String secretKey = logConfig.getValue("secret_key");
      Validator.requireNonEmpty(queue_url, "Amazon logger queue url must required");

      accessKey = Validator.requireNonEmpty(accessKey, "Amazon access key must required");
      secretKey = Validator.requireNonEmpty(secretKey, "Amazon secret key must required");

      BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKey, secretKey);
      amazonSQSAsync = AmazonSQSAsyncClientBuilder.standard().withRegion(Regions.US_EAST_1)
          .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials)).build();
    }
  }

  private static void processLog(final JsonObject log_payload) {

    RxLogger.CONNECT_LOGGER_WORKSPACE.submitWork(new Work<>(log_payload) {

      @Override
      protected final void doWork(final JsonObject payload) {
        payload.addProperty("radix_site_id", RxLogger.site_id);
        payload.addProperty("radix_site_name", RxLogger.site_name);

        if (RxLogger.isLoggerEnable && RxLogger.amazonSQSAsync != null) {
          RxLogger.amazonSQSAsync.sendMessageAsync(new SendMessageRequest(
              RxLogger.queue_url, String.valueOf(payload)).withDelaySeconds(0));
        }
      }
    });
  }

  public static void logMessage(String message) {
    RxLogger.logMessage("", message);
  }

  public static void logMessage(String context, String message) {
    RxLogger.LOGGER.info(message);
    RxLogger.pushLog("Message", context, message);
  }

  public static void logError(String errorMsg) {
    RxLogger.logError("", errorMsg);
  }

  public static void logError(String context, String errorMsg) {
    RxLogger.LOGGER.error(errorMsg);
    RxLogger.pushLog("Error", context, errorMsg);
  }

  public static void logException(Throwable error) {
    RxLogger.logException("", error);
  }

  public static void logException(String context, Throwable error) {

    if (error != null) {
      error.printStackTrace();

      StringBuilder sb = new StringBuilder();
      sb.append(error.getClass().getName()).append(": ").append(error.getMessage());
      Arrays.stream(error.getStackTrace()).forEach((trace) -> sb.append("\n at ").append(trace));
      RxLogger.pushLog("Exception", context, sb.toString().trim());
    }
  }

  private static void pushLog(String logType, String context, String details) {

    if (RxLogger.isLoggerEnable && !Validator.isEmpty(details)) {
      JsonObject log = new JsonObject();
      log.addProperty("log_type", logType);
      log.addProperty("context", context);
      log.addProperty("details", details);
      RxLogger.processLog(log);
    }
  }

  public static QueryLog logQuery(String query_id, String query) {
    return Validator.isEmpty(query) ? null : new RxLogger.QueryLog(query_id, query);
  }

  public static RequestLog logRequest(String context, HttpServletRequest req) {
    return req == null ? null : new RxLogger.RequestLog(context, req);
  }

  public static RequestLog logRequest(String remoteUserId, String context, HttpGet req) {
    return req == null ? null : new RxLogger.RequestLog(remoteUserId, context, req);
  }

  public static RequestLog logRequest(String remoteUserId, String context, HttpPost req) {
    return req == null ? null : new RxLogger.RequestLog(remoteUserId, context, req);
  }

  /**
   * @author kiran.chokhande
   */
  public static final class QueryLog implements AutoCloseable {

    private final String query_id, query, remoteUserId;
    private final Timestamp queryStartTs = new Timestamp(System.currentTimeMillis());

    private QueryLog(String query_id, String query) {

      query = query == null ? null : query.replaceAll("([\n\r])", "$1\t\t");

      String user_id = null;
      try {
        RequestAttributes requestAttribute = RequestContextHolder.currentRequestAttributes();
        user_id = ((ServletRequestAttributes) requestAttribute).getRequest().getRemoteUser();
      } catch (Exception ignored) {
      }

      this.remoteUserId = Validator.coalesce(user_id, "Unknown");
      this.query_id = query_id;
      this.query = Objects.requireNonNull(query, "Error: SQL Query can not be NULL");
    }

    @Override
    public final void close() {

      Timestamp queryEndTs = new Timestamp(System.currentTimeMillis());
      float timeDuration = (queryEndTs.getTime() - queryStartTs.getTime()) / 1000;

      if (RxLogger.isLoggerEnable && RxLogger.isSqlQueryLogEnable) {
        JsonObject log = new JsonObject();
        log.addProperty("log_type", "SQL Query");
        log.addProperty("remote_user", remoteUserId);
        log.addProperty("query_id", query_id);
        log.addProperty("query", query);
        log.addProperty("query_started_ts", String.valueOf(queryStartTs));
        log.addProperty("query_completed_ts", String.valueOf(queryEndTs));
        log.addProperty("total_time_taken_sec", timeDuration);
        RxLogger.processLog(log);
      }

      if (!RxLogger.isLogPrintingEnable) {
        RxLogger.LOGGER.info("[QUERY_LOG] ==> Executing query, ID: " + query_id);
      } else {
        RxLogger.LOGGER.info("[QUERY_LOG]\n==> Query Log"
            + "\n# =========================================================================================="
            + "\n# Remote_User_ID : " + remoteUserId
            + "\n# SQL_Query_ID   : " + query_id
            + "\n# Started_Time   : " + queryStartTs
            + "\n# Completed_Time : " + queryEndTs
            + "\n# Time_Duration  : " + timeDuration
            + "\n# Executed_Query : " + query
            + "\n# ==========================================================================================");
      }
    }
  }

  /**
   * @author kiran.chokhande
   */
  public static final class RequestLog implements AutoCloseable {

    private int responseStatusCode;
    private JsonElement requestBody, responseBody;
    private String ipAddress, sessionId, queryString;

    private final boolean isThirdPartyRequest;
    private final String remoteUserId, context, requestUrl, method, requestType;
    private final Timestamp requestStartTs = new Timestamp(System.currentTimeMillis());

    private RequestLog(String remoteUserId, String context, HttpGet request) {

      Objects.requireNonNull(request, "Error: HttpPost can not be NULL");

      this.isThirdPartyRequest = true;
      this.requestType = "Third Party Web Request";
      this.remoteUserId = Validator.coalesce(remoteUserId, "Unknown");
      this.context = Validator.coalesce(context, "N/A");
      this.requestUrl = request.getURI().toString();
      this.method = request.getMethod();
    }

    private RequestLog(String remoteUserId, String context, HttpPost request) {

      Objects.requireNonNull(request, "Error: HttpPost can not be NULL");

      this.isThirdPartyRequest = true;
      this.requestType = "Third Party Web Request";
      this.remoteUserId = Validator.coalesce(remoteUserId, "Unknown");
      this.context = Validator.coalesce(context, "N/A");
      this.requestUrl = request.getURI().toString();
      this.method = request.getMethod();

      try {
        this.requestBody = request.getEntity() == null ? null
            : JsonUtil.getAsJsonE(request.getEntity().getContent());
      } catch (Exception ignored) {
      }
    }

    private RequestLog(String context, HttpServletRequest request) {

      Objects.requireNonNull(request, "Error: HttpServletRequest can not be NULL");

      this.isThirdPartyRequest = false;
      this.requestType = "Connect Web Request";
      this.remoteUserId = Validator.coalesce(request.getRemoteUser(), "Unknown");
      this.context = Validator.coalesce(context, "N/A");
      this.ipAddress = request.getRemoteAddr();
      this.sessionId = request.getSession().getId();
      this.requestUrl = request.getRequestURL().toString();
      this.method = request.getMethod();
      this.queryString = request.getQueryString();

      this.requestBody = JsonUtil
          .getAsJsonE(request.getAttribute(Constant.HTTP_REQUEST_KEY_TO_HOLD_PAYLOAD));
      if (this.requestBody == null || this.requestBody.isJsonNull()) {
        final JsonObject requestParameters = new JsonObject();
        if (!Validator.isEmpty(request.getParameterMap())) {
          request.getParameterMap().forEach((key, values) -> requestParameters
              .addProperty(key, values == null || values.length <= 0 ? null : values[0]));
        }
        this.requestBody = requestParameters;
      }
    }

    public final void setResponse(int responseStatusCode, Object responseBody) {
      this.responseStatusCode = responseStatusCode;
      try {
        this.responseBody = JsonUtil.getAsJsonE(responseBody);
      } catch (Exception ignored) {
      }
    }

    @Override
    public final void close() {

      Timestamp requestEndTs = new Timestamp(System.currentTimeMillis());
      float timeDuration = (requestEndTs.getTime() - requestStartTs.getTime()) / 1000;

      if (RxLogger.isLoggerEnable) {
        final JsonObject log = new JsonObject();
        log.addProperty("log_type", requestType);
        log.addProperty("context", context);
        log.addProperty("remote_user", remoteUserId);
        log.addProperty("ip_address", ipAddress);
        log.addProperty("session_id", sessionId);
        log.addProperty("request_url", requestUrl);
        log.addProperty("request_method", method);
        log.addProperty("query_param_str", queryString);
        log.add("request_body", requestBody);
        log.add("response_body", responseBody);
        if (responseStatusCode > 0) {
          log.addProperty("response_status_code", responseStatusCode);
        }
        log.addProperty("request_started_ts", String.valueOf(requestStartTs));
        log.addProperty("request_completed_ts", String.valueOf(requestEndTs));
        log.addProperty("total_time_taken_sec", timeDuration);
        RxLogger.processLog(log);
      }

      if (!RxLogger.isLogPrintingEnable) {
        RxLogger.LOGGER.info(String.format("[REQUEST_LOG] ==> %s:%s", method, requestUrl));
      } else {
        String requestTag = isThirdPartyRequest ? "Third Party Request Log" : "Request Log";
        RxLogger.LOGGER.info("[REQUEST_LOG]\n==> " + requestTag
            + "\n# =========================================================================================="
            + "\n# Remote_User_ID : " + remoteUserId
            + "\n# Ip_Address     : " + ipAddress
            + "\n# Request_Method : " + method
            + "\n# Request_Context: " + context
            + "\n# Request_URL    : " + requestUrl
            + "\n# Started_Time   : " + requestStartTs
            + "\n# Completed_Time : " + requestEndTs
            + "\n# Time_Duration  : " + timeDuration
            + "\n# Request_Payload: " + requestBody
            + "\n# ==========================================================================================");
      }
    }
  }
}
