package com.radix.connect.core;

/**
 * @author kiran.chokhande
 */
@FunctionalInterface
public interface ErrorHandler<Entity> {

  void onError(Entity entity, Exception exception);
}
