package com.radix.connect.core.annotation;

import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.SenderAPI;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Lazy
@Service
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public @interface MsgSender {

  SenderAPI sender_api();

  Modality modality();
}
