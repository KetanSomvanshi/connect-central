package com.radix.connect.core.util;

/**
 * @author kiran.chokhande
 */
public final class BCrypt {

  private static final String SALT = org.springframework.security.crypto.bcrypt.BCrypt.gensalt(10);

  /**
   * It will avoid instantiation for this class
   */
  private BCrypt() {
  }

  /**
   * Check that a plain text string matches a previously hashed one
   *
   * @param plaintext the plain text string to verify
   * @param hashed the previously-hashed string
   * @return true if the plain text and hashed string match, false otherwise
   */
  public static boolean isMatch(String plaintext, String hashed) {
    try {
      return org.springframework.security.crypto.bcrypt.BCrypt.checkpw(plaintext, hashed);
    } catch (IllegalArgumentException iae) {
      return false;
    }
  }

  /**
   * Hash a plain text string using the OpenBSD bcrypt scheme
   *
   * @param plaintext the plain text string to hash
   * @return the hashed string
   */
  public static String hash(String plaintext) {
    return org.springframework.security.crypto.bcrypt.BCrypt.hashpw(plaintext, SALT);
  }

  /**
   * Hash a plain text string using the OpenBSD bcrypt scheme
   *
   * @param plaintext the plain text string to hash
   * @return the hashed string
   */
  public static String encode(String plaintext) {
    return BCrypt.hash(plaintext);
  }
}
