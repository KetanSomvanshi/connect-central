package com.radix.connect.core.util;

import java.util.Objects;
import java.util.StringTokenizer;
import java.util.function.Consumer;

/**
 * @author kiran.chokhande
 */
public final class Tokenizer extends StringTokenizer {

  public Tokenizer(String input) {
    super(input);
  }

  public Tokenizer(String input, String delimiter) {
    this(input, delimiter, true);
  }

  public Tokenizer(String input, String delimiter, boolean returnDelimiters) {
    super(input, delimiter, returnDelimiters);
  }

  public static Tokenizer split(String input, String delimiter) {
    return new Tokenizer(input, delimiter);
  }

  public static Tokenizer split(String input, String delimiter, boolean returnDelimiters) {
    return new Tokenizer(input, delimiter, returnDelimiters);
  }

  public void forEach(Consumer<String> action) {
    Objects.requireNonNull(action, "Error: action can not be NULL");
    while (hasMoreTokens()) {
      action.accept(nextToken());
    }
  }

}
