package com.radix.connect.core.util;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author kiran.chokhande
 */
public final class RepetitiveSequence<Entity> {

  private final List<Entity> entry;
  private final transient AtomicInteger i = new AtomicInteger();

  public RepetitiveSequence(final List<Entity> entry) {
    this.entry = entry == null ? null : Collections.unmodifiableList(entry);
  }

  public final boolean contains(Entity entity) {
    return entry != null && entry.contains(entity);
  }

  public final Entity next() {
    return entry == null || entry.isEmpty() ? null
        : entry.get(i.get() >= entry.size() - 1 ? i.getAndSet(0) : i.getAndIncrement());
  }
}
