package com.radix.connect.core.util;

import java.io.Closeable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author kiran.chokhande
 */
public final class RateLimiter {

  private final AtomicInteger maxRate, current;
  private final Permission permission = new Permission();

  public RateLimiter(final int maxRate) {
    this.current = new AtomicInteger();
    this.maxRate = new AtomicInteger(maxRate <= 0 ? 1 : maxRate);
  }

  public final void setMaxRate(final int maxRate) {
    this.maxRate.set(maxRate <= 0 ? 1 : maxRate);
  }

  public final Permission acquire() {
    permission.tryAcquirePermit();
    return this.permission;
  }

  /**
   * @author kiran.chokhande
   */
  public final class Permission implements Closeable {

    private synchronized void tryAcquirePermit() {
      if (current.get() >= maxRate.get()) {
        try {
          this.wait();
        } catch (Exception ignored) {
        }
      }
      current.incrementAndGet();
    }

    @Override
    public final synchronized void close() {
      current.set(Math.max(0, current.get() - 1));
      this.notify();
    }
  }
}
