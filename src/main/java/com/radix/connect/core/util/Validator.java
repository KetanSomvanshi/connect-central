package com.radix.connect.core.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.error.FieldRequiredException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author kiran.chokhande
 */
public final class Validator {

  private static final Pattern PHONE_REGEX;
  private static final Pattern EMAIL_REGEX;
  private static final Pattern UNICODE_REGEX;

  private Validator() {
  }

  static {
    String unicodeRegex = "[A-Za-z0-9áéíóúàèìòùäåöüÁÉÍÓÚÀÈÌÒÙÄÅÖÜøΘØΦÆÇÑΓΔΛΞßΠ¡Σ¤æ§ΨΩ€ñ<|>¿ \\r\\n@£$¥!\\\"#$%&amp;'\\(\\)*\\-\\+,_.~\\/\\\\:;&lt;=&gt;?^{}\\[\\]]+";
    String phoneRegex = "^\\+?\\d{10,13}$";
    String emailRegex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

    PHONE_REGEX = Pattern.compile(phoneRegex);
    EMAIL_REGEX = Pattern.compile(emailRegex);
    UNICODE_REGEX = Pattern.compile(unicodeRegex);
  }

  /**
   * This function is used to get first non Empty and non Null value from given array
   *
   * @param values input <b>String[]</b>
   * @return first not NULL and not Empty value from given array
   */
  public static String coalesce(String... values) {
    return values == null ? null
        : Arrays.stream(values).filter(v -> !isEmpty(v)).findFirst().orElse(null);
  }

  /**
   * This function is used to get first non Null value from given array
   *
   * @param values input <b>E[]</b>
   * @return first not NULL value from given array
   */
  public static <E> E coalesce(E... values) {
    return values == null ? null
        : Arrays.stream(values).filter(Objects::nonNull).findFirst().orElse(null);
  }

  /**
   * This function is used to check whether input String is NULL or empty
   *
   * @param value input <b>String</b> which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(String value) {
    return value == null || value.trim().isEmpty();
  }

  /**
   * This function is used to check whether input Array is NULL or empty
   *
   * @param value input <b>Array</b> which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(Object[] value) {
    return value == null || value.length <= 0;
  }

  /**
   * This function is used to check whether input byte Array is NULL or empty
   *
   * @param value input <b>byte</b> Array which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(byte[] value) {
    return value == null || value.length <= 0;
  }

  /**
   * This function is used to check whether input short Array is NULL or empty
   *
   * @param value input <b>short</b> Array which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(short[] value) {
    return value == null || value.length <= 0;
  }

  /**
   * This function is used to check whether input integer Array is NULL or empty
   *
   * @param value input <b>integer</b> Array which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(int[] value) {
    return value == null || value.length <= 0;
  }

  /**
   * This function is used to check whether input long Array is NULL or empty
   *
   * @param value input <b>long</b> Array which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(long[] value) {
    return value == null || value.length <= 0;
  }

  /**
   * This function is used to check whether input float Array is NULL or empty
   *
   * @param value input <b>float</b> Array which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(float[] value) {
    return value == null || value.length <= 0;
  }

  /**
   * This function is used to check whether input double Array is NULL or empty
   *
   * @param value input <b>double</b> Array which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(double[] value) {
    return value == null || value.length <= 0;
  }

  /**
   * This function is used to check whether input char Array is NULL or empty
   *
   * @param value input <b>char</b> Array which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(char[] value) {
    return value == null || value.length <= 0;
  }

  /**
   * This function is used to check whether input boolean Array is NULL or empty
   *
   * @param value input <b>boolean</b> Array which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(boolean[] value) {
    return value == null || value.length <= 0;
  }

  /**
   * This function is used to check whether input Collection is NULL or empty
   *
   * @param value input <b>Collection</b> which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(Collection<?> value) {
    return value == null || value.isEmpty();
  }

  /**
   * This function is used to check whether input JsonArray is NULL or empty
   *
   * @param value input <b>JsonArray</b> which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(JsonArray value) {
    return value == null || value.size() <= 0;
  }

  /**
   * This function is used to check whether input JsonObject is NULL or empty
   *
   * @param value input <b>JsonObject</b> which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(JsonObject value) {
    return value == null || value.entrySet().isEmpty();
  }

  /**
   * This function is used to check whether input JsonElement is NULL or empty
   *
   * @param value input <b>JsonElement</b> which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(JsonElement value) {
    return value == null || value.isJsonNull() ||
        (value.isJsonObject() ? isEmpty(value.getAsJsonObject())
            : value.isJsonArray() ? isEmpty(value.getAsJsonArray())
                : !value.isJsonPrimitive() || isEmpty(value.getAsString()));
  }

  /**
   * This function is used to check whether input Map is NULL or empty
   *
   * @param value input <b>Map</b> which you want to check NULL or empty
   * @return <b>true</b>: NULL or empty, <b>false</b>: not NULL and not empty
   */
  public static boolean isEmpty(Map<?, ?> value) {
    return value == null || value.isEmpty();
  }

  /**
   * This function is used to check whether given contact is valid or not
   *
   * @param modality type of contact
   * @param contact phone number or email address
   * @return <b>true</b>: Valid contact, <b>false</b>: Invalid contact
   */
  public static boolean isValidContact(Modality modality, String contact) {

    contact = Validator.sanitizeContact(modality, contact);
    return !Validator.isEmpty(contact) &&
        (modality == Modality.EMAIL ? EMAIL_REGEX : PHONE_REGEX).matcher(contact).matches();
  }

  /**
   * This function is used to sanitize given contact
   *
   * @param modality type of contact
   * @param contact input contact
   * @return valid phone number otherwise NULL
   */
  public static String sanitizeContact(Modality modality, String contact) {

    if (modality == Modality.EMAIL) {
      contact = contact == null ? null : contact.replaceAll("\\s+", "");
    } else {
      contact = contact == null ? null : contact.replaceAll("\\D", "");
      contact = contact == null ? null : contact.length() == 10 ? "1" + contact : contact;
    }
    return Validator.isEmpty(contact) ? null : contact;
  }

  /**
   * This function is used to find unicode characters from given string
   *
   * @param message input string to find unicode characters
   * @return unicode characters if any
   */
  public static String findUnicodeChar(String message) {

    String[] x = isEmpty(message) ? null : UNICODE_REGEX.split(message);
    return x == null || x.length <= 0 ? null : String.join("",
        Arrays.stream(x).filter(c -> !c.isEmpty()).collect(Collectors.toSet()));
  }

  public static <T> T requireNonNull(T t) {
    return requireNonNull(t, null);
  }

  public static <T> T requireNonNull(T t, String error) {

    if (t == null || t instanceof JsonNull) {
      throw new FieldRequiredException(error);
    }
    return t;
  }

  public static <T> T requireNonEmpty(T t) {
    return requireNonEmpty(t, null);
  }

  public static <T> T requireNonEmpty(T t, String error) {

    if (t == null) {
      throw new FieldRequiredException(error);
    }
    if (t instanceof String && isEmpty((String) t)) {
      throw new FieldRequiredException(error);
    }
    if (t instanceof Map && isEmpty((Map<?, ?>) t)) {
      throw new FieldRequiredException(error);
    }
    if (t instanceof Collection && isEmpty((Collection<?>) t)) {
      throw new FieldRequiredException(error);
    }
    if (t instanceof JsonElement && isEmpty((JsonElement) t)) {
      throw new FieldRequiredException(error);
    }
    return t;
  }
}

