package com.radix.connect.core.util;

import com.radix.connect.core.logger.RxLogger;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author kiran.chokhande
 */
public final class WorkManager {

  private static final Map<String, WorkSpace> WORK_SPACE_MAP = new ConcurrentHashMap<>();

  public static synchronized WorkSpace workSpace(final String name) {

    return WORK_SPACE_MAP.computeIfAbsent(name, workSpace -> new WorkSpace(name));
  }

  public static void watchInfiniteWorks() {

    System.out.println("*** Watching Infinite Workers ***");

    WORK_SPACE_MAP.forEach((name, ws) -> ws.infiniteWorksMap.forEach((workId, work) -> {
      if (!work.isWorking()) {
        ws.resumeWork(work);
        RxLogger.logMessage(ws.wSpaceContext, "Work re-started, id:" + workId);
      }
    }));
  }

  public static void shutdownAllWorkSpaces() {
    WORK_SPACE_MAP.forEach((workSpaceName, workSpace) -> workSpace.shutdown());
  }

  /**
   * @author kiran.chokhande
   */
  public static final class WorkSpace {

    private final String name;
    private final String wSpaceContext;

    private final ExecutorService workExecutorsService = Executors.newCachedThreadPool();
    private final Map<String, InfiniteWork> infiniteWorksMap = new ConcurrentHashMap<>();

    @Override
    public String toString() { return "WorkSpace[" + name + "]"; }

    private WorkSpace(final String name) {
      this.name = Validator.requireNonEmpty(name, "WorkSpace name can not  Empty");
      this.wSpaceContext = "WorkSpace: " + name;
      RxLogger.logMessage(wSpaceContext, "New WorkSpace created, name:" + name);
    }

    public final void shutdown() {
      try {
        workExecutorsService.shutdownNow();
      } catch (Exception exception) {
        RxLogger.logException("Error while shutting down work-space:" + name, exception);
      } finally {
        infiniteWorksMap.clear();
        WORK_SPACE_MAP.remove(name);
        RxLogger.logMessage(wSpaceContext, "Shutting down work-space: " + name);
      }
    }

    public final void stopAllWorkers() {
      infiniteWorksMap.forEach((workerId, infiniteWorker) -> infiniteWorker.terminate());
    }

    public final void submitWork(final Work<?> work) {
      submitWorkToExecutor(work);
    }

    public final void submitWork(final InfiniteWork<?> work) {
      work.startWatcher(this, submitWorkToExecutor(work));
    }

    public final void resumeWork(final InfiniteWork<?> work) {

      InfiniteWork oldW = infiniteWorksMap.remove(work.workId);
      if (oldW != null) { oldW.terminate(); }

      work.startWatcher(this, submitWorkToExecutor(work));
    }

    private <Entity> Future<Entity> submitWorkToExecutor(final Callable<Entity> work) {

      if (workExecutorsService.isShutdown() || workExecutorsService.isTerminated()) {
        throw new RuntimeException("Can not accept new work, Workspace has been terminated");
      }
      return workExecutorsService.submit(work);
    }

    /**
     * @author kiran.chokhande
     */
    public abstract static class Work<Entity> implements Callable<Work> {

      private final Entity entity;

      public Work(final Entity entity) { this.entity = entity; }

      @Override
      public final Work call() throws Exception {
        doWork(entity);
        return this;
      }

      protected abstract void doWork(Entity entity) throws Exception;
    }


    /**
     * @author kiran.chokhande
     */
    public abstract static class InfiniteWork<Entity> implements Callable<InfiniteWork> {

      private final Entity entity;
      protected final String workId;

      private WorkSpace ws;
      private Future<InfiniteWork> future;
      private final AtomicInteger continuousFailedCnt = new AtomicInteger();

      public InfiniteWork(final String workId, final Entity entity) {
        this.workId = Validator.requireNonEmpty(workId, "Work ID can not Empty");
        this.entity = entity;
      }

      private synchronized void startWatcher(WorkSpace ws, Future<InfiniteWork> iwf) {

        this.ws = ws;
        if (this.ws.infiniteWorksMap.containsKey(workId)) {
          throw new RuntimeException("InfiniteWork already submitted, work Id: " + workId);
        }

        this.future = Objects.requireNonNull(iwf);
        this.notifyAll();
        this.ws.infiniteWorksMap.put(workId, this);
      }

      protected final boolean isWorking() {
        return future != null && !future.isDone() && !future.isCancelled();
      }

      protected final boolean terminate() {
        try {
          return future != null && future.cancel(true);
        } finally {
          future = null;
          ws.infiniteWorksMap.remove(workId);
          RxLogger.logMessage(ws.wSpaceContext, "Work terminated, id: " + workId);
        }
      }

      protected final boolean terminateOnCondition(boolean terminateCondition) {
        return terminateCondition && terminate();
      }

      protected final void sleepOnCondition(boolean sleepingConditions, int sleepSeconds) {
        try {
          if (sleepingConditions && sleepSeconds > 0) {
            Thread.sleep(sleepSeconds * 1000);
          }
        } catch (InterruptedException ignored) {
        }
      }

      protected final void sleepOnContinuousFailed(int maxFailureCount, int sleepSeconds) {
        if (this.continuousFailedCnt.get() >= maxFailureCount) {
          this.continuousFailedCnt.set(0);
          sleepOnCondition(true, sleepSeconds);
        }
      }

      @Override
      public final InfiniteWork call() throws Exception {

        try {

          synchronized (this) { if (future == null) { this.wait(); } }

          RxLogger.logMessage(ws.wSpaceContext, "Work started for id: " + workId);
          while (future != null && !future.isDone() && !future.isCancelled()) {
            try {
              doWork(entity);
              continuousFailedCnt.set(0);
            } catch (Exception exc) {
              continuousFailedCnt.incrementAndGet();
              String executeWorkError = "Error while executing work id:%s in workspace:%s";
              RxLogger.logException(String.format(executeWorkError, workId, ws.name), exc);
            }
          }
          return this;
        } finally {
          this.future = null;
          RxLogger.logMessage(ws.wSpaceContext, "Work stopped for id: " + workId);
        }
      }

      protected abstract void doWork(Entity entity) throws Exception;
    }
  }
}
