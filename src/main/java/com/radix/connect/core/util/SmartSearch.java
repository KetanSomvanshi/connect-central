package com.radix.connect.core.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.ToStringFunction;
import me.xdrop.fuzzywuzzy.model.BoundExtractedResult;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;

/**
 * <p>
 * The class <b>SmartSearch</b> is used to search records based on phrase
 *
 * @author kiran.chokhande
 */
public final class SmartSearch {

  /**
   * It will avoid instantiation for this class
   */
  private SmartSearch() {
  }

  /**
   * Search records based on provided phrase in provided List of entities
   *
   * @param query The query string to search
   * @param entities A list of entities to search provided phrase
   * @param choices Extracted choices from entity
   * @param cutoff The minimum score to return result
   * @param limit The number of results to return
   * @return List of entities
   */
  public static <E> List<E> extract(String query, Collection<E> entities, Choice<String[], E> choices, int cutoff, int limit) {

    if (choices != null && !Validator.isEmpty(entities) && !Validator.isEmpty(query)) {

      ToStringFunction<E> stringFunction;
      stringFunction = item -> extractOneChoice(query, choices.apply(item));

      List<BoundExtractedResult<E>> list;
      if (limit <= 0) {
        list = FuzzySearch.extractSorted(query.toUpperCase(), entities, stringFunction);
      } else {
        list = FuzzySearch.extractTop(query.toUpperCase(), entities, stringFunction, limit);
      }
      if (!Validator.isEmpty(list)) {
        return list.stream().filter(x -> cutoff <= 0 || x.getScore() >= cutoff)
            .map(BoundExtractedResult::getReferent).collect(Collectors.toList());
      }
    }
    return null;
  }

  private static String extractOneChoice(final String query, final String... choices) {
    ExtractedResult result;
    result = choices == null ? null : FuzzySearch.extractOne(query, Arrays.asList(choices));
    return result == null ? null : result.getString();
  }

  /**
   * Search keys in Map based on provided phrase
   *
   * @param query The query string to search
   * @param entityMap A Map of choices to search provided phrase
   * @param choices Extracted choices from entity map
   * @param cutoff The minimum score to return result
   * @param limit The number of results to return
   * @return Map with resultant keys
   */
  public static <K, V> Map<K, V> extract(String query, Map<K, V> entityMap, Choice<String, K> choices, int cutoff, int limit) {

    if (choices != null && !Validator.isEmpty(entityMap) && !Validator.isEmpty(query)) {

      ToStringFunction<K> stringFunction;
      stringFunction = item -> extractOneChoice(query, choices.apply(item));

      List<BoundExtractedResult<K>> list;
      if (limit <= 0) {
        list = FuzzySearch.extractSorted(query.toUpperCase(), entityMap.keySet(), stringFunction);
      } else {
        list = FuzzySearch.extractTop(query.toUpperCase(), entityMap.keySet(), stringFunction, limit);
      }
      if (!Validator.isEmpty(list)) {

        Map<K, V> result = new HashMap<>();
        list.stream().filter(x -> cutoff <= 0 || x.getScore() >= cutoff)
            .forEach(r -> result.put(r.getReferent(), entityMap.get(r.getReferent())));
        return result;
      }
    }
    return null;
  }

  @FunctionalInterface
  public interface Choice<T, E> {

    T apply(E entity);
  }
}
