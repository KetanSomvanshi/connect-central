package com.radix.connect.core.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * @author kiran.chokhande
 */
public final class JsonUtil {

  private static final Gson GSON = new GsonBuilder().setDateFormat("yyyy-MM-dd h:mm:ss a").create();

  private JsonElement params;

  public JsonUtil(String json) {
    this.params = json == null ? null : JsonParser.parseString(json);
  }

  public JsonUtil(Reader reader) {
    this.params = JsonUtil.getAsJsonE(reader);
  }

  public JsonUtil(InputStream stream) {
    this(new InputStreamReader(stream));
  }

  public JsonUtil(JsonElement params) {
    this.params = params;
  }

  public static JsonElement toJson(String json) {
    return json == null || json.trim().isEmpty() ? null : JsonParser.parseString(json);
  }

  public static String getAsString(InputStream stream) {

    return JsonUtil.getAsString(new InputStreamReader(stream));
  }

  public static JsonElement getAsJsonE(InputStream stream) {

    return JsonUtil.getAsJsonE(new InputStreamReader(stream));
  }

  public static JsonElement getAsJsonE(Reader reader) {

    JsonReader jsonReader = new JsonReader(reader);
    jsonReader.setLenient(true);
    return JsonParser.parseReader(reader);
  }

  public static String getAsString(Reader reader) {

    JsonElement jsonE = JsonUtil.getAsJsonE(reader);
    return jsonE == null || jsonE.isJsonNull() ? null
        : jsonE.isJsonPrimitive() ? jsonE.getAsString() : jsonE.toString();
  }

  public static JsonElement getAsJsonE(Object e) {

    return e == null ? null : e instanceof String ? toJson((String) e)
        : e instanceof JsonElement ? (JsonElement) e : GSON.toJsonTree(e);
  }

  public static String toString(Object entity) {
    return entity == null ? null
        : entity instanceof String ? (String) entity : GSON.toJson(entity);
  }

  public static <E> E toType(String json, Class<E> type) {
    return json == null ? null : GSON.fromJson(json, type);
  }

  public static <E> E toType(JsonElement json, Class<E> type) {
    return json == null || json.isJsonNull() ? null : GSON.fromJson(json, type);
  }

  public void remove(String paramName) {

    JsonObject jsonO = getAsJsonO();
    if (jsonO != null) jsonO.remove(paramName);
  }

  public void add(String paramName, String paramValue) {

    JsonObject jsonO = getAsJsonO();
    if (jsonO != null) jsonO.addProperty(paramName, paramValue);
  }

  public final String getValue(String paramName) {

    JsonObject jsonO = getAsJsonO();
    JsonElement jsonE = jsonO == null ? null : jsonO.get(paramName);
    return jsonE == null || jsonE.isJsonNull() ? null
        : jsonE.isJsonPrimitive() ? jsonE.getAsString() : jsonE.toString();
  }

  public final String getAsString() {

    return params == null || params.isJsonNull() ? null
        : params.isJsonPrimitive() ? params.getAsString() : params.toString();
  }

  public final JsonArray getAsJsonA() {

    return params == null || !params.isJsonArray() ? new JsonArray() : params.getAsJsonArray();
  }

  public final JsonObject getAsJsonO() {

    return params == null || !params.isJsonObject() ? new JsonObject() : params.getAsJsonObject();
  }

  public final JsonArray getAsJsonA(String paramName) {

    JsonObject jsonO = getAsJsonO();
    JsonElement jsonE = jsonO == null ? null : jsonO.get(paramName);
    return jsonE == null || !jsonE.isJsonArray() ? null : jsonE.getAsJsonArray();
  }

  public final JsonObject getAsJsonO(String paramName) {

    JsonObject jsonO = getAsJsonO();
    JsonElement jsonE = jsonO == null ? null : jsonO.get(paramName);
    return jsonE == null || !jsonE.isJsonObject() ? null : jsonE.getAsJsonObject();
  }

  public final JsonElement getAsJsonE(String paramName) {

    JsonObject jsonO = getAsJsonO();
    return jsonO == null ? null : jsonO.get(paramName);
  }

  public final JsonPrimitive getAsJsonP(String paramName) {

    JsonObject jsonO = getAsJsonO();
    JsonElement jsonE = jsonO == null ? null : jsonO.get(paramName);
    return jsonE == null || !jsonE.isJsonPrimitive() ? null : jsonE.getAsJsonPrimitive();
  }

  public final byte getAsByte(String paramName) {

    JsonPrimitive jsonP = getAsJsonP(paramName);
    String value = jsonP == null ? null : jsonP.getAsString();
    return value == null || value.trim().isEmpty() ? 0 : Byte.parseByte(value);
  }

  public final short getAsShort(String paramName) {

    JsonPrimitive jsonP = getAsJsonP(paramName);
    String value = jsonP == null ? null : jsonP.getAsString();
    return value == null || value.trim().isEmpty() ? 0 : Short.parseShort(value);
  }

  public final int getAsInt(String paramName) {

    JsonPrimitive jsonP = getAsJsonP(paramName);
    String value = jsonP == null ? null : jsonP.getAsString();
    return value == null || value.trim().isEmpty() ? 0 : Integer.parseInt(value);
  }

  public final long getAsLong(String paramName) {

    JsonPrimitive jsonP = getAsJsonP(paramName);
    String value = jsonP == null ? null : jsonP.getAsString();
    return value == null || value.trim().isEmpty() ? 0 : Long.parseLong(value);
  }

  public final float getAsFloat(String paramName) {

    JsonPrimitive jsonP = getAsJsonP(paramName);
    String value = jsonP == null ? null : jsonP.getAsString();
    return value == null || value.trim().isEmpty() ? 0.0f : Float.parseFloat(value);
  }

  public final double getAsDouble(String paramName) {

    JsonPrimitive jsonP = getAsJsonP(paramName);
    String value = jsonP == null ? null : jsonP.getAsString();
    return value == null || value.trim().isEmpty() ? 0.0 : Double.parseDouble(value);
  }

  public final boolean getAsBoolean(String paramName) {

    JsonPrimitive jsonP = getAsJsonP(paramName);
    return Boolean.parseBoolean(jsonP == null ? "false" : jsonP.getAsString());
  }
}
