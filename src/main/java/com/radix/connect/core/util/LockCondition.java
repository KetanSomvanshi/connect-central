package com.radix.connect.core.util;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;

/**
 * @author kiran.chokhande
 */
public final class LockCondition implements Condition {

  @Override
  public final synchronized void signal() {
    this.notify();
  }

  @Override
  public final synchronized void signalAll() {
    this.notifyAll();
  }

  @Override
  public final synchronized void await() throws InterruptedException {
    this.wait();
  }

  @Override
  public final synchronized void awaitUninterruptibly() {
    try {
      this.wait();
    } catch (InterruptedException ignored) {
    }
  }

  @Override
  public final synchronized long awaitNanos(long nanosTimeout) throws InterruptedException {

    long deadline = System.nanoTime() + nanosTimeout;
    this.wait(nanosTimeout / 1000000);
    return deadline - System.nanoTime();
  }

  @Override
  public final synchronized boolean await(long time, TimeUnit unit) throws InterruptedException {

    long millisTimeout = unit.toMillis(time);
    long deadline = System.currentTimeMillis() + millisTimeout;
    this.wait(millisTimeout);
    return deadline > System.currentTimeMillis();
  }

  @Override
  public final synchronized boolean awaitUntil(Date deadline) throws InterruptedException {

    long millisTimeout = deadline.getTime() - System.currentTimeMillis();
    this.wait(millisTimeout);
    return deadline.getTime() > System.currentTimeMillis();
  }
}
