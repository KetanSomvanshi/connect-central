package com.radix.connect.core.util;

import com.radix.connect.core.constant.Constant.SystemEnvVar;
import com.radix.connect.error.ConnectException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.postgresql.util.Base64;

/**
 * @author kiran.chokhande
 */
public final class Crypto {

  private static final Key SECRET_KEY;
  private static final SecretKeySpec SECRET_KEY_SPEC;
  private static final SecureRandom SECURE_RANDOM = new SecureRandom();
  private static final String CIPHER_TRANSFORMATION = "AES/CBC/PKCS5Padding";

  private Crypto() {
  }

  static {

    String sysEnvVarName = SystemEnvVar.CRYPTO_SECRET;
    String cryptoSecretKey = System.getenv(sysEnvVarName);
    if (cryptoSecretKey == null || cryptoSecretKey.trim().isEmpty()) {
      throw new ConnectException("Missing system env variable '" + sysEnvVarName + "'");
    }

    try {
      PBEKeySpec key_spec = new PBEKeySpec(cryptoSecretKey.toCharArray());
      SECRET_KEY = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(key_spec);
      SECRET_KEY_SPEC = new SecretKeySpec(Crypto.SECRET_KEY.getEncoded(), "AES");
    } catch (Exception exception) {
      throw new RuntimeException("Failed to configure Crypto to encrypt/decrypt", exception);
    }
  }

  public static String encrypt(char[] text) {
    return text == null ? null : encrypt(String.valueOf(text));
  }

  public static String encrypt(String text) {

    try {

      if (text == null || text.trim().isEmpty()) return null;

      byte[] saltBytes = new byte[20];
      SECURE_RANDOM.nextBytes(saltBytes);
      Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
      cipher.init(1, SECRET_KEY_SPEC);

      byte[] ivBytes = cipher.getParameters().getParameterSpec(IvParameterSpec.class).getIV();
      byte[] textBytes = cipher.doFinal(text.getBytes(StandardCharsets.UTF_8));
      byte[] encrypted = new byte[20 + ivBytes.length + textBytes.length];
      System.arraycopy(saltBytes, 0, encrypted, 0, 20);
      System.arraycopy(ivBytes, 0, encrypted, 20, ivBytes.length);
      System.arraycopy(textBytes, 0, encrypted, ivBytes.length + 20, textBytes.length);
      return Base64.encodeBytes(encrypted);
    } catch (Exception exception) {
      throw new RuntimeException("Failed to encrypt text: " + text, exception);
    }
  }

  public static String decrypt(char[] text) {
    return text == null ? null : decrypt(new String(text));
  }

  public static String decrypt(String text) {

    try {

      if (text == null || text.trim().isEmpty()) return null;

      Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
      ByteBuffer buffer = ByteBuffer.wrap(Base64.decode(text));
      buffer.get(new byte[20], 0, 20);
      byte[] ivBytes = new byte[cipher.getBlockSize()];
      buffer.get(ivBytes, 0, ivBytes.length);
      cipher.init(2, SECRET_KEY_SPEC, new IvParameterSpec(ivBytes));
      byte[] encrypted = new byte[buffer.capacity() - ivBytes.length - 20];
      buffer.get(encrypted);
      byte[] decrypted = cipher.doFinal(encrypted);

      return decrypted == null ? null : new String(decrypted);
    } catch (Exception exception) {
      throw new RuntimeException("Failed to decrypt text: " + text, exception);
    }
  }
}
