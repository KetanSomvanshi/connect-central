package com.radix.connect.core.util;

import com.radix.connect.core.constant.Modality;
import com.radix.connect.error.ConnectException;
import java.util.UUID;

/**
 * @author kiran.chokhande
 */
public final class IdGenerator {

  private IdGenerator() {
  }

  public static String createSenderId() {
    return ("SN" + UUID.randomUUID()).replaceAll("-", "");
  }

  public static String createAccountId() {
    return ("AC" + UUID.randomUUID()).replaceAll("-", "");
  }

  public static String createAuthToken() {
    return UUID.randomUUID().toString().replaceAll("-", "");
  }

  public static String createMessageId(final Modality modality) {

    String prefix = modality == Modality.TEXT ? "SM" : modality == Modality.FAX ? "FX"
        : modality == Modality.EMAIL ? "EM" : modality == Modality.VOICE ? "CA" : null;

    if (!Validator.isEmpty(prefix)) {
      return (prefix + UUID.randomUUID()).replaceAll("-", "");
    }
    throw new ConnectException("Unsupported modality to create Message ID, modality:" + modality);
  }
}
