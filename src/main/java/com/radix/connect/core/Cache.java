package com.radix.connect.core;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/**
 * @author kiran.chokhande
 */
public interface Cache<Value extends Serializable> {

  void put(final String key, final Value value);

  boolean isEmpty();

  boolean contains(final String key);

  Set<String> keys();

  List<Value> list();

  Map<String, Value> map();

  Value get(final String key);

  Value computeIfAbsent(final String key, final Function<String, Value> valueFunction);

  void remove(final String... keys);

  void remove(final Collection<String> keys);

  void clearCache();
}
