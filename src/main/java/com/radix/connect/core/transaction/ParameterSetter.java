package com.radix.connect.core.transaction;

/**
 * @author kiran.chokhande
 */
@FunctionalInterface
public interface ParameterSetter {

  ParameterSetter NULLABLE = null;

  void setValues(Statement statement) throws Exception;
}