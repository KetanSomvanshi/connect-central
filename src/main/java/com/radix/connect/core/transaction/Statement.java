package com.radix.connect.core.transaction;

import java.util.Date;

/**
 * @author kiran.chokhande
 */
public interface Statement {

  void setNumber(String paramName, Number value) throws Exception;

  void setNumber(int parameterIndex, Number value) throws Exception;

  void setString(String paramName, String value) throws Exception;

  void setString(int parameterIndex, String value) throws Exception;

  void setString(String paramName, Object value) throws Exception;

  void setString(int parameterIndex, Object value) throws Exception;

  void setObject(String paramName, Object value) throws Exception;

  void setObject(int parameterIndex, Object value) throws Exception;

  void setValue(String paramName, boolean value) throws Exception;

  void setValue(int parameterIndex, boolean value) throws Exception;

  void setValue(String paramName, Date value) throws Exception;

  void setValue(int parameterIndex, Date value) throws Exception;

  void setArray(String paramName, Object[] value) throws Exception;

  void setArray(int parameterIndex, Object[] value) throws Exception;

  void addBatch() throws Exception;
}