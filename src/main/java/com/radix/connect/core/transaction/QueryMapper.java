package com.radix.connect.core.transaction;

import com.radix.connect.core.logger.RxLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.commons.lang.StringEscapeUtils;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

/**
 * @author kiran.chokhande
 */
public final class QueryMapper {

  private static final String QUERY_ID = "id", QUERY_PREFIX = "prefix";
  private static final String MAPPER_XML_FILE_EXTENSION = ".mapper.xml";
  private static final String EMPTY_STRING = "", BRACKET_REGEX = "[#\\{\\}]";

  private static final String MAPPER_XSD_FILE = "classpath:sql/xsd/mapper.xsd";
  private static final String MAPPER_XML_FILE_BASE_DIRECTORY = "classpath:sql/mapper/*";

  private static final Pattern INDEX_PARAM_PATTERN = Pattern.compile("\\?");
  private static final Pattern NUM_PARAM_PATTERN = Pattern.compile("#\\{P:\\d+\\}");
  private static final Pattern NAMED_PARAM_PATTERN = Pattern.compile("#\\{[a-zA-Z0-9_$]+\\}");

  private static final String LOADS_MAPPER_XML_ERR = "Failed to load mapper xml file: %s";
  private static final String NO_QRY_CONF_ER = "No sql query found with given query id: %s";

  private static final ResourcePatternResolver RESOURCE_LOADER;
  private static final SAXBuilder SAX_BUILDER = new SAXBuilder();
  private static final Map<String, Query> QUERY_MAP = new HashMap<>();

  static {
    try {

      RESOURCE_LOADER = new PathMatchingResourcePatternResolver();

      SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      Validator validator = factory.newSchema(new StreamSource(
          RESOURCE_LOADER.getResource(MAPPER_XSD_FILE).getInputStream())).newValidator();

      QueryMapper.loadMapperXmlDir(validator, MAPPER_XML_FILE_BASE_DIRECTORY);
    } catch (RuntimeException ex) {
      throw ex;
    } catch (Exception exception) {
      throw new RuntimeException("Failed to load mapper xml file", exception);
    }
  }

  private static void loadMapperXmlDir(final Validator validator, final String mapperXmlDir) {

    try {

      Arrays.stream(RESOURCE_LOADER.getResources(mapperXmlDir)).forEach(mapperXmlResource -> {
        final String name = mapperXmlResource.getFilename();
        if (name != null && !name.trim().isEmpty()) {
          if (name.endsWith(MAPPER_XML_FILE_EXTENSION)) {
            loadMapperXmlFile(validator, mapperXmlResource);
          } else if (!name.contains(".")) {
            loadMapperXmlDir(validator, mapperXmlDir.replace("*", name + "/*"));
          }
        }
      });
    } catch (Exception e) {
      throw new RuntimeException("Failed to load mapper xml file's from: " + mapperXmlDir, e);
    }
  }

  private static void loadMapperXmlFile(final Validator validator, final Resource mResource) {

    try {

      validator.validate(new StreamSource(mResource.getInputStream()));
      Element mapperXmlFileE = SAX_BUILDER.build(mResource.getInputStream()).getRootElement();

      String mapperPrefix = mapperXmlFileE.getAttributeValue(QUERY_PREFIX).trim();
      mapperXmlFileE.getChildren().stream().filter(Objects::nonNull).forEach(childE -> {
        String id = childE.getAttributeValue(QUERY_ID), query = childE.getTextTrim();
        if (id != null && !id.isEmpty() && query != null && !query.isEmpty()) {
          String queryId = mapperPrefix + "." + id.trim();
          QUERY_MAP.put(queryId, new Query(queryId, query.trim()));
        }
      });

      RxLogger.logMessage("*** Loaded mapper xml file: '" + mResource.getFilename() + "'");
    } catch (Exception e) {
      throw new RuntimeException(String.format(LOADS_MAPPER_XML_ERR, mResource.getFilename()), e);
    }
  }

  public static Query get(final String queryId) {
    return Objects.requireNonNull(QUERY_MAP.get(queryId), String.format(NO_QRY_CONF_ER, queryId));
  }

  /**
   * @author kiran.chokhande
   */
  public static final class Query {

    private final Map<String, Set<Integer>> namedParameterIndex;
    private final String queryId, query, queryWithNumIndexParam;

    public Query(final String queryId, final String query) {

      Objects.requireNonNull(query, "SQL query string can not NULL or Empty");
      this.queryId = Objects.requireNonNull(queryId, "Query ID can't NULL or Empty");

      Matcher queryNamedParameterMatcher = NAMED_PARAM_PATTERN.matcher(query);

      Map<String, Set<Integer>> namedParamIndex = new HashMap<>();
      AtomicInteger parameterIndexNo = new AtomicInteger(0);

      AtomicBoolean isSqlQueryContainsNamedParameter = new AtomicBoolean(false);
      String queryWithNumIndexParam = queryNamedParameterMatcher.replaceAll(match -> {
        namedParamIndex.computeIfAbsent(match.group().replaceAll(BRACKET_REGEX, EMPTY_STRING),
            namedParameterIndexes -> new HashSet<>()).add(parameterIndexNo.incrementAndGet());
        isSqlQueryContainsNamedParameter.set(true);
        return "#{P:" + parameterIndexNo.get() + "}";
      });
      if (!isSqlQueryContainsNamedParameter.get()) {
        parameterIndexNo.set(0);
        queryWithNumIndexParam = INDEX_PARAM_PATTERN.matcher(query)
            .replaceAll(indexParamMatch -> "#{P:" + parameterIndexNo.incrementAndGet() + "}");
      }

      this.queryWithNumIndexParam = queryWithNumIndexParam;
      this.query = queryNamedParameterMatcher.replaceAll("?");
      this.namedParameterIndex = Collections.unmodifiableMap(namedParamIndex);
    }

    @Override
    public String toString() {
      return query;
    }

    public String getQueryId() {
      return queryId;
    }

    public String getQuery() {
      return query;
    }

    public Set<Integer> getParamIndex(String paramName) {
      return namedParameterIndex.get(paramName);
    }

    private String encodeToSql(String value) {
      value = value == null ? null : value.replace("$", "\\$");
      return value == null ? "NULL" : "'" + StringEscapeUtils.escapeSql(value) + "'";
    }

    public String buildQuery(final Map<Integer, Map<Integer, Object>> indexParameterValues) {

      final Matcher queryNumParamMatcher = NUM_PARAM_PATTERN.matcher(queryWithNumIndexParam);

      final List<String> queriesWithValue = new ArrayList<>();
      indexParameterValues.forEach((batchIndex, indexParameterValueMap) -> queriesWithValue
          .add(queryNumParamMatcher.replaceAll(match -> {
            int id = Integer.parseInt(match.group().replaceAll("\\D", ""));

            Object value = indexParameterValueMap.get(id);
            return value == null ? "NULL" : value instanceof Number ? value.toString()
                : value instanceof Boolean ? value.toString() : encodeToSql(value.toString());
          })));

      if (queriesWithValue.isEmpty() && indexParameterValues.isEmpty()) {
        queriesWithValue.add(query);
      }
      return queriesWithValue.isEmpty() ? null : String.join("\n", queriesWithValue);
    }
  }
}
