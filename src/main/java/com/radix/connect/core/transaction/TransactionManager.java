package com.radix.connect.core.transaction;

import com.radix.connect.core.Builder;
import com.radix.connect.core.Configurable;
import com.radix.connect.core.constant.TxnType;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.TransactionException;
import javax.sql.DataSource;

/**
 * @author kiran.chokhande
 */
public final class TransactionManager implements Configurable<TxnType> {

  private volatile TransactionFactory transactionFactory;

  private final Builder<TxnType, ? extends TransactionFactory> txnFactoryBuilder;

  private static final String TXN_MANAGER_NOT_CONFIG = "Must configure transaction manager before use";
  private static final String TXN_FAC_BUILDER_REQ = "Transaction factory builder must be required for initializing transaction manager";
  private static final String TRANSACTION_FACTORY_TYPE_REQUIRED_ER = "Transaction factory type must be required to configure transaction manager";
  private static final String NO_TRANSACTION_FACTORY_IMPLEMENTED_OR_CONFIGURED_ERR = "Transaction factory not implemented or not configured for txn factory type: %s";

  public TransactionManager(Builder<TxnType, ? extends TransactionFactory> txnFactoryBuilder) {
    this.txnFactoryBuilder = Validator.requireNonNull(txnFactoryBuilder, TXN_FAC_BUILDER_REQ);
  }

  @Override
  public final void configure(final TxnType txnType) {

    Validator.requireNonNull(txnType, TRANSACTION_FACTORY_TYPE_REQUIRED_ER);

    transactionFactory = Validator.requireNonNull(txnFactoryBuilder.build(txnType),
        String.format(NO_TRANSACTION_FACTORY_IMPLEMENTED_OR_CONFIGURED_ERR, txnType));
  }

  public final Transaction beginTransaction() {
    return beginTransaction(true);
  }

  public final Transaction beginTransaction(final boolean autoCommitMode) {

    try {

      Validator.requireNonNull(transactionFactory, TXN_MANAGER_NOT_CONFIG);

      Transaction transaction = transactionFactory.getTransaction();
      transaction.setAutoCommit(autoCommitMode);
      return transaction;
    } catch (TransactionException exception) {
      throw exception;
    } catch (Exception exception) {
      RxLogger.logException("Begin Transaction", exception);
      throw new TransactionException(exception.getMessage(), exception);
    }
  }

  /**
   * @author kiran.chokhande
   */
  public interface TransactionFactory extends Configurable<DataSource> {

    Transaction getTransaction() throws Exception;
  }
}
