package com.radix.connect.core.transaction;

import java.sql.ResultSet;

/**
 * @author kiran.chokhande
 */
@FunctionalInterface
public interface ResultExtractor<T> {

  ResultExtractor NULLABLE = null;

  T extract(ResultSet result) throws Exception;
}