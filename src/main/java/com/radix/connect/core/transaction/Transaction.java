package com.radix.connect.core.transaction;

import com.radix.connect.core.transaction.QueryMapper.Query;

/**
 * @author kiran.chokhande
 */
public interface Transaction extends AutoCloseable {

  void setAutoCommit(boolean autoCommitMode);

  <E> E select(Query query, ResultExtractor<E> extractor);

  <E> E select(String query_id, ResultExtractor<E> extractor);

  <E> E select(String query_id, ResultExtractor<E> extractor, Object... params);

  <E> E select(String query_id, Object[] params, ResultExtractor<E> extractor);

  <E> E select(String query_id, ResultExtractor<E> extractor, ParameterSetter setter);

  <E> E select(String query_id, ParameterSetter setter, ResultExtractor<E> extractor);

  int update(Query query);

  int update(String query_id);

  int update(String query_id, Object... params);

  int update(String query_id, ParameterSetter setter);

  int[] batchUpdate(String query_id, ParameterSetter setter);

  void commit();

  void rollback();

  void close();
}
