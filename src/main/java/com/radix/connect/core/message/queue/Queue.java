package com.radix.connect.core.message.queue;

import com.radix.connect.core.Configurable;
import com.radix.connect.core.Handler;
import com.radix.connect.core.model.QueueConfig;
import com.radix.connect.core.model.QueueMsg;
import java.util.List;

/**
 * @author kiran.chokhande
 */
public interface Queue extends Configurable<QueueConfig> {

  List<QueueMsg> select();

  void insert(QueueMsg queueMsg, Handler<QueueMsg> handler);

  void delete(QueueMsg queueMsg, Handler<QueueMsg> handler);
}
