package com.radix.connect.core.message.sender;

import com.radix.connect.core.Configurable;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.SenderConfig;
import com.radix.connect.core.model.response.MsgProcessResponse;

/**
 * @author kiran.chokhande
 */
public interface Sender extends Configurable<SenderConfig> {

  MsgProcessResponse fetchStatus(Message message);

  MsgProcessResponse sendMessage(Message message, boolean isResponseMessage);
}
