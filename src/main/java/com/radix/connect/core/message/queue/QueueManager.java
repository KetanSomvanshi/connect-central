package com.radix.connect.core.message.queue;

import com.radix.connect.core.Builder;
import com.radix.connect.core.Configurable;
import com.radix.connect.core.Consumer;
import com.radix.connect.core.ErrorHandler;
import com.radix.connect.core.Handler;
import com.radix.connect.core.constant.QueueType;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.model.QueueConfig;
import com.radix.connect.core.model.QueueMsg;
import com.radix.connect.core.util.Validator;
import com.radix.connect.core.util.WorkManager.WorkSpace.InfiniteWork;
import java.util.List;

/**
 * @author kiran.chokhande
 */
public final class QueueManager implements Configurable<QueueConfig> {

  private volatile Queue queue;

  private final Builder<QueueConfig, ? extends Queue> queueBuilder;

  private static final String CONTEXT = "Message Queue";
  private static final String QUEUE_BUILDER_REQUIRED_ER = "Queue builder must be required for initializing queue manager";
  private static final String QUEUE_CONF_REQ_ERR = "Queue config must be required to configure message queue into queue manager";
  private static final String NO_QUEUE_IMPL = "Queue not implemented for queue type: %s";
  private static final String QUEUE_CONFIGURED_SUCCESSFULLY = "Queue configured with queue manager for queue type: %s";

  private static final String MESSAGE_CONSUMING = "*** Consuming messages from queue, Consumer Id: %s";
  private static final String MESSAGE_CONSUME_CNT = "Message consumed from queue, Consumer: %s, Consumed message count: %d";
  private static final String MESSAGE_DELETED = "Message deleted from queue, Account ID: %s, Message ID: %s";
  private static final String MESSAGE_DELETE_ER = "Failed to delete message from queue, Account ID: %s, Message ID: %s";
  private static final String MESSAGE_INSERTS = "Message produced into queue, Account ID: %s, Message ID: %s";
  private static final String MESSAGE_INSERT_ER = "Failed to produce message into queue, Account ID: %s, Message ID: %s";

  public QueueManager(final Builder<QueueConfig, ? extends Queue> queueBuilder) {

    this.queueBuilder = Validator.requireNonNull(queueBuilder, QUEUE_BUILDER_REQUIRED_ER);
  }

  @Override
  public final void configure(final QueueConfig queueConfig) {

    QueueType queueType = queueConfig == null ? null : queueConfig.getQueue_type();

    Queue queue;
    queue = queueBuilder.build(Validator.requireNonNull(queueConfig, QUEUE_CONF_REQ_ERR));
    this.queue = Validator.requireNonNull(queue, String.format(NO_QUEUE_IMPL, queueType));
    RxLogger.logMessage(CONTEXT, String.format(QUEUE_CONFIGURED_SUCCESSFULLY, queueType));
  }

  private void runPreCheck() {
    Validator.requireNonNull(queue, "Configure queue with queue manager before use");
  }

  public final void produce(final QueueMsg message, final ErrorHandler<QueueMsg> handler) {

    runPreCheck();
    queue.insert(message, new Handler<>() {
      @Override
      public void onSuccess(final QueueMsg queueMsg) {
        String accountId = message.getAccount_id(), messageId = message.getMessage_id();
        RxLogger.logMessage(CONTEXT, String.format(MESSAGE_INSERTS, accountId, messageId));
      }

      @Override
      public void onFailure(final QueueMsg queueMsg, final Exception exception) {
        String accountId = message.getAccount_id(), messageId = message.getMessage_id();
        RxLogger.logError(CONTEXT, String.format(MESSAGE_INSERT_ER, accountId, messageId));
        if (handler != null) handler.onError(queueMsg, exception);
      }
    });
  }

  public final void delete(final QueueMsg message, final ErrorHandler<QueueMsg> handler) {

    runPreCheck();
    queue.delete(message, new Handler<>() {
      @Override
      public void onSuccess(final QueueMsg queueMsg) {
        String accountId = message.getAccount_id(), messageId = message.getMessage_id();
        RxLogger.logMessage(CONTEXT, String.format(MESSAGE_DELETED, accountId, messageId));
      }

      @Override
      public void onFailure(final QueueMsg queueMsg, final Exception exception) {
        String accountId = message.getAccount_id(), messageId = message.getMessage_id();
        RxLogger.logError(CONTEXT, String.format(MESSAGE_DELETE_ER, accountId, messageId));
        if (handler != null) handler.onError(queueMsg, exception);
      }
    });
  }

  public final QueueConsumer consumer(String id, final Consumer<List<QueueMsg>> consumer) {
    return new QueueConsumer(id, consumer);
  }

  /**
   * @author kiran.chokhande
   */
  public final class QueueConsumer extends InfiniteWork<Consumer<List<QueueMsg>>> {

    private QueueConsumer(String consumerId, Consumer<List<QueueMsg>> consumer) {
      super(consumerId, consumer);
    }

    @Override
    protected void doWork(Consumer<List<QueueMsg>> consumer) {

      System.out.println(String.format(MESSAGE_CONSUMING, workId));
      super.sleepOnContinuousFailed(10, 300);

      runPreCheck();

      final List<QueueMsg> messages = queue.select();
      if (messages != null && !messages.isEmpty()) {
        int msCount = messages.size();
        RxLogger.logMessage(CONTEXT, String.format(MESSAGE_CONSUME_CNT, workId, msCount));
        consumer.accept(messages);
      }
    }
  }
}
