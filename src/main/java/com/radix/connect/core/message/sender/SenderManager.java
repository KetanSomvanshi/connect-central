package com.radix.connect.core.message.sender;

import com.radix.connect.core.Builder;
import com.radix.connect.core.Reloadable;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.SenderConfig;
import com.radix.connect.core.model.response.MsgProcessResponse;
import com.radix.connect.core.util.Validator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author kiran.chokhande
 */
public final class SenderManager implements Reloadable<SenderConfig> {

  private final Builder<Modality, ? extends SenderFactory> senderFactoryBuilder;
  private final Map<Modality, SenderFactory> senderFactoryMap = new ConcurrentHashMap<>();

  private static final String SENDER_FACTORY_BUILDER_REQUIRED_ERROR = "Sender factory builder must be required for initializing sender manager";
  private static final String NO_FACTORY_IMPL_ER = "Sender factory not implemented for modality: %s";
  private static final String NO_SENDER_IMPL_OR_CONFIGURE_ERR = "Sender not implemented or not configured for modality: %s and account id: %s";

  public SenderManager(final Builder<Modality, ? extends SenderFactory> factoryBuilder) {
    this.senderFactoryBuilder = Validator
        .requireNonNull(factoryBuilder, SENDER_FACTORY_BUILDER_REQUIRED_ERROR);
  }

  @Override
  public final void reload(final SenderConfig senderConfig) {

    if (senderConfig != null && senderConfig.getModality() != null) {
      getSenderFactory(senderConfig.getModality()).reload(senderConfig.getSender_id());
    }
  }

  private SenderFactory getSenderFactory(final Modality modality) {

    SenderFactory senderFactory;
    if ((senderFactory = senderFactoryMap.get(modality)) != null) {
      return senderFactory;
    }

    synchronized (senderFactoryMap) {
      SenderFactory factory = senderFactoryMap.get(modality);
      if (factory == null) {
        this.senderFactoryMap.put(modality, factory = senderFactoryBuilder.build(modality));
      }
      return Validator.requireNonNull(factory, String.format(NO_FACTORY_IMPL_ER, modality));
    }
  }

  private Sender fetchSupportedSenderForMessage(final Message message) {

    Modality modality = message.getModality();
    SenderFactory senderFactory = getSenderFactory(modality);
    return Validator.requireNonNull(senderFactory.getSender(message.getAccount_id()),
        String.format(NO_SENDER_IMPL_OR_CONFIGURE_ERR, modality, message.getAccount_id()));
  }

  public final MsgProcessResponse fetchStatus(Message message) {

    if (Validator.isEmpty(message.getMsg_sid()) || message.getProcessed_ts() == null) {
      return null;
    }
    return fetchSupportedSenderForMessage(message).fetchStatus(message);
  }

  public final MsgProcessResponse sendMessage(Message message, boolean isResponseMessage) {
    return fetchSupportedSenderForMessage(message).sendMessage(message, isResponseMessage);
  }

  /**
   * @author kiran.chokhande
   */
  public interface SenderFactory extends Reloadable<String> {

    Sender getSender(String accountId);
  }
}
