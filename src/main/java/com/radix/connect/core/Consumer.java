package com.radix.connect.core;

/**
 * @author kiran.chokhande
 */
@FunctionalInterface
public interface Consumer<Entity> {

  /**
   * Performs this operation on the given argument.
   *
   * @param entity the input argument
   */
  void accept(Entity entity);
}
