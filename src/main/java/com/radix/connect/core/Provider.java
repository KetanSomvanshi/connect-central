package com.radix.connect.core;

/**
 * @author kiran.chokhande
 */
@FunctionalInterface
public interface Provider<Input, Output> {

  Output provide(Input value);
}
