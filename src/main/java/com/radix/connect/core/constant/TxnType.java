package com.radix.connect.core.constant;

import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;

/**
 * @author kiran.chokhande
 */
public enum TxnType {

  JDBC_TXN, HIBERNATE_TXN;

  public static TxnType forValue(final String type) {
    try {
      return Validator.isEmpty(type) ? null : valueOf(type.trim().toUpperCase());
    } catch (Exception exc) {
      throw new ConnectException("Found invalid transaction type value:" + type);
    }
  }
}