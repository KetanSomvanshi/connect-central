package com.radix.connect.core.constant;

import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;

/**
 * @author kiran.chokhande
 */
public enum Status {

  QUEUED, SKIPPED, TIME_OUT, TIME_BLOCK, SENT, IN_PROCESS, COMPLETED, DELIVERED,
  UNDELIVERED, RECEIVED, BUSY, NO_ANSWER, CANCELED, BOUNCE, REJECTED, FAILED, EXCEPTION;

  public static Status forValue(final String status) {
    try {
      return Validator.isEmpty(status) ? null : valueOf(status.trim().toUpperCase());
    } catch (Exception exc) {
      throw new ConnectException("Found invalid message status value: " + status);
    }
  }

  public boolean isSuccess() {
    switch (this) {
      case QUEUED:
      case SENT:
      case RECEIVED:
      case DELIVERED:
      case COMPLETED:
      case IN_PROCESS:
        return true;
      default:
        return false;
    }
  }

  public boolean isInProcess() {
    switch (this) {
      case SENT:
      case IN_PROCESS:
        return true;
      default:
        return false;
    }
  }
}