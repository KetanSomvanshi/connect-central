package com.radix.connect.core.constant;

import com.radix.connect.error.ConnectException;
import com.radix.connect.core.util.Validator;

/**
 * @author kiran.chokhande
 */
public enum Direction {

  INBOUND, OUTBOUND;

  public static Direction forValue(final String direction) {
    try {
      return Validator.isEmpty(direction) ? null : valueOf(direction.trim().toUpperCase());
    } catch (Exception exc) {
      throw new ConnectException("Found invalid message direction value: " + direction);
    }
  }
}