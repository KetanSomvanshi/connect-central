package com.radix.connect.core.constant;

import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;

/**
 * @author kiran.chokhande
 */
public enum QueueType {

  AMAZON_MQ, RABBIT_MQ;

  public static QueueType forValue(final String type) {
    try {
      return Validator.isEmpty(type) ? null : valueOf(type.trim().toUpperCase());
    } catch (Exception exc) {
      throw new ConnectException("Found invalid message queue type value: " + type);
    }
  }
}