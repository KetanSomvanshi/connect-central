package com.radix.connect.core.constant;

import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;

/**
 * @author kiran.chokhande
 */
public enum SenderAPI {

  AMAZON_API, SMTP_API, INTERFAX_API, TWILIO_API;

  public static SenderAPI forValue(final String api) {
    try {
      return Validator.isEmpty(api) ? null : valueOf(api.trim().toUpperCase());
    } catch (Exception exc) {
      throw new ConnectException("Found invalid msg sender api value: " + api);
    }
  }
}