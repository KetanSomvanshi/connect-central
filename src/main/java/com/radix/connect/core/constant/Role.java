package com.radix.connect.core.constant;

import com.radix.connect.error.ConnectException;
import com.radix.connect.core.util.Validator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * @author kiran.chokhande
 */
public enum Role {

  ROLE_API, ROLE_USER, ROLE_ADMIN;

  private final GrantedAuthority authority;

  Role() {
    this.authority = new SimpleGrantedAuthority(toString());
  }

  public GrantedAuthority getAuthority() {
    return authority;
  }

  public static Role forValue(final String role) {
    try {
      return Validator.isEmpty(role) ? null : valueOf(role.trim().toUpperCase());
    } catch (Exception exc) {
      throw new ConnectException("Found invalid role value: " + role);
    }
  }
}