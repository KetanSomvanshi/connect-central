package com.radix.connect.core.constant;

import com.radix.connect.error.ConnectException;
import com.radix.connect.core.util.Validator;

/**
 * @author kiran.chokhande
 */
public enum Modality {

  TEXT, VOICE, EMAIL, FAX;

  public static Modality forValue(final String modality) {
    try {
      return Validator.isEmpty(modality) ? null : valueOf(modality.trim().toUpperCase());
    } catch (Exception exc) {
      throw new ConnectException("Found invalid modality value: " + modality);
    }
  }
}