package com.radix.connect.core;

/**
 * @author kiran.chokhande
 */
public interface Handler<Entity> {

  void onSuccess(Entity entity);

  void onFailure(Entity entity, Exception exception);
}
