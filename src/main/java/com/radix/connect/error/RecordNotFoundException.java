package com.radix.connect.error;

/**
 * @author kiran.chokhande
 */
public final class RecordNotFoundException extends ConnectException {

  public RecordNotFoundException(String message) {
    super(message);
  }
}
