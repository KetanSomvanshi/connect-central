package com.radix.connect.error;

/**
 * @author kiran.chokhande
 */
public class ConnectException extends RuntimeException {

  public ConnectException(String message) {
    super(message);
  }

  public ConnectException(String message, Throwable cause) {
    super(message, cause);
  }
}
