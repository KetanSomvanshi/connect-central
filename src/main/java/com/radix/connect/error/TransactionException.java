package com.radix.connect.error;

/**
 * @author kiran.chokhande
 */
public final class TransactionException extends RuntimeException {

  public TransactionException(String message) {
    super(message);
  }

  public TransactionException(String message, Throwable cause) {
    super(message, cause);
  }
}
