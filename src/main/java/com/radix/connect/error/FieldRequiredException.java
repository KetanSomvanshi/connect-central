package com.radix.connect.error;

/**
 * @author kiran.chokhande
 */
public final class FieldRequiredException extends ConnectException {

  public FieldRequiredException(String message) {
    super(message);
  }
}
