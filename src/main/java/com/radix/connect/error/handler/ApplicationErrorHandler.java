package com.radix.connect.error.handler;

import com.radix.connect.error.ConnectException;
import com.radix.connect.error.RecordNotFoundException;
import com.radix.connect.core.model.response.ApiResponse;
import javax.ws.rs.ForbiddenException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * @author kiran.chokhande
 */
@ControllerAdvice
public final class ApplicationErrorHandler {

  @ExceptionHandler(RecordNotFoundException.class)
  public ResponseEntity handleException(RecordNotFoundException exc) {
    System.out.println("ConnectException handler:" + exc);

    return buildResponseEntity(HttpStatus.NOT_FOUND, exc.getMessage());
  }

  @ExceptionHandler(AuthenticationException.class)
  public ResponseEntity handleException(AuthenticationException exc) {
    System.out.println("AuthenticationException handler:" + exc);

    return buildResponseEntity(HttpStatus.UNAUTHORIZED, exc.getMessage());
  }

  @ExceptionHandler(ConnectException.class)
  public ResponseEntity handleException(ConnectException exc) {
    System.out.println("ConnectException handler:" + exc);

    return buildResponseEntity(HttpStatus.BAD_REQUEST, exc.getMessage());
  }

  @ExceptionHandler(ForbiddenException.class)
  public ResponseEntity handleException(ForbiddenException exc) {
    System.out.println("ForbiddenException handler:" + exc);

    return buildResponseEntity(HttpStatus.FORBIDDEN, exc.getMessage());
  }

  @ExceptionHandler(NoHandlerFoundException.class)
  public ResponseEntity handleException(NoHandlerFoundException exc) {
    System.out.println("NoHandlerFoundException handler1:" + exc);

    return buildResponseEntity(HttpStatus.NOT_FOUND, exc.getMessage());
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity handleException(Exception exc) {
    System.out.println("Default Exception handler:" + exc);

    return buildResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR, exc.getMessage());
  }

  private ResponseEntity buildResponseEntity(HttpStatus status, String error) {

    return ResponseEntity.status(status)
        .contentType(MediaType.APPLICATION_JSON).body(ApiResponse.failure(status, error));
  }
}