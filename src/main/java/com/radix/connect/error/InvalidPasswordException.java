package com.radix.connect.error;

import com.radix.connect.core.util.Validator;
import org.springframework.security.core.AuthenticationException;

/**
 * @author kiran.chokhande
 */
public final class InvalidPasswordException extends AuthenticationException {

  private final String username;

  public InvalidPasswordException(String username, String error) {
    super(error);
    this.username = Validator.requireNonEmpty(username, "Username must be required");
  }

  public final String getUsername() {
    return username;
  }
}
