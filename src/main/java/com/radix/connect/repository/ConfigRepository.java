package com.radix.connect.repository;

import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.SenderAPI;
import com.radix.connect.core.model.SenderConfig;
import com.radix.connect.core.model.SenderMapping;
import com.radix.connect.core.model.SystemConfig;
import com.radix.connect.core.transaction.Transaction;
import com.radix.connect.core.transaction.TransactionManager;
import com.radix.connect.core.util.Crypto;
import com.radix.connect.core.util.Validator;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class ConfigRepository {

  private final TransactionManager transactionManager;

  public ConfigRepository(@Autowired final TransactionManager transactionManager) {
    this.transactionManager = Objects
        .requireNonNull(transactionManager, "TransactionManager instance can not NULL");
  }

  public final SystemConfig selectSystemConfig(final String key) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Config.SELECT_SYSTEM_CONFIG_BY_KEY",
          result -> result == null || !result.next() ? null : buildSystemConfig(result), key);
    }
  }

  public final List<SystemConfig> selectSystemConfigs() {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Config.SELECT_SYSTEM_CONFIG", result -> {
        List<SystemConfig> configs = new ArrayList<>();
        while (result != null && result.next()) {
          configs.add(buildSystemConfig(result));
        }
        return configs;
      });
    }
  }

  private SystemConfig buildSystemConfig(final ResultSet resultSet) throws Exception {

    String key = resultSet.getString("key");
    String desc = resultSet.getString("key_desc");
    String group = resultSet.getString("config_group");
    String value_type = resultSet.getString("value_type");
    boolean encrypted = resultSet.getBoolean("encrypted");

    String value = resultSet.getString("value");

    SystemConfig config = new SystemConfig(key, desc, group, value_type, encrypted);
    config.setLast_updated_by(resultSet.getString("last_updated_by"));
    config.setLast_updated_ts(resultSet.getTimestamp("last_updated_ts"));
    config.setValue(Validator.isEmpty(value) ? null : !encrypted ? value : Crypto.decrypt(value));

    return config;
  }

  public final boolean updateSystemConfig(String key, String value, String updUserId) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.update("Config.UPDATE_SYSTEM_CONFIG", statement -> {
        int i = 0;
        statement.setString(++i, value);
        statement.setString(++i, updUserId);
        statement.setString(++i, key);
      }) > 0;
    }
  }

  public final List<SenderConfig> selectSenderConfigs() {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Config.SELECT_SENDER_CONFIG", result -> {
        final List<SenderConfig> senderConfigs = new ArrayList<>();
        while (result != null && result.next()) {
          senderConfigs.add(buildSenderConfig(result));
        }
        return senderConfigs;
      });
    }
  }

  public final SenderConfig selectSenderConfig(final String senderId) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Config.SELECT_SENDER_CONFIG_BY_ID",
          res -> res == null || !res.next() ? null : buildSenderConfig(res), senderId);
    }
  }

  public final List<SenderConfig> selectSenderConfigs(final Modality modality) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Config.SELECT_SENDER_CONFIG_BY_MODALITY", result -> {
        final List<SenderConfig> senderConfigs = new ArrayList<>();
        while (result != null && result.next()) {
          senderConfigs.add(buildSenderConfig(result));
        }
        return senderConfigs;
      }, modality == null ? null : String.valueOf(modality));
    }
  }

  private SenderConfig buildSenderConfig(final ResultSet resultSet) throws Exception {

    String senderId = resultSet.getString("sender_id");
    Modality modality = Modality.forValue(resultSet.getString("modality"));
    SenderAPI senderAPI = SenderAPI.forValue(resultSet.getString("sender_api"));

    int messagingRate = resultSet.getInt("messaging_rate");

    SenderConfig senderConfig = new SenderConfig(senderId, modality, senderAPI);
    senderConfig.setMessaging_rate(messagingRate <= 0 ? 1 : messagingRate);
    senderConfig.setSender_desc(resultSet.getString("sender_desc"));
    senderConfig.setConfig_json(resultSet.getString("config_json"));
    senderConfig.setLast_updated_by(resultSet.getString("last_updated_by"));
    senderConfig.setLast_updated_ts(resultSet.getTimestamp("last_updated_ts"));
    return senderConfig;
  }

  public final boolean updateSenderConfig(final SenderConfig senderConfig) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.update("Config.UPDATE_SENDER_CONFIG", statement -> {
        int i = 0;
        statement.setString(++i, senderConfig.getSender_desc());
        statement.setNumber(++i, senderConfig.getMessaging_rate());
        statement.setString(++i, senderConfig.getConfig_json());
        statement.setString(++i, senderConfig.getLast_updated_by());
        statement.setString(++i, senderConfig.getSender_id());
      }) > 0;
    }
  }

  public final List<SenderMapping> selectSenderMappings() {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Config.SELECT_SENDER_MAPPING", result -> {
        final List<SenderMapping> senderMappings = new ArrayList<>();
        while (result != null && result.next()) {
          senderMappings.add(buildSenderMapping(result));
        }
        return senderMappings;
      });
    }
  }

  public final SenderMapping selectSenderMapping(String accountId, Modality modality) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Config.SELECT_SENDER_MAPPING_BY_ACCOUNT_ID_AND_MODALITY",
          r -> r == null || !r.next() ? null : buildSenderMapping(r), statement -> {
            statement.setString(1, accountId);
            statement.setString(2, modality);
          });
    }
  }

  private SenderMapping buildSenderMapping(final ResultSet resultSet) throws Exception {

    String accountId = resultSet.getString("account_id");
    String senderId = resultSet.getString("sender_id");
    Modality modality = Modality.forValue(resultSet.getString("modality"));

    SenderMapping senderMapping = new SenderMapping(accountId, modality, senderId);
    senderMapping.setLast_updated_by(resultSet.getString("last_updated_by"));
    senderMapping.setLast_updated_ts(resultSet.getTimestamp("last_updated_ts"));
    return senderMapping;
  }

  public final boolean updateSenderMapping(final SenderMapping senderMapping) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.update("SenderLog.UPDATE_SENDER_MAPPING", statement -> {

        statement.setString("account_id", senderMapping.getAccount_id());
        statement.setString("modality", senderMapping.getModality());
        statement.setString("sender_id", senderMapping.getSender_id());
        statement.setString("updated_by", senderMapping.getLast_updated_by());
      }) > 0;
    }
  }
}
