package com.radix.connect.repository;

import com.radix.connect.core.constant.Role;
import com.radix.connect.core.model.AppUser;
import com.radix.connect.core.transaction.Transaction;
import com.radix.connect.core.transaction.TransactionManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class UserRepository {

  private final TransactionManager transactionManager;

  public UserRepository(@Autowired final TransactionManager transactionManager) {
    this.transactionManager = Objects
        .requireNonNull(transactionManager, "TransactionManager instance can not NULL");
  }

  public final List<AppUser> selectUser() {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("User.SELECT_USER", result -> {
        List<AppUser> users = new ArrayList<>();
        while (result != null && result.next()) {
          users.add(buildUser(result));
        }
        return users;
      });
    }
  }

  public final AppUser selectUser(final String userId) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("User.SELECT_USER_BY_ID",
          result -> result == null || !result.next() ? null : buildUser(result), userId);
    }
  }

  private AppUser buildUser(final ResultSet resultSet) throws Exception {

    String userId = resultSet.getString("user_id");
    AppUser user = new AppUser(userId, resultSet.getString("password"));
    user.setName(resultSet.getString("name"));
    user.setPicture(resultSet.getString("image_url"));
    user.setActive(resultSet.getBoolean("is_active"));
    user.setLocked(resultSet.getBoolean("is_locked"));
    user.setRole(Role.forValue(resultSet.getString("role")));
    user.setLast_logged_in(resultSet.getTimestamp("last_logged_in"));
    user.setLast_updated_by(resultSet.getString("last_updated_by"));
    user.setLast_updated_ts(resultSet.getTimestamp("last_updated_ts"));
    user.setNum_failed_login_attempt(resultSet.getInt("num_failed_login_attempt"));

    return user;
  }

  public final boolean insertUser(final AppUser user) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.update("User.INSERT_USER", statement -> {
        byte i = 0;
        statement.setString(++i, user.getUser_id());
        statement.setString(++i, user.getPassword());
        statement.setString(++i, user.getName());
        statement.setString(++i, user.getPicture());
        statement.setString(++i, user.getRole());
        statement.setValue(++i, user.isActive());
        statement.setValue(++i, user.getLast_logged_in());
        statement.setString(++i, user.getLast_updated_by());
      }) > 0;
    }
  }

  public final boolean updateUser(final AppUser user) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.update("User.UPDATE_USER", statement -> {
        int i = 0, numFailedLoginAttempt = user.getNum_failed_login_attempt();
        statement.setString(++i, user.getName());
        statement.setString(++i, user.getPicture());
        statement.setString(++i, user.getRole());
        statement.setValue(++i, user.isActive());
        statement.setValue(++i, user.isLocked());
        statement.setNumber(++i, numFailedLoginAttempt <= 0 ? 0 : numFailedLoginAttempt);
        statement.setValue(++i, user.getLast_logged_in());
        statement.setString(++i, user.getLast_updated_by());
        statement.setString(++i, user.getUser_id());
      }) > 0;
    }
  }
}
