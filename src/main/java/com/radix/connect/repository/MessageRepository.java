package com.radix.connect.repository;

import com.radix.connect.core.constant.Direction;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.transaction.Transaction;
import com.radix.connect.core.transaction.TransactionManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class MessageRepository {

  private final TransactionManager transactionManager;

  public MessageRepository(@Autowired final TransactionManager transactionManager) {
    this.transactionManager = Objects
        .requireNonNull(transactionManager, "TransactionManager instance can not NULL");
  }

  public final Message selectMessage(final String accountId, final String messageId) {

    String[] parameters = {accountId, messageId};
    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Message.SELECT_MESSAGE", parameters,
          result -> result == null || !result.next() ? null : buildMessage(result));
    }
  }

  public final Message selectMessageByMsgSid(final Modality modality, final String msgSid) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Message.SELECT_MESSAGE_BY_MSG_SID", statement -> {
            statement.setString(1, modality);
            statement.setString(2, msgSid);
          },
          result -> result == null || !result.next() ? null : buildMessage(result));
    }
  }

  public final List<Message> selectMessage(final Status... statuses) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Message.SELECT_MESSAGE_BY_STATUS", result -> {
        List<Message> messages = new ArrayList<>();
        while (result != null && result.next()) {
          messages.add(buildMessage(result));
        }
        return messages;
      }, statement -> statement.setArray(1, statuses));
    }
  }

  private Message buildMessage(final ResultSet resultSet) throws Exception {

    String accountId = resultSet.getString("account_id");
    String messageId = resultSet.getString("message_id");
    Modality modality = Modality.forValue(resultSet.getString("modality"));
    Direction direction = Direction.forValue(resultSet.getString("direction"));

    Message message = new Message(accountId, modality, messageId, direction);
    message.setMsg_sid(resultSet.getString("msg_sid"));
    message.setError_desc(resultSet.getString("error_desc"));
    message.setLogged_ts(resultSet.getTimestamp("logged_ts"));
    message.setBefore_ts(resultSet.getTimestamp("before_ts"));
    message.setProcessed_ts(resultSet.getTimestamp("processed_ts"));
    message.setMsg_status(Status.forValue(resultSet.getString("msg_status")));

    return message;
  }

  public final boolean insertMessage(final Message message, int maxTimeoutInMinutes) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.update("Message.INSERT_MESSAGE", statement -> {
        byte i = 0;
        statement.setString(++i, message.getAccount_id());
        statement.setString(++i, message.getMessage_id());
        statement.setString(++i, message.getMsg_sid());
        statement.setString(++i, message.getError_desc());
        statement.setString(++i, message.getMsg_status());
        statement.setString(++i, message.getModality());
        statement.setString(++i, message.getDirection());
        statement.setNumber(++i, maxTimeoutInMinutes < 10 ? 120 : maxTimeoutInMinutes);
      }) > 0;
    }
  }

  public final boolean updateMessage(final Message message) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.update("Message.UPDATE_MESSAGE", statement -> {
        int i = 0;
        statement.setString(++i, message.getMsg_sid());
        statement.setString(++i, message.getMsg_status());
        statement.setString(++i, message.getError_desc());
        statement.setValue(++i, message.getProcessed_ts());
        statement.setString(++i, message.getAccount_id());
        statement.setString(++i, message.getMessage_id());
      }) > 0;
    }
  }
}
