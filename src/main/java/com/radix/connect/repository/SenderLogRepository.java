package com.radix.connect.repository;

import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.model.SenderLog;
import com.radix.connect.core.transaction.Transaction;
import com.radix.connect.core.transaction.TransactionManager;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class SenderLogRepository {

  private final TransactionManager transactionManager;

  public SenderLogRepository(@Autowired final TransactionManager transactionManager) {
    this.transactionManager = Objects
        .requireNonNull(transactionManager, "TransactionManager instance can not NULL");
  }

  public final SenderLog selectRecentSenderLogByPhone(Modality modality, String toPhoneNo) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("SenderLog.SELECT_RECENT_SENDER_LOG_BY_PHONE", result ->
              result == null || !result.next() ? null : buildSenderLog(result),
          String.valueOf(modality), toPhoneNo.replaceAll("\\D", ""));
    }
  }

  public final List<SenderLog> selectRecentSenderLogs() {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("SenderLog.SELECT_RECENT_SENDER_LOG", result -> {
        List<SenderLog> senderLogs = new ArrayList<>();
        while (result != null && result.next()) {
          senderLogs.add(buildSenderLog(result));
        }
        return senderLogs;
      });
    }
  }

  private SenderLog buildSenderLog(final ResultSet resultSet) throws Exception {

    String accountId = resultSet.getString("account_id");
    String messageId = resultSet.getString("message_id");
    String toPhone = resultSet.getString("to_phone");
    String fromPhone = resultSet.getString("from_phone");

    Timestamp timestamp = resultSet.getTimestamp("sent_timestamp");
    Modality modality = Modality.forValue(resultSet.getString("modality"));

    toPhone = toPhone == null ? null : toPhone.replaceAll("\\D", "");
    return new SenderLog(modality, toPhone, fromPhone, accountId, messageId, timestamp);
  }

  public final boolean updateRecentSenderLog(final SenderLog senderLog) {

    String toPhoneNo = senderLog.getTo_phone().replaceAll("\\D", "");
    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.update("SenderLog.UPDATE_RECENT_SENDER_LOG", statement -> {

        statement.setString("modality", senderLog.getModality());
        statement.setString("account_id", senderLog.getAccount_id());
        statement.setString("message_id", senderLog.getMessage_id());
        statement.setString("to_phone", toPhoneNo);
        statement.setString("from_phone", senderLog.getFrom_phone());
        statement.setValue("sent_timestamp", senderLog.getSent_timestamp());
      }) > 0;
    }
  }
}
