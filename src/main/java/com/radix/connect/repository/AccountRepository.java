package com.radix.connect.repository;

import com.radix.connect.core.transaction.Transaction;
import com.radix.connect.core.transaction.TransactionManager;
import com.radix.connect.core.model.Account;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class AccountRepository {

  private final TransactionManager transactionManager;

  public AccountRepository(@Autowired final TransactionManager transactionManager) {
    this.transactionManager = Objects
        .requireNonNull(transactionManager, "TransactionManager instance can not NULL");
  }

  public final Account selectAccount(final String accountId) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Account.SELECT_ACCOUNT_BY_ID",
          result -> result == null || !result.next() ? null : buildAccount(result), accountId);
    }
  }

  public final List<Account> selectAccounts() {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.select("Account.SELECT_ACCOUNT", result -> {
        List<Account> accounts = new ArrayList<>();
        while (result != null && result.next()) {
          accounts.add(buildAccount(result));
        }
        return accounts;
      });
    }
  }

  private Account buildAccount(final ResultSet resultSet) throws Exception {

    String accountId = resultSet.getString("account_id");
    String authToken = resultSet.getString("auth_token");

    Account account = new Account(accountId, authToken);
    account.setActive(resultSet.getBoolean("is_active"));
    account.setAccount_name(resultSet.getString("account_name"));
    account.setApp_base_url(resultSet.getString("app_base_url"));
    account.setLast_updated_by(resultSet.getString("last_updated_by"));
    account.setLast_updated_ts(resultSet.getTimestamp("last_updated_ts"));

    return account;
  }

  public final boolean insertAccount(final Account account) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.update("Account.INSERT_ACCOUNT", statement -> {
        byte i = 0;
        statement.setString(++i, account.getAccount_id());
        statement.setString(++i, account.getAuth_token());
        statement.setString(++i, account.getAccount_name());
        statement.setString(++i, account.getApp_base_url());
        statement.setString(++i, account.getLast_updated_by());
      }) > 0;
    }
  }

  public final boolean updateAccount(final Account account) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.update("Account.UPDATE_ACCOUNT", statement -> {
        int i = 0;
        statement.setString(++i, account.getAccount_name());
        statement.setString(++i, account.getApp_base_url());
        statement.setValue(++i, account.isActive());
        statement.setString(++i, account.getLast_updated_by());
        statement.setString(++i, account.getAccount_id());
      }) > 0;
    }
  }

  public final boolean updateAuthToken(String accountId, String authToken, String updUserId) {

    try (Transaction transaction = transactionManager.beginTransaction()) {
      return transaction.update("Account.UPDATE_AUTH_TOKEN", statement -> {
        int i = 0;
        statement.setString(++i, authToken);
        statement.setString(++i, updUserId);
        statement.setString(++i, accountId);
      }) > 0;
    }
  }
}
