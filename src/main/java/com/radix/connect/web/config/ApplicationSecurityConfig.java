package com.radix.connect.web.config;

import com.radix.connect.web.auth.filter.ApisAuthenticationFilter;
import com.radix.connect.web.auth.filter.UserAuthenticationFilter;
import com.radix.connect.web.auth.handler.UserAuthHandler;
import javax.ws.rs.ForbiddenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.ErrorPageRegistrar;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.web.server.ServerErrorException;

/**
 * @author kiran.chokhande
 */
@Configuration
@EnableWebSecurity
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private UserAuthHandler userAuthenticationHandler;
  @Autowired
  private UserAuthenticationFilter userAuthenticationFilter;
  @Autowired
  private ApisAuthenticationFilter apisAuthenticationFilter;

  @Bean
  @Primary
  public static ErrorPageRegistrar configureApplicationErrorPageRegistrar() {

    return appsErrorPageRegistrar -> appsErrorPageRegistrar.addErrorPages
        (
            new ErrorPage(RequestRejectedException.class, "/"),
            new ErrorPage(AuthenticationException.class, "/"),

            new ErrorPage(ForbiddenException.class, "/error/403"),
            new ErrorPage(ServerErrorException.class, "/error/500"),
            new ErrorPage(AccessDeniedException.class, "/error/403")
        );
  }

  @Override
  public final void configure(final WebSecurity webSecurity) {

    String[] openEndpointsToAvoidAuthentication = {
        "/favicon.ico", "/css/**", "/plugin/**", "/fonts/**", "/image/**",
        "/error/**",
        "/api/message/*/receive", "/api/message/*/status"
    };

    apisAuthenticationFilter.permitAll(openEndpointsToAvoidAuthentication);

    webSecurity.ignoring().antMatchers(openEndpointsToAvoidAuthentication);
    webSecurity.ignoring().antMatchers(HttpMethod.GET, "/login");
  }

  @Override
  public final void configure(final HttpSecurity httpSecurity) throws Exception {

    httpSecurity.authorizeRequests().antMatchers("/api/**").hasAnyRole("API");
    httpSecurity.authorizeRequests().antMatchers("/app/**").hasAnyRole("ADMIN", "USER");

    httpSecurity.csrf().disable().anonymous().disable()
        .sessionManagement().invalidSessionUrl("/logout").maximumSessions(1).expiredUrl("/logout");

    httpSecurity.authorizeRequests().anyRequest().authenticated().and()
        .formLogin().loginPage("/login").and().oauth2Login().loginPage("/login")
        .successHandler(userAuthenticationHandler.getOAuth2AuthenticationSuccessHandler())
        .and().logout().logoutSuccessHandler(userAuthenticationHandler.getLogoutSuccessHandler());

    httpSecurity.addFilterAt(userAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
    httpSecurity.addFilterAt(apisAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
  }
}