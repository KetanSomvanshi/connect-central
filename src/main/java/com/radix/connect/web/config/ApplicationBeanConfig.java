package com.radix.connect.web.config;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.radix.connect.core.Builder;
import com.radix.connect.core.Provider;
import com.radix.connect.core.constant.Constant.SystemEnvVar;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.TxnType;
import com.radix.connect.core.message.queue.Queue;
import com.radix.connect.core.message.queue.QueueManager;
import com.radix.connect.core.message.sender.SenderManager;
import com.radix.connect.core.message.sender.SenderManager.SenderFactory;
import com.radix.connect.core.model.Account;
import com.radix.connect.core.model.AppUser;
import com.radix.connect.core.model.QueueConfig;
import com.radix.connect.core.transaction.TransactionManager;
import com.radix.connect.core.transaction.TransactionManager.TransactionFactory;
import com.radix.connect.core.util.Validator;
import com.radix.connect.core.util.WorkManager;
import com.radix.connect.error.ConnectException;
import com.radix.connect.web.auth.filter.ApisAuthenticationFilter;
import com.radix.connect.web.auth.filter.UserAuthenticationFilter;
import com.radix.connect.web.auth.handler.ApisAuthHandler;
import com.radix.connect.web.auth.handler.UserAuthHandler;
import com.radix.connect.web.filter.HttpRequestWrapperFilter;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import javax.annotation.PreDestroy;
import javax.servlet.Filter;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author kiran.chokhande
 */
@Configuration
@EnableScheduling
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApplicationBeanConfig {

  private static final String DB_URL_EXTRACTOR = "([\\w\\W:]+://)([\\w\\W]+):+([\\w\\W]+)@([\\w\\W]+:\\d+/[\\w\\W]+)";

  public ApplicationBeanConfig(@Autowired final ObjectMapper objectMapper) {

    objectMapper.setSerializationInclusion(Include.NON_EMPTY);
    objectMapper.setTimeZone(TimeZone.getTimeZone("America/New_York"));
    objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a"));
  }

  @PreDestroy
  public static void destroyConfiguration() {
    WorkManager.shutdownAllWorkSpaces();
  }

  @Scheduled(initialDelay = 99999, fixedDelay = 300000)
  public static void watchingInfiniteWorkers() {
    WorkManager.watchInfiniteWorks();
  }

  @Bean
  @Primary
  public DataSource getDataSource() {

    String sysEnvVarName = SystemEnvVar.DATABASE_URL;
    String DATABASE_URL_ENV = System.getenv(sysEnvVarName);
    if (DATABASE_URL_ENV == null || !DATABASE_URL_ENV.matches(DB_URL_EXTRACTOR)) {
      throw new ConnectException(
          String.format("Invalid system env variable, %s: %s", sysEnvVarName, DATABASE_URL_ENV));
    }

    String DB_USERNAME = DATABASE_URL_ENV.replaceAll(DB_URL_EXTRACTOR, "$2");
    String DB_PASSWORD = DATABASE_URL_ENV.replaceAll(DB_URL_EXTRACTOR, "$3");
    String DATABASE_URL = DATABASE_URL_ENV.replaceAll(DB_URL_EXTRACTOR, "$1$4");

    BasicDataSource basicDataSource = new BasicDataSource();
    basicDataSource.setUrl(DATABASE_URL);
    basicDataSource.setUsername(DB_USERNAME);
    basicDataSource.setPassword(DB_PASSWORD);
    basicDataSource.setInitialSize(50);
    basicDataSource.setMinIdle(10);
    basicDataSource.setMaxIdle(50);
    basicDataSource.setMaxActive(1000);
    basicDataSource.setMaxWait(100000);
    basicDataSource.setValidationQuery("SELECT 1;");
    basicDataSource.setValidationQueryTimeout(5000);

    return basicDataSource;
  }

  @Bean
  @Primary
  public FilterRegistrationBean filterRegistrationBean() {

    FilterRegistrationBean<Filter> filterRegistrationBean = new FilterRegistrationBean<>();
    filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
    filterRegistrationBean.addUrlPatterns("/app/*", "/api/*");
    filterRegistrationBean.setFilter(new HttpRequestWrapperFilter());
    return filterRegistrationBean;
  }

  @Bean
  @Primary
  @Autowired
  public QueueManager getQueueManager(final Builder<QueueConfig, ? extends Queue> queueBuilder) {
    return new QueueManager(queueBuilder);
  }

  @Bean
  @Primary
  @Autowired
  public SenderManager senderManager(Builder<Modality, ? extends SenderFactory> factoryBuilder) {
    return new SenderManager(factoryBuilder);
  }

  @Bean
  @Primary
  @Autowired
  public TransactionManager txnManager(Builder<TxnType, ? extends TransactionFactory> fBuilder) {

    String sysEnvVarVal = System.getenv(SystemEnvVar.DATABASE_TXN_TYPE);
    TxnType txnType = Validator.isEmpty(sysEnvVarVal) ? null : TxnType.forValue(sysEnvVarVal);

    TransactionManager transactionManager = new TransactionManager(fBuilder);
    transactionManager.configure(txnType != null ? txnType : TxnType.JDBC_TXN);
    return transactionManager;
  }

  @Bean
  @Primary
  @Autowired
  public ApisAuthenticationFilter apisAuth(ApisAuthHandler h, Provider<String, Account> provider) {
    return new ApisAuthenticationFilter(h, provider);
  }

  @Bean
  @Primary
  @Autowired
  public UserAuthenticationFilter userAuth(UserAuthHandler h, Provider<String, AppUser> provider) {
    return new UserAuthenticationFilter(h, provider);
  }
}
