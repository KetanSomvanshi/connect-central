package com.radix.connect.web.controller;

import com.radix.connect.core.annotation.LogRequest;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.model.AppUser;
import com.radix.connect.error.ConnectException;
import com.radix.connect.service.config.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kiran.chokhande
 */
@RestController
@RequestMapping("/app/user")
public class UserController {

  private final UserService service;

  public UserController(@Autowired final UserService userService) {
    this.service = userService;
  }

  @LogRequest("Get All Users")
  @GetMapping
  public List<AppUser> getUsers() throws Exception {

    try {

      return service.getUsers();
    } catch (Exception exception) {
      RxLogger.logException("Get All User Details", exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to load user details, please check application logs for more details");
    }
  }

  @LogRequest("Get User By ID")
  @GetMapping("/{userId}")
  public AppUser getUser(@PathVariable final String userId) throws Exception {

    try {

      return service.getUserById(userId);
    } catch (Exception exception) {
      RxLogger.logException("Get User Detail By ID:" + userId, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to load user detail, please check application logs for more details");
    }
  }
}
