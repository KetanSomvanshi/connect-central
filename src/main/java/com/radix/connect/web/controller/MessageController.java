package com.radix.connect.web.controller;

import com.google.gson.JsonObject;
import com.radix.connect.core.annotation.LogRequest;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.constant.Status;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.model.Message;
import com.radix.connect.core.model.response.MsgStatus;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;
import com.radix.connect.service.message.MessageService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kiran.chokhande
 */
@RestController
@RequestMapping({"/app", "/api"})
public class MessageController {

  private final MessageService service;

  public MessageController(@Autowired final MessageService messageService) {
    this.service = messageService;
  }

  @LogRequest("Get Message")
  @GetMapping("/{accountId}/message/{messageId}")
  public Message getMessage(@PathVariable String accountId, @PathVariable String messageId)
      throws Exception {

    try {

      return service.getMessageById(accountId, messageId);
    } catch (Exception exception) {
      RxLogger.logException("Get Message, Message ID:" + messageId, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to get message, please check application logs for more details");
    }
  }

  @LogRequest("Get Message Status")
  @GetMapping("/{accountId}/message/{messageId}/status")
  public MsgStatus getMessageStatus(@PathVariable String accountId, @PathVariable String messageId)
      throws Exception {

    try {

      return service.getMessageStatus(accountId, messageId);
    } catch (Exception exception) {
      RxLogger.logException("Get Message Status, Message ID:" + messageId, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to get message status, please check application logs for more details");
    }
  }

  @LogRequest("Process Outbound Message")
  @PostMapping("/{accountId}/message/{modalityId}/send")
  public MsgStatus outboundMessage(@PathVariable String accountId, @PathVariable String modalityId,
      HttpServletRequest request) throws Exception {

    try {

      String timeoutMin = request.getParameter("max_timeout_in_minute");
      timeoutMin = timeoutMin == null ? "120" : timeoutMin.replaceAll("\\D", "");
      int maxTimeoutInMinutes = Validator.isEmpty(timeoutMin) ? 120 : Integer.parseInt(timeoutMin);

      boolean isReply = Boolean.parseBoolean(request.getParameter("is_reply_message"));
      boolean sendNow = Boolean.parseBoolean(request.getParameter("send_immediately"));

      Modality modality = Modality.forValue(modalityId);
      JsonUtil message = new JsonUtil(request.getParameter("message_body"));
      if (!Validator.isEmpty(request.getParameter("contact"))) {
        message.add("contact", request.getParameter("contact"));
      }
      return service.processOutboundMessage(accountId, modality, message.getAsJsonO(),
          request.getParameter("timezone"), maxTimeoutInMinutes, sendNow, isReply);
    } catch (Exception exception) {
      RxLogger.logException("Process Outbound Message, Modality:" + modalityId, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to process outbound message, please check application logs for more details");
    }
  }

  @LogRequest("Process Inbound Message")
  @GetMapping("/message/{modalityId}/receive")
  public ResponseEntity inboundMessage(@PathVariable String modalityId, HttpServletRequest request)
      throws Exception {

    try {

      if (!"TEXT".equalsIgnoreCase(modalityId)) {
        throw new ConnectException("Can't process inbound message for modality:" + modalityId);
      }

      JsonObject message = new JsonObject();
      message.addProperty("contact", request.getParameter("From"));
      message.addProperty("message", request.getParameter("Body"));
      service.processInboundMessage(request.getParameter("SmsSid"), Modality.TEXT, message);

      return new ResponseEntity(HttpStatus.NO_CONTENT);
    } catch (Exception exception) {
      RxLogger.logException("Process Inbound Message, MessageType:" + modalityId, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to process inbound message, please check application logs for more details");
    }
  }

  @LogRequest("Message Status Callback")
  @PostMapping("/message/{modalityId}/status")
  public void statusCallback(@PathVariable String modalityId, HttpServletRequest request)
      throws Exception {

    try {

      String msgSid = null, msgStatus = null, errorDesc = null;
      if ("TEXT".equalsIgnoreCase(modalityId)) {
        msgSid = request.getParameter("SmsSid");
        msgStatus = request.getParameter("SmsStatus");
      } else if ("VOICE".equalsIgnoreCase(modalityId)) {
        msgSid = request.getParameter("CallSid");
        msgStatus = request.getParameter("CallStatus");
      } else if ("EMAIL".equalsIgnoreCase(modalityId)) {

        String eventType = request.getParameter("eventType");
        msgStatus = "REJECT".equalsIgnoreCase(eventType) ? "REJECTED"
            : "DELIVERY".equalsIgnoreCase(eventType) ? "DELIVERED" : eventType;
        msgSid = new JsonUtil(request.getParameter("mail")).getValue("messageId");
      }

      Modality modality = Modality.forValue(modalityId);
      if (!Validator.isEmpty(msgSid) && !Validator.isEmpty(msgStatus)) {
        service.updateMsgStatus(modality, msgSid, Status.forValue(msgStatus), errorDesc);
      }
    } catch (Exception exception) {
      RxLogger.logException("Message Status Callback, Modality:" + modalityId, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to update message status, please check application logs for more details");
    }
  }
}