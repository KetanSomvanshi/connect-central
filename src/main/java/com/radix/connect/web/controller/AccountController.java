package com.radix.connect.web.controller;

import com.radix.connect.core.annotation.LogRequest;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.model.Account;
import com.radix.connect.core.model.AuthToken;
import com.radix.connect.error.ConnectException;
import com.radix.connect.service.config.AccountService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kiran.chokhande
 */
@RestController
@RequestMapping("/app/account")
public class AccountController {

  private final AccountService service;

  public AccountController(@Autowired final AccountService accountService) {
    this.service = accountService;
  }

  @LogRequest("Get All Accounts")
  @GetMapping
  public List<Account> getAccounts() throws Exception {

    try {

      return service.getAccounts();
    } catch (Exception exception) {
      RxLogger.logException("Get All Account Details", exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to load account details, please check application logs for more details");
    }
  }

  @LogRequest("Get Account By ID")
  @GetMapping("/{accountId}")
  public Account getAccount(@PathVariable final String accountId) throws Exception {

    try {

      return service.getAccountById(accountId);
    } catch (Exception exception) {
      RxLogger.logException("Get Account Detail By ID:" + accountId, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to load account detail, please check application logs for more details");
    }
  }

  @LogRequest("Create Account")
  @PostMapping
  public Account createAccount(HttpServletRequest request) throws Exception {

    try {

      String updUserId = request.getRemoteUser();
      String accountName = request.getParameter("account_name");
      String appBaseUrl = request.getParameter("app_base_url");

      return service.createAccount(accountName, appBaseUrl, updUserId);
    } catch (Exception exception) {
      RxLogger.logException("Create New Account", exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to create account, please check application logs for more details");
    }
  }

  @LogRequest("Update Account")
  @PutMapping("/{accountId}")
  public Account updateAccount(@PathVariable final String accountId, HttpServletRequest request)
      throws Exception {

    try {

      String updUserId = request.getRemoteUser();
      String accountName = request.getParameter("account_name");
      String appBaseUrl = request.getParameter("app_base_url");
      boolean isActive = Boolean.parseBoolean(request.getParameter("active"));

      return service.updateAccount(accountId, accountName, appBaseUrl, isActive, updUserId);
    } catch (Exception exception) {
      RxLogger.logException("Update Account, ID: " + accountId, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to update account, please check application logs for more details");
    }
  }

  @LogRequest("Get Authorization Token")
  @GetMapping("/{accountId}/token")
  public AuthToken getAuthToken(@PathVariable final String accountId) throws Exception {

    try {

      return service.getAuthToken(accountId);
    } catch (Exception exception) {
      RxLogger.logException("Refresh Authorization Token, ID: " + accountId, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to refresh authorization token, please check application logs for more details");
    }
  }

  @LogRequest("Refresh Authorization Token")
  @PostMapping("/{accountId}/token/refresh")
  public AuthToken refreshToken(@PathVariable final String accountId, HttpServletRequest request)
      throws Exception {

    try {

      return service.refreshAuthToken(accountId, request.getRemoteUser());
    } catch (Exception exception) {
      RxLogger.logException("Refresh Authorization Token, ID: " + accountId, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to refresh authorization token, please check application logs for more details");
    }
  }
}
