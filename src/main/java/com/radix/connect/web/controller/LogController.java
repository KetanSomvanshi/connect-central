package com.radix.connect.web.controller;

import com.radix.connect.core.annotation.LogRequest;
import com.radix.connect.core.constant.Modality;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.model.SenderLog;
import com.radix.connect.error.ConnectException;
import com.radix.connect.service.message.SenderLogService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kiran.chokhande
 */
@RestController
@RequestMapping("/app/log")
public class LogController {

  private final SenderLogService senderLogService;

  public LogController(@Autowired final SenderLogService senderLogService) {
    this.senderLogService = senderLogService;
  }

  @LogRequest("Get Sender Logs By Phrase")
  @GetMapping("/sender/{mode}")
  public List<SenderLog> getSenderLogs(@PathVariable String mode, @RequestParam("q") String phrase)
      throws Exception {

    try {

      return senderLogService.getRecentSenderLogsByPhrase(Modality.forValue(mode), phrase);
    } catch (Exception exception) {
      RxLogger.logException("Get Sender Logs By Phrase: " + phrase, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to load sender logs, please check application logs for more details");
    }
  }
}