package com.radix.connect.web.controller;

import com.radix.connect.core.annotation.LogRequest;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.model.SenderConfig;
import com.radix.connect.core.model.SystemConfig;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.ConnectException;
import com.radix.connect.service.config.ConfigService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kiran.chokhande
 */
@RestController
@RequestMapping("/app/config")
public class ConfigController {

  private final ConfigService service;

  public ConfigController(@Autowired final ConfigService configService) {
    this.service = configService;
  }

  @LogRequest("Get System Configs")
  @GetMapping("/system")
  public List<SystemConfig> getSystemConfigs(HttpServletRequest request) throws Exception {

    try {

      return service.getSystemConfigs(request.getParameterValues("key"));
    } catch (Exception exception) {
      RxLogger.logException("Get System Configs", exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to load system configs, please check application logs for more details");
    }
  }

  @LogRequest("Update System Config")
  @PutMapping("/system/{key}")
  public SystemConfig updateSystemConfig(@PathVariable final String key, HttpServletRequest request)
      throws Exception {

    try {

      String updUserId = request.getRemoteUser();
      String value = request.getParameter("value");

      return service.updateSystemConfig(key, value, updUserId);
    } catch (Exception exception) {
      RxLogger.logException("Update System Config Key: " + key, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to update system config, please check application logs for more details");
    }
  }

  @LogRequest("Get All Sender Configs")
  @GetMapping("/sender")
  public List<SenderConfig> getSenderConfigs() throws Exception {

    try {

      return service.getSenderConfigs();
    } catch (Exception exception) {
      RxLogger.logException("Get All Sender Configs", exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to load sender configs, please check application logs for more details");
    }
  }

  @LogRequest("Get Sender Configs By ID")
  @GetMapping("/sender/{senderId}")
  public SenderConfig getSenderConfig(@PathVariable final String senderId) throws Exception {

    try {

      return service.getSenderConfigById(senderId);
    } catch (Exception exception) {
      RxLogger.logException("Get Sender Configs By ID: " + senderId, exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to load sender config, please check application logs for more details");
    }
  }

  @LogRequest("Update Sender Config")
  @PutMapping("/sender/{senderId}")
  public SenderConfig updateSenderConfig(@PathVariable final String senderId,
      final HttpServletRequest request) throws Exception {

    try {

      String userId = request.getRemoteUser();
      String senderDesc = request.getParameter("sender_desc");
      String configJson = request.getParameter("config_json");
      String msgRate = request.getParameter("messaging_rate");
      msgRate = msgRate == null ? null : msgRate.replaceAll("\\D", "");

      int messagingRate = Validator.isEmpty(msgRate) ? 1 : Integer.parseInt(msgRate);

      return service.updateSenderConfig(senderId, senderDesc, messagingRate, configJson, userId);
    } catch (Exception exception) {
      RxLogger.logException("Update Sender Config", exception);

      throw exception instanceof ConnectException ? exception : new ConnectException(
          "Failed to update sender config, please check application logs for more details");
    }
  }
}