package com.radix.connect.web.controller;

import com.radix.connect.core.model.AppUser;
import com.radix.connect.core.util.Validator;
import com.radix.connect.service.config.UserService;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author kiran.chokhande
 */
@Controller
public final class HomeController {

  private final UserService userService;

  public HomeController(@Autowired final UserService userService) {
    this.userService = userService;
  }

  @GetMapping({"/", "/home", "/login"})
  public final String home(final Principal principal, final Model model) {

    AppUser user = null;
    String username = principal == null ? null : principal.getName();
    if (model != null && !Validator.isEmpty(username)) {
      System.out.println("Logged in remote User ID: " + username);

      if ((user = userService.getUserById(username)) != null) {
        model.addAttribute("user", user);
      }
    }

    return Validator.isEmpty(username) || user == null ? "login" : "home";
  }

  @GetMapping({"/error/{http_status}"})
  public String error(@PathVariable int http_status) {
    return "error/" + http_status;
  }
}