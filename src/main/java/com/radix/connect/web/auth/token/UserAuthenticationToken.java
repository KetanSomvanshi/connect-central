package com.radix.connect.web.auth.token;

import java.util.Collections;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author kiran.chokhande
 */
public final class UserAuthenticationToken extends AbstractAuthenticationToken {

  private final String username;
  private final String password;

  public UserAuthenticationToken(String username, String password) {
    this(username, password, null);
  }

  private UserAuthenticationToken(String username, String password, GrantedAuthority authority) {
    super(authority == null ? null : Collections.singletonList(authority));
    super.setAuthenticated(false);

    this.username = username;
    this.password = password;
  }

  public static UserAuthenticationToken successToken(String username, GrantedAuthority authority) {
    UserAuthenticationToken token = new UserAuthenticationToken(username, null, authority);
    token.setAuthenticated(true);
    return token;
  }

  public final String getUsername() {
    return username;
  }

  public final String getPassword() {
    return password;
  }

  @Override
  public final String getName() {
    return getUsername();
  }

  @Override
  public final Object getPrincipal() {
    return getUsername();
  }

  @Override
  public final Object getCredentials() {
    return getPassword();
  }

  @Override
  public final String toString() {
    return "Authentication [ Username: " + username + "; "
        + "Authenticated: " + super.isAuthenticated() + "; "
        + "Granted Authorities: " + getAuthorities() + " ]";
  }
}
