package com.radix.connect.web.auth.provider;

import com.radix.connect.core.Provider;
import com.radix.connect.core.model.Account;
import com.radix.connect.core.util.Validator;
import com.radix.connect.web.auth.token.ApisAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;

/**
 * @author kiran.chokhande
 */
public final class ApisAuthenticationProvider implements AuthenticationProvider {

  private final GrantedAuthority ROLE_API = new SimpleGrantedAuthority("ROLE_API");

  private final Provider<String, Account> accountProvider;

  public ApisAuthenticationProvider(final Provider<String, Account> accountProvider) {
    this.accountProvider = Validator
        .requireNonNull(accountProvider, "Account detail provider must be required");
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {

    Assert.isInstanceOf(ApisAuthenticationToken.class, authentication,
        () -> "ApisAuthenticationProvider supports only ApisAuthenticationToken");

    ApisAuthenticationToken apiAuthentication = (ApisAuthenticationToken) authentication;

    String accountId = apiAuthentication.getAccount_id();
    String authToken = apiAuthentication.getAuth_token();
    Account account = accountProvider.provide(accountId);
    if (account == null) {
      throw new UsernameNotFoundException("Account ID " + accountId + " not found");
    }
    if (!account.isActive()) {
      throw new DisabledException("Inactive Account, activate account to access connect API");
    }
    if (!authToken.equals(account.getAuth_token())) {
      throw new BadCredentialsException("Invalid Account ID or Authorization token");
    }

    return ApisAuthenticationToken.successToken(accountId, ROLE_API);
  }

  @Override
  public final boolean supports(Class<?> authentication) {
    return ApisAuthenticationToken.class.isAssignableFrom(authentication);
  }
}
