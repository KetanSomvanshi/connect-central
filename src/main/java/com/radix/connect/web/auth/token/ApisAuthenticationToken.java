package com.radix.connect.web.auth.token;

import java.util.Collections;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author kiran.chokhande
 */
public final class ApisAuthenticationToken extends AbstractAuthenticationToken {

  private final String account_id;
  private final String auth_token;

  public ApisAuthenticationToken(String accountId, String authToken) {
    this(accountId, authToken, null);
  }

  private ApisAuthenticationToken(String accountId, String authToken, GrantedAuthority authority) {
    super(authority == null ? null : Collections.singletonList(authority));
    super.setAuthenticated(false);

    this.account_id = accountId;
    this.auth_token = authToken;
  }

  public static ApisAuthenticationToken successToken(String accountId, GrantedAuthority authority) {
    ApisAuthenticationToken token = new ApisAuthenticationToken(accountId, null, authority);
    token.setAuthenticated(true);
    return token;
  }

  public final String getAccount_id() {
    return account_id;
  }

  public final String getAuth_token() {
    return auth_token;
  }

  @Override
  public final String getName() {
    return getAccount_id();
  }

  @Override
  public final Object getPrincipal() {
    return getAccount_id();
  }

  @Override
  public final Object getCredentials() {
    return getAuth_token();
  }

  @Override
  public final String toString() {
    return "Authentication [ Account ID: " + account_id + "; "
        + "Authenticated: " + super.isAuthenticated() + "; "
        + "Granted Authorities: " + getAuthorities() + " ]";
  }
}
