package com.radix.connect.web.auth.filter;

import com.radix.connect.core.Provider;
import com.radix.connect.core.model.AppUser;
import com.radix.connect.core.util.Validator;
import com.radix.connect.web.auth.handler.UserAuthHandler;
import com.radix.connect.web.auth.provider.UserAuthenticationProvider;
import com.radix.connect.web.auth.token.UserAuthenticationToken;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * @author kiran.chokhande
 */
public final class UserAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

  private static final String USERNAME_NOT_FOUND = "Username must be required";
  private static final String PASSWORD_NOT_FOUND = "Password must be required";

  public UserAuthenticationFilter(UserAuthHandler authHandler, Provider<String, AppUser> provider) {

    super(new AntPathRequestMatcher("/login", "POST"));
    super.setAuthenticationManager(new UserAuthenticationProvider(provider)::authenticate);
    super.setAuthenticationSuccessHandler(authHandler.getAuthenticationSuccessHandler());
    super.setAuthenticationFailureHandler(authHandler.getAuthenticationFailureHandler());
  }

  @Override
  public final Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
      throws AuthenticationException {

    String username = obtainUsername(req).trim();
    String password = obtainPassword(req).trim();

    if (Validator.isEmpty(username)) {
      throw new AuthenticationCredentialsNotFoundException(USERNAME_NOT_FOUND);
    }
    if (Validator.isEmpty(password)) {
      throw new AuthenticationCredentialsNotFoundException(PASSWORD_NOT_FOUND);
    }

    UserAuthenticationToken authRequest = new UserAuthenticationToken(username, password);
    authRequest.setDetails(super.authenticationDetailsSource.buildDetails(req));

    return super.getAuthenticationManager().authenticate(authRequest);
  }

  private String obtainUsername(HttpServletRequest request) {
    String username = request.getParameter("username");
    return username == null ? "" : username;
  }

  private String obtainPassword(HttpServletRequest request) {
    String password = request.getParameter("password");
    return password == null ? "" : password;
  }
}
