package com.radix.connect.web.auth.provider;

import com.radix.connect.core.Provider;
import com.radix.connect.core.constant.Role;
import com.radix.connect.core.model.AppUser;
import com.radix.connect.core.util.BCrypt;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.InvalidPasswordException;
import com.radix.connect.web.auth.token.UserAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;

/**
 * @author kiran.chokhande
 */
public final class UserAuthenticationProvider implements AuthenticationProvider {

  private final Provider<String, AppUser> userProvider;

  public UserAuthenticationProvider(final Provider<String, AppUser> userProvider) {
    this.userProvider = Validator
        .requireNonNull(userProvider, "User detail provider must be required");
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {

    Assert.isInstanceOf(UserAuthenticationToken.class, authentication,
        () -> "UserAuthenticationProvider supports only UserAuthenticationToken");

    UserAuthenticationToken userAuthentication = (UserAuthenticationToken) authentication;

    String username = userAuthentication.getUsername();
    String password = userAuthentication.getPassword();
    AppUser appUser = userProvider.provide(username);
    if (appUser == null) {
      throw new UsernameNotFoundException("Username " + username + " not found");
    }
    if (!appUser.isActive()) {
      throw new DisabledException("User account is inactive");
    }
    if (appUser.isLocked()) {
      throw new LockedException("User account has been locked");
    }
    if (!BCrypt.isMatch(password, appUser.getPassword())) {
      throw new InvalidPasswordException(username, "Invalid username or password");
    }

    Role authorizationRole = Validator.coalesce(appUser.getRole(), Role.ROLE_USER);
    return UserAuthenticationToken.successToken(username, authorizationRole.getAuthority());
  }

  @Override
  public final boolean supports(Class<?> authentication) {
    return UserAuthenticationToken.class.isAssignableFrom(authentication);
  }
}
