package com.radix.connect.web.auth.handler;

import com.google.gson.JsonElement;
import com.radix.connect.core.constant.Constant;
import com.radix.connect.core.constant.Role;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.model.AppUser;
import com.radix.connect.core.util.BCrypt;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import com.radix.connect.error.InvalidPasswordException;
import com.radix.connect.service.config.UserService;
import com.radix.connect.web.auth.token.UserAuthenticationToken;
import java.sql.Timestamp;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class UserAuthHandler {

  private static final RedirectStrategy REDIRECT_STRATEGY = new DefaultRedirectStrategy();

  private static final String LOGGED_IN_SUCCESS_MSG = "User logged in successfully, User ID: ";
  private static final String LOGGED_OUT_SUCCESS_MSG = "User logged out successfully, User ID: ";

  private final UserService userService;

  public UserAuthHandler(@Autowired final UserService userService) {
    this.userService = userService;
  }

  public final LogoutSuccessHandler getLogoutSuccessHandler() {
    return (request, response, authentication) -> {

      String userId = authentication == null ? null : authentication.getName();
      RxLogger.logMessage("User Logged Out", LOGGED_OUT_SUCCESS_MSG + userId);

      request.getSession().removeAttribute(Constant.HTTP_SESSION_KEY_TO_HOLD_USER_ID);

      REDIRECT_STRATEGY.sendRedirect(request, response, "/login");
    };
  }

  public final AuthenticationFailureHandler getAuthenticationFailureHandler() {
    return (request, response, authentication) -> {

      String error = authentication.getMessage();
      error = !Validator.isEmpty(error) ? error : "Invalid username or password";

      if (authentication instanceof InvalidPasswordException) {

        AppUser user = userService
            .getUserById(((InvalidPasswordException) authentication).getUsername());
        int numFailedLogin = 1 + (user == null ? 0 : user.getNum_failed_login_attempt());

        if (user != null) {
          user.setLocked(numFailedLogin >= 5);
          user.setLast_updated_by(user.getUser_id());
          user.setNum_failed_login_attempt(numFailedLogin);
          userService.updateUser(user);
        }

        if (numFailedLogin >= 5) {
          error = "User account has been locked";
        } else if (numFailedLogin <= 2) {
          error = "Invalid username or password";
        } else if (numFailedLogin == 3) {
          error = "Invalid credential, 2 attempt remaining";
        } else {
          error = "Last attempt, user account will be lock";
        }
      }

      RxLogger.logError("User Login Failed", error);
      request.getSession().removeAttribute(Constant.HTTP_SESSION_KEY_TO_HOLD_USER_ID);

      REDIRECT_STRATEGY.sendRedirect(request, response, "/login?error=" + error);
    };
  }

  public final AuthenticationSuccessHandler getAuthenticationSuccessHandler() {
    return (request, response, authentication) -> {

      String userId = authentication == null ? null : authentication.getName();
      RxLogger.logMessage("User Logged In", LOGGED_IN_SUCCESS_MSG + userId);

      request.getSession().setAttribute(Constant.HTTP_SESSION_KEY_TO_HOLD_USER_ID, userId);

      AppUser user = userService.getUserById(userId);
      if (user != null) {
        user.setActive(true);
        user.setLocked(false);
        user.setLast_updated_by(userId);
        user.setNum_failed_login_attempt(0);
        user.setLast_logged_in(new Timestamp(System.currentTimeMillis()));
        userService.updateUser(user);
      }

      REDIRECT_STRATEGY.sendRedirect(request, response, "/");
    };
  }

  public final AuthenticationSuccessHandler getOAuth2AuthenticationSuccessHandler() {
    return (request, response, authentication) -> {

      String userId = authentication == null ? null : authentication.getName();
      RxLogger.logMessage("User Logged In", LOGGED_IN_SUCCESS_MSG + userId);

      request.getSession().setAttribute(Constant.HTTP_SESSION_KEY_TO_HOLD_USER_ID, userId);

      JsonElement principal = authentication == null ? null
          : userId == null ? null : JsonUtil.getAsJsonE(authentication.getPrincipal());
      if (principal != null && principal.isJsonObject()) {
        JsonUtil attributes = new JsonUtil(principal.getAsJsonObject().get("attributes"));
        String name = attributes.getValue("name");
        String picture = attributes.getValue("picture");

        final String randomsCred = UUID.randomUUID().toString();
        final AppUser userOld = userService.getUserById(userId),
            userNew = userOld != null ? userOld : new AppUser(userId, BCrypt.encode(randomsCred));

        userNew.setName(name);
        userNew.setActive(true);
        userNew.setLocked(false);
        userNew.setPicture(picture);
        userNew.setLast_updated_by(userId);
        userNew.setNum_failed_login_attempt(0);
        userNew.setLast_logged_in(new Timestamp(System.currentTimeMillis()));
        if (userOld == null) {
          userNew.setRole(Role.ROLE_USER);
          userService.createUser(userNew);
        } else {
          userService.updateUser(userNew);
        }

        Role role = Validator.coalesce(userNew.getRole(), Role.ROLE_USER);
        SecurityContextHolder.getContext()
            .setAuthentication(UserAuthenticationToken.successToken(userId, role.getAuthority()));
      }

      REDIRECT_STRATEGY.sendRedirect(request, response, "/");
    };
  }
}
