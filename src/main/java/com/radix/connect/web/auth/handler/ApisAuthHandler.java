package com.radix.connect.web.auth.handler;

import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.model.response.ApiResponse;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

/**
 * @author kiran.chokhande
 */
@Service
public final class ApisAuthHandler {

  private static final String API_AUTH_SUCCESS = "API request authenticated successfully, Account ID: ";

  public final AuthenticationSuccessHandler getAuthenticationSuccessHandler() {
    return (request, response, authentication) -> {

      String userId = authentication == null ? null : authentication.getName();
      RxLogger.logMessage("API Authentication Success", API_AUTH_SUCCESS + userId);

      request.getRequestDispatcher(request.getRequestURI()).forward(request, response);
    };
  }

  public final AuthenticationFailureHandler getAuthenticationFailureHandler() {
    return (request, response, authentication) -> {

      String error = authentication.getMessage();
      error = !Validator.isEmpty(error) ? error : "Invalid account id or authorization token";

      RxLogger.logError("API Authentication Failed", error);

      HttpStatus status = HttpStatus.UNAUTHORIZED;

      response.reset();
      response.setStatus(status.value());
      response.setContentType(MediaType.APPLICATION_JSON_VALUE);
      response.getWriter().write(JsonUtil.toString(ApiResponse.failure(status, error)));
    };
  }
}
