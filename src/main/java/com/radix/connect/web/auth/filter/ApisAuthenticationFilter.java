package com.radix.connect.web.auth.filter;

import com.radix.connect.core.Provider;
import com.radix.connect.core.model.Account;
import com.radix.connect.core.util.Validator;
import com.radix.connect.web.auth.handler.ApisAuthHandler;
import com.radix.connect.web.auth.provider.ApisAuthenticationProvider;
import com.radix.connect.web.auth.token.ApisAuthenticationToken;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * @author kiran.chokhande
 */
public final class ApisAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

  private final List<AntPathRequestMatcher> antPathMatcherToAvoidAuthentication = new ArrayList<>();

  private static final String REGEX_TO_EXTRACT_ACCOUNT_ID_FROM_URL = "^(.*/?api/)(AC[\\w]+)(/?.*)$";

  private static final String ACCOUNT_ID_NOT_FOUND = "Account id must be required in request path";
  private static final String AUTH_TOKEN_NOT_FOUND = "Authorization token must be required in request header";

  public ApisAuthenticationFilter(ApisAuthHandler authHandler, Provider<String, Account> provider) {

    super(new AntPathRequestMatcher("/api/**"));
    super.setAuthenticationManager(new ApisAuthenticationProvider(provider)::authenticate);
    super.setAuthenticationSuccessHandler(authHandler.getAuthenticationSuccessHandler());
    super.setAuthenticationFailureHandler(authHandler.getAuthenticationFailureHandler());
  }

  public final void permitAll(final String... requestPatterns) {
    this.antPathMatcherToAvoidAuthentication.clear();
    if (requestPatterns != null && requestPatterns.length > 0) {
      Arrays.stream(requestPatterns).filter(rPattern -> !Validator.isEmpty(rPattern)).forEach(
          rPattern -> antPathMatcherToAvoidAuthentication.add(new AntPathRequestMatcher(rPattern)));
    }
  }

  @Override
  public final boolean requiresAuthentication(HttpServletRequest req, HttpServletResponse res) {

    AntPathRequestMatcher antRequestMatcher = this.antPathMatcherToAvoidAuthentication.stream()
        .filter(antPathReqMatcher -> antPathReqMatcher.matches(req)).findFirst().orElse(null);

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    boolean canAuthenticate = authentication == null || authentication.getAuthorities() == null
        || authentication.getAuthorities().stream().filter(authority -> authority != null
        && "ROLE_API".equals(authority.getAuthority())).findFirst().orElse(null) != null;

    return super.requiresAuthentication(req, res) && antRequestMatcher == null && canAuthenticate;
  }

  @Override
  public final Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
      throws AuthenticationException {

    String accountId = obtainAccountId(req).trim();
    String authToken = obtainAuthToken(req).trim();

    if (Validator.isEmpty(accountId)) {
      throw new AuthenticationCredentialsNotFoundException(ACCOUNT_ID_NOT_FOUND);
    }
    if (Validator.isEmpty(authToken)) {
      throw new AuthenticationCredentialsNotFoundException(AUTH_TOKEN_NOT_FOUND);
    }

    ApisAuthenticationToken authRequest = new ApisAuthenticationToken(accountId, authToken);
    authRequest.setDetails(super.authenticationDetailsSource.buildDetails(req));

    return super.getAuthenticationManager().authenticate(authRequest);
  }

  private String obtainAccountId(HttpServletRequest request) {
    String requestUrl = request.getRequestURI();
    return !requestUrl.matches(REGEX_TO_EXTRACT_ACCOUNT_ID_FROM_URL) ? ""
        : requestUrl.replaceFirst(REGEX_TO_EXTRACT_ACCOUNT_ID_FROM_URL, "$2");
  }

  private String obtainAuthToken(HttpServletRequest request) {
    String authToken = request.getHeader("Authorization");
    return authToken == null ? "" : authToken.replaceAll("Bearer\\s+", "");
  }
}
