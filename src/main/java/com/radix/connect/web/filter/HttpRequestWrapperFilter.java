package com.radix.connect.web.filter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.radix.connect.core.constant.Constant;
import com.radix.connect.core.logger.RxLogger;
import com.radix.connect.core.util.JsonUtil;
import com.radix.connect.core.util.Validator;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author kiran.chokhande
 */
public final class HttpRequestWrapperFilter implements Filter {

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
      throws IOException, ServletException {

    HttpServletResponse httpServletResponse = (HttpServletResponse) response;
    httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
    httpServletResponse.setHeader("X-Content-Type-Options", "nosniff");
    httpServletResponse.setHeader("X-XSS-Protection", "1");
    httpServletResponse.setHeader("Server", "HIDDEN");
    httpServletResponse.addDateHeader("Last-Modified", System.currentTimeMillis());

    filterChain.doFilter(new HttpRequestWrapper((HttpServletRequest) request), response);
  }

  /**
   * @author kiran.chokhande
   */
  private final class HttpRequestWrapper extends HttpServletRequestWrapper {

    private final JsonUtil payload;
    private final byte[] payloadBytes;

    private HttpRequestWrapper(final HttpServletRequest httpRequest) {
      super(httpRequest);

      JsonElement requestPayload = readRequest(httpRequest);
      this.payload = requestPayload == null ? null : new JsonUtil(requestPayload);
      this.payloadBytes = this.payload == null ? new byte[0]
          : this.payload.getAsString() == null ? new byte[0] : payload.getAsString().getBytes();

      httpRequest.setAttribute(Constant.HTTP_REQUEST_KEY_TO_HOLD_PAYLOAD, requestPayload);
    }

    @Override
    public final String getRemoteUser() {

      String remoteUserId = getUserPrincipal() == null ? null : getUserPrincipal().getName();
      if (Validator.isEmpty(remoteUserId)) {
        Object id = getSession().getAttribute(Constant.HTTP_SESSION_KEY_TO_HOLD_USER_ID);
        remoteUserId = id == null ? null : String.valueOf(id);

        if (Validator.isEmpty(remoteUserId)) {
          remoteUserId = SecurityContextHolder.getContext().getAuthentication() == null
              ? null : SecurityContextHolder.getContext().getAuthentication().getName();
        }
      }
      return !Validator.isEmpty(remoteUserId) ? remoteUserId : super.getRemoteUser();
    }

    @Override
    public final String getParameter(final String parameterName) {
      String pValue = payload == null ? null : payload.getValue(parameterName);
      return !Validator.isEmpty(pValue) ? pValue : super.getParameter(parameterName);
    }

    @Override
    public final ServletInputStream getInputStream() {
      return new ServletInputStream() {
        @Override
        public boolean isFinished() {
          return false;
        }
        @Override
        public boolean isReady() {
          return true;
        }
        @Override
        public void setReadListener(final ReadListener readListener) {
        }
        @Override
        public int read() {
          return new ByteArrayInputStream(payloadBytes).read();
        }
      };
    }

    private JsonElement readRequest(final HttpServletRequest request) {

      JsonElement requestParameters = new JsonObject(), requestPayloads = null;

      Enumeration reqParamNames = request.getParameterNames();
      while (reqParamNames.hasMoreElements()) {

        String paramName = String.valueOf(reqParamNames.nextElement());
        String paramValue = request.getParameter(paramName);
        paramValue = Validator.isEmpty(paramValue) ? null : paramValue;
        requestParameters.getAsJsonObject().addProperty(paramName, paramValue);
      }

      try {
        requestPayloads = JsonUtil.getAsJsonE(request.getInputStream());
      } catch (Exception exception) {
        RxLogger.logException("Failed to read request payload.", exception);
      }

      if (requestPayloads != null && !Validator.isEmpty(requestPayloads)) {
        if (!requestPayloads.isJsonObject()) {
          requestParameters = requestPayloads;
        } else {
          JsonObject requestParameter = requestParameters.getAsJsonObject();
          requestPayloads.getAsJsonObject().entrySet()
              .forEach(e -> requestParameter.add(e.getKey(), e.getValue()));
        }
      }
      return !Validator.isEmpty(requestParameters) ? requestParameters : null;
    }
  }
}
