
----------------------------------------------------------------------------------------------------
-- Create schema 'part' for managing partition tables

CREATE SCHEMA IF NOT EXISTS part;
----------------------------------------------------------------------------------------------------

-- Dropping existing triggers and functions on tables
DROP TRIGGER IF EXISTS tgr_on_users_before_update ON public.users;
DROP TRIGGER IF EXISTS tgr_on_account_after_insert ON public.account;
DROP TRIGGER IF EXISTS tgr_on_account_before_update ON public.account;
DROP TRIGGER IF EXISTS tgr_on_system_config_before_update ON public.system_config;
DROP TRIGGER IF EXISTS tgr_on_sender_config_before_update ON public.sender_config;
DROP TRIGGER IF EXISTS tgr_on_sender_mapping_before_update ON public.sender_mapping;

DROP FUNCTION IF EXISTS public.fn_update_last_updated_ts();
DROP FUNCTION IF EXISTS public.fn_create_message_log_partition();

-- Dropping existing tables
DROP TABLE IF EXISTS public.users;
DROP TABLE IF EXISTS public.sender_mapping;
DROP TABLE IF EXISTS public.sender_config;
DROP TABLE IF EXISTS public.message_log;
DROP TABLE IF EXISTS public.recent_sender_log;
DROP TABLE IF EXISTS public.account;
DROP TABLE IF EXISTS public.system_config;
----------------------------------------------------------------------------------------------------

-- Create TABLE public.users
CREATE TABLE public.users (
	user_id text NOT NULL,
	password text NOT NULL,
	name text NULL,
	image_url text NULL,
	role text NOT NULL DEFAULT 'ROLE_USER',
	is_active boolean NOT NULL DEFAULT FALSE,
	is_locked boolean NOT NULL DEFAULT FALSE,
	num_failed_login_attempt smallint NOT NULL DEFAULT 0,
	last_logged_in timestamptz NULL,
	last_updated_by text NOT NULL,
	last_updated_ts timestamptz NOT NULL DEFAULT CLOCK_TIMESTAMP(),
	CONSTRAINT users_pkey PRIMARY KEY (user_id),
	CONSTRAINT users_role CHECK (role ~ '^ROLE_(API|USER|ADMIN)$')
);
----------------------------------------------------------------------------------------------------

-- Create TABLE public.account
CREATE TABLE public.account (
	account_id varchar(34) NOT NULL DEFAULT 'AC' || MD5(RANDOM()::TEXT || CLOCK_TIMESTAMP()::TEXT),
	auth_token text NOT NULL,
	is_active boolean NOT NULL DEFAULT FALSE,
	account_name text NOT NULL,
	app_base_url text NOT NULL,
	last_updated_by text NOT NULL,
	last_updated_ts timestamptz NOT NULL DEFAULT CLOCK_TIMESTAMP(),
	CONSTRAINT account_pkey PRIMARY KEY (account_id),
	CONSTRAINT account_auth_token CHECK (auth_token ~ '^[a-zA-Z0-9]{32}$'),
	CONSTRAINT account_account_id CHECK (account_id ~ '^AC[a-zA-Z0-9]{32}$')
);
----------------------------------------------------------------------------------------------------

-- Create TABLE public.sender_config
CREATE TABLE public.sender_config (
	sender_id varchar(34) NOT NULL DEFAULT 'SN' || MD5(RANDOM()::TEXT || CLOCK_TIMESTAMP()::TEXT),
	modality text NOT NULL,
	sender_api text NOT NULL,
	sender_desc text NOT NULL,
	messaging_rate smallint NOT NULL DEFAULT 1,
	config_json json NOT NULL,
	last_updated_by text NOT NULL,
	last_updated_ts timestamptz NOT NULL DEFAULT CLOCK_TIMESTAMP(),
	CONSTRAINT sender_config_pkey PRIMARY KEY (sender_id, modality),
	CONSTRAINT sender_config_unique UNIQUE (sender_id),
	CONSTRAINT sender_config_messaging_rate CHECK (messaging_rate > 0),
	CONSTRAINT sender_config_sender_id CHECK (sender_id ~ '^SN[a-zA-Z0-9]{32}$'),
	CONSTRAINT sender_config_modality CHECK (modality ~ '^(TEXT|VOICE|EMAIL|FAX)$'),
	CONSTRAINT sender_config_sender_api CHECK (sender_api ~ '^(SMTP|AMAZON|TWILIO|INTERFAX)_API$')
);
----------------------------------------------------------------------------------------------------

-- Create TABLE public.sender_mapping
CREATE TABLE public.sender_mapping (
	account_id varchar(34) NOT NULL,
	modality text NOT NULL,
	sender_id varchar(34) NOT NULL,
	last_updated_by text NOT NULL,
	last_updated_ts timestamptz NOT NULL DEFAULT CLOCK_TIMESTAMP(),
	CONSTRAINT sender_mapping_pkey PRIMARY KEY (account_id, modality),
	CONSTRAINT sender_mapping_fkey_account_id FOREIGN KEY (account_id) REFERENCES account(account_id),
	CONSTRAINT sender_mapping_fkey_sender_id FOREIGN KEY (sender_id, modality) REFERENCES sender_config(sender_id, modality)
);
----------------------------------------------------------------------------------------------------

-- Create TABLE public.recent_sender_log
CREATE TABLE public.recent_sender_log (
	modality text NOT NULL,
	to_phone varchar(15) NOT NULL,
	from_phone varchar(15) NOT NULL,
	account_id varchar(34) NOT NULL,
	message_id varchar(34) NOT NULL,
	sent_timestamp timestamptz NOT NULL DEFAULT CLOCK_TIMESTAMP(),
	CONSTRAINT recent_sender_log_pkey PRIMARY KEY (modality, to_phone),
	CONSTRAINT recent_sender_log_modality CHECK (modality ~ '^(TEXT|VOICE)$'),
	CONSTRAINT recent_sender_log_to_phone CHECK (to_phone ~ '^[0-9]{10,15}$')
);
----------------------------------------------------------------------------------------------------

-- Create TABLE public.message_log
CREATE TABLE public.message_log (
	account_id varchar(34) NOT NULL,
	message_id varchar(34) NOT NULL,
	modality text NOT NULL,
	msg_sid text NULL,
	direction text NOT NULL,
	msg_status text NOT NULL,
	error_desc text NULL,
	logged_ts timestamptz NOT NULL DEFAULT CLOCK_TIMESTAMP(),
	before_ts timestamptz NOT NULL DEFAULT CLOCK_TIMESTAMP() + INTERVAL '2 HOURS',
	processed_ts timestamptz NULL,
	CONSTRAINT message_log_pkey PRIMARY KEY (account_id, message_id),
	CONSTRAINT message_log_fkey FOREIGN KEY (account_id) REFERENCES account(account_id),
	CONSTRAINT message_log_direction CHECK (direction ~ '^(INBOUND|OUTBOUND)$'),
	CONSTRAINT message_log_modality CHECK (modality ~ '^(TEXT|VOICE|EMAIL|FAX)$'),
	CONSTRAINT message_log_message_id CHECK (message_id ~ '^SM|CA|EM|FX[a-zA-Z0-9]{32}$')
) PARTITION BY LIST(account_id);

CREATE INDEX idx_message_log_account_id ON public.message_log(account_id);
CREATE INDEX idx_message_log_message_id ON public.message_log(message_id);
CREATE INDEX idx_message_log_modality ON public.message_log(modality);
CREATE INDEX idx_message_log_msg_sid ON public.message_log(msg_sid);
CREATE INDEX idx_message_log_msg_status ON public.message_log(msg_status);
CREATE INDEX idx_message_log_direction ON public.message_log(direction);
CREATE INDEX idx_message_log_logged_ts ON public.message_log(((logged_ts AT TIME ZONE 'America/New_York')::DATE));
CREATE INDEX idx_message_log_processed_ts ON public.message_log(((processed_ts AT TIME ZONE 'America/New_York')::DATE));
----------------------------------------------------------------------------------------------------

-- Create TABLE public.system_config
CREATE TABLE public.system_config (
	key text NOT NULL,
	key_desc text NOT NULL,
	config_group text NOT NULL DEFAULT 'General',
	value_type text NOT NULL DEFAULT 'TEXT',
	value text NULL,
	encrypted boolean NOT NULL DEFAULT FALSE,
	last_updated_by text NOT NULL,
	last_updated_ts timestamptz NOT NULL DEFAULT CLOCK_TIMESTAMP(),
	CONSTRAINT system_config_pkey PRIMARY KEY (key),
	CONSTRAINT system_config_key CHECK (TRIM(key) != '' AND key ~ '[A-Z_]+'),
	CONSTRAINT system_config_key_desc CHECK (TRIM(key_desc) != ''),
	CONSTRAINT system_config_config_group CHECK (TRIM(config_group) != ''),
	CONSTRAINT system_config_value CHECK (
		CASE
			WHEN encrypted OR value IS NULL THEN TRUE
			WHEN (value_type = 'INT') THEN (value ~ '^-?[0-9]+$')
			WHEN (value_type = 'NUMERIC') THEN (value ~ '^-?[0-9]+\.?[0-9]*$')
			WHEN (value_type = 'BOOL') THEN (value ILIKE ANY (ARRAY['True', 'False'])) ELSE TRUE
		END),
  CONSTRAINT system_config_value_type CHECK (value_type ~ '^(TEXT|DATE|TIME|EMAIL|PHONE|BOOL|INT|NUMERIC|JSON|CRON)$')
);
----------------------------------------------------------------------------------------------------

-- Create FUNCTION public.fn_update_last_updated_ts for TRIGGER
CREATE OR REPLACE FUNCTION public.fn_update_last_updated_ts() RETURNS TRIGGER AS
$$
BEGIN
  NEW.last_updated_ts = CLOCK_TIMESTAMP(); RETURN NEW;
END
$$ LANGUAGE plpgsql;
----------------------------------------------------------------------------------------------------

-- Create FUNCTION public.fn_create_message_log_partition for TRIGGER
CREATE OR REPLACE FUNCTION public.fn_create_message_log_partition() RETURNS TRIGGER AS
$$
BEGIN

  EXECUTE ('CREATE TABLE IF NOT EXISTS part.message_log_' || NEW.account_id || ' PARTITION OF public.message_log FOR VALUES IN(''' || NEW.account_id || ''')');

  RETURN NEW;
END
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------

-- Create TRIGGER on before update public.users
CREATE TRIGGER tgr_on_users_before_update BEFORE UPDATE
ON public.users FOR EACH ROW EXECUTE PROCEDURE public.fn_update_last_updated_ts();

-- Create TRIGGER on before update public.account
CREATE TRIGGER tgr_on_account_before_update BEFORE UPDATE
ON public.account FOR EACH ROW EXECUTE PROCEDURE public.fn_update_last_updated_ts();

-- Create TRIGGER on before update public.system_config
CREATE TRIGGER tgr_on_system_config_before_update BEFORE UPDATE
ON public.system_config FOR EACH ROW EXECUTE PROCEDURE public.fn_update_last_updated_ts();

-- Create TRIGGER on before update public.sender_config
CREATE TRIGGER tgr_on_sender_config_before_update BEFORE UPDATE
ON public.sender_config FOR EACH ROW EXECUTE PROCEDURE public.fn_update_last_updated_ts();

-- Create TRIGGER on before update public.sender_mapping
CREATE TRIGGER tgr_on_sender_mapping_before_update BEFORE UPDATE
ON public.sender_mapping FOR EACH ROW EXECUTE PROCEDURE public.fn_update_last_updated_ts();

-- Create TRIGGER on after insert public.account
CREATE TRIGGER tgr_on_account_after_insert AFTER INSERT
ON public.account FOR EACH ROW EXECUTE PROCEDURE public.fn_create_message_log_partition();
----------------------------------------------------------------------------------------------------